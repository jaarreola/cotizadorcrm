﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public ProductosController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta todos los productos
        [Route("[action]")]
        [HttpPost]
        public IActionResult getProductosLista([FromBody] getProductosListaModel getProductosListaParams)
        {
            //Asigna la variable de conexión a la clase que maneja la transacción de datos (DataBaseAccess)
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getProductosListaParams);
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catProducto_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON); 
        }


        //API Para obtener un porducto por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getProductosById([FromBody] getProductosByIdModel getProductosParams)
        {
            //Asigna la variable de conexión a la clase que maneja la transacción de datos (DataBaseAccess)
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getProductosParams);
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catProducto_ProductoById", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableRowToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON); 
        }

        //API Para Insertar - Actualizar un Producto
        [Route("[action]")]
        [HttpPost]
        public IActionResult setProducto([FromBody] setProductosModel setProductosParams)
        {
            //Asigna la variable de conexión a la clase que maneja la transacción de datos (DataBaseAccess)
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setProductosParams);
            //Ejecuta la función de Query SIN Response y obtiene en el mismo diccionario de parámetros el Identity generado en SP
            dbAccess.ExecuteNonQuery("sp_set_catProducto", postParams);
            //Si no existe error, regresa el response de la petición 
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Consulta Las plazas por Empresa y Area
        [Route("[action]")]
        [HttpPost]
        public IActionResult getProductosAutocomplete([FromBody] getProductosAutocompleteModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catProducto_ListadoByEmpresaArea", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }
    }
}
