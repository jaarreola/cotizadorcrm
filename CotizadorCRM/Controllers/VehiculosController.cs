﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiculosController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public VehiculosController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta todos los Tipos de Operacion
        [Route("[action]")]
        [HttpPost]
        public IActionResult getVehiculosLista([FromBody] getVehiculosListaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catVehiculoArmado_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para obtener un Tipo de Operacion por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getVehiculoById([FromBody] getVehiculoIdModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catVehiculoArmado_VehiculoById", postParams).Tables[0];
            Dictionary<string, object> dicResult = page.DataTableToMapRow(dtInfo);
            DataTable dtInfoDetail = dbAccess.ExecuteQuery("sp_get_catVehiculoArmadoProductos_ProductosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "productos");
            string responseJSON = JsonSerializer.Serialize(dicResult);
            return Ok(responseJSON);
        }

        //API Para Insertar - Actualizar un vehículo
        [Route("[action]")]
        [HttpPost]
        public IActionResult setVehiculo([FromBody] setVehiculoModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            dbAccess.ExecuteNonQuery("sp_set_catVehiculoArmado", postParams);
            //Convierte el detalle de Model Contactos de Cliente a tipo Dictionary: envía la lista de detalle (contactos) y el listado de propiedades. 
            List<Dictionary<string, object>> dictProductos = page.ListModelToDictionary<setVehiculoProductosModel>((List<setVehiculoProductosModel>)postParams["productos"], typeof(setVehiculoProductosModel).GetProperties());
            //Inserta detalle: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
            page.InsertDetail(dbAccess, "idVehiculoArmado", dictProductos, "sp_set_catVehiculoArmadoProductos", "sp_delete_catVehiculoArmadoProductos", postParams["idVehiculoArmado"].ToString(), true);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Consulta Las plazas por Empresa y Area
        [Route("[action]")]
        [HttpPost]
        public IActionResult getVehiculoListaByEmpresaAreaModel([FromBody] getVehiculoListaByEmpresaAreaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catVehiculoArmado_ListadoByEmpresaArea", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }
    }
}
