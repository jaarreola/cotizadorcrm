﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TiposOperacionController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public TiposOperacionController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta todos los Tipos de Operacion
        [Route("[action]")]
        [HttpPost]
        public IActionResult getTiposOperacionLista([FromBody] getTiposOperacionListaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catTipoOperacion_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo); 
            return Ok(responseJSON);
        }

        //API Para obtener un Tipo de Operacion por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getTipoOperacionById([FromBody] getTipoOperacionIdModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catTipoOperacion_TipoOperacionById", postParams).Tables[0];
            string responseJSON = page.DataTableRowToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para Insertar - Actualizar un Tipo de Operacion
        [Route("[action]")]
        [HttpPost]
        public IActionResult setTipoOperacion([FromBody] setTipoOperacionModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            dbAccess.ExecuteNonQuery("sp_set_catTipoOperacion", postParams);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Consulta todos los Tipos de Operacion
        [Route("[action]")]
        [HttpPost]
        public IActionResult getTiposOperacionByEmpresaArea([FromBody] getTiposOperacionByEmpresaAreaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catTipoOperacion_ListadoByEmpresaArea", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }
    }
}
