﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CombustibleTarifasController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public CombustibleTarifasController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta Listado
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCombustibleTarifasLista([FromBody] getTarifaListaModel getProductosListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getProductosListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catCombustibleTarifas_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }


        //API Para obtener registro por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCombustibleTarifaById([FromBody] getTarifaByIdModel getProductosParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getProductosParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catCombustibleTarifas_TarifaById", postParams).Tables[0];
            string responseJSON = page.DataTableRowToJSON(dtInfo); 
            return Ok(responseJSON);
        }

        //API Para Insertar - Actualizar Registro
        [Route("[action]")]
        [HttpPost]
        public IActionResult setCombustibleTarifa([FromBody] setTarifaModel setProductosParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setProductosParams);
            dbAccess.ExecuteNonQuery("sp_set_catCombustibleTarifas", postParams);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Listado Autocomplete
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCombustibleTarifasAutocomplete([FromBody] getCombustibleTarifasAutocomplete getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catCombustibleTarifas_ListadoByEmpresa", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }
        
    }
}
