﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UreaTarifasController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public UreaTarifasController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta Listado
        [Route("[action]")]
        [HttpPost]
        public IActionResult getUreaTarifasLista([FromBody] getUreaTarifaListaModel getParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catUreaTarifas_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }


        //API Para obtener registro por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getUreaTarifaById([FromBody] getUreaTarifaByIdModel getParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catUreaTarifas_TarifaById", postParams).Tables[0];
            string responseJSON = page.DataTableRowToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para Insertar - Actualizar Registro
        [Route("[action]")]
        [HttpPost]
        public IActionResult setUreaTarifa([FromBody] setUreaTarifaModel setParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setParams);
            dbAccess.ExecuteNonQuery("sp_set_catUreaTarifas", postParams);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Listado Autocomplete
        [Route("[action]")]
        [HttpPost]
        public IActionResult getUreaTarifasAutocomplete([FromBody] getUreaTarifasAutocomplete getParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catUreaTarifas_ListadoByEmpresa", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }
    }
}
