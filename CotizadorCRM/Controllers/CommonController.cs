﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public CommonController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta de Embalajes
        [Route("[action]")]
        [HttpGet]
        public IActionResult getEmbalajeListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbEmbalajeIdioma_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todos los Tipos de Operaciones
        [Route("[action]")]
        [HttpGet]
        public IActionResult getClasificacionTipoOperacionListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbClasificacionTipoOperacionIdioma_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todos los Ejes vehiculares
        [Route("[action]")]
        [HttpGet]
        public IActionResult getVehiculoEjesListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbVehiculoEjesIdioma_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todos los Ejes vehiculares
        [Route("[action]")]
        [HttpGet]
        public IActionResult getVehiculoClasificacionListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbVehiculoClasificacionIdioma_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todos los Tipos de Servicios
        [Route("[action]")]
        [HttpGet]
        public IActionResult getTiposServiciosListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbTipoServicioIdioma_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todas las Monedas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getMonedasListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbMonedaIdioma_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todas las Monedas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getFuentesTipoCambioListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            //Genera diccionario de datos en base a la estructura del MODEL
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            //Ejecuta la función de Query + Response para obtener los datos (params: SP, parametros). Regresa un DataTable
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbTipoCambioFuente_Listado", postParams).Tables[0];
            //Utiliza función estandar DataTableToSimpleJSON de BasePAge para convertir DataTable a JSON
            string responseJSON = page.DataTableToJSON(dtInfo); //response["dtInfo"] = page.DataTableToMap(dtInfo);
            //Si no existe error, regresa el response de la petición 
            return Ok(responseJSON);
        }

        //API Consulta todas Tipo de Personal
        [Route("[action]")]
        [HttpGet]
        public IActionResult getPersonalTipoListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbPersonalTipoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo); 
            return Ok(responseJSON);
        }

        //API Consulta todos los Países
        [Route("[action]")]
        [HttpGet]
        public IActionResult getPaisesListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbPais_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los productos
        [Route("[action]")]
        [HttpPost]
        public IActionResult getEstadosLista([FromBody] getEstadosListaListaModel getParams)
        {
            //Asigna la variable de conexión a la clase que maneja la transacción de datos (DataBaseAccess)
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbEstados_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de cliente
        [Route("[action]")]
        [HttpGet]
        public IActionResult getClienteTiposLista()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbClienteTipoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de rutas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getRutaTiposLista()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbRutaTipoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de vehiculo de rutas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getRutaTiposVehiculoLista()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbRutaTipoVehiculoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de condiciones de rutas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getRutaCondicionesOrigenLista()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbRutaCondicionesOrigenIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de parada de rutas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getRutaTiposParadaLista()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbRutaTipoParadaIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los movimientos de rutas
        [Route("[action]")]
        [HttpGet]
        public IActionResult getRutaMovimientosLista()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbRutaMovimientoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los Tipos de Costo
        [Route("[action]")]
        [HttpGet]
        public IActionResult getCostosTiposListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbCostosTipoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los Tipos de Costo
        [Route("[action]")]
        [HttpGet]
        public IActionResult getFrecuenciasServicioListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbFrecuenciasServicioIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los Tipos de Cobro
        [Route("[action]")]
        [HttpGet]
        public IActionResult getTipoCobroListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbTipoCobroIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos las Unidades de Medida
        [Route("[action]")]
        [HttpGet]
        public IActionResult getUnidadesMedidaListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbUnidadMedida_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de carretera
        [Route("[action]")]
        [HttpGet]
        public IActionResult getTiposCarreteraListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbTipoCarretera_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de carretera
        [Route("[action]")]
        [HttpGet]
        public IActionResult getCotizacionEstatusListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbCotizacionEstatus_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los tipos de Pago
        [Route("[action]")]
        [HttpGet]
        public IActionResult getTipoPagoListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbTipoPagoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los IVA's
        [Route("[action]")]
        [HttpGet]
        public IActionResult getIVAListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbImpuestos_ListadoIVA", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los IVA's
        [Route("[action]")]
        [HttpGet]
        public IActionResult getRetencionesListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbImpuestos_ListadoRetenciones", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta estatus de convenios
        [Route("[action]")]
        [HttpGet]
        public IActionResult getConvenioEstatusListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbConvenioEstatus_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta Tipos de Combustible
        [Route("[action]")]
        [HttpGet]
        public IActionResult getCombustibleTiposListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbCombustibleTipoIdioma_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta Tipos de Combustible
        [Route("[action]")]
        [HttpGet]
        public IActionResult getCotizacionMotivosRechazoListado()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbCotizacionRechazo_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta todos los Tipos de Costo
        [Route("[action]")]
        [HttpGet]
        public IActionResult getConfiguracionTipoFactor()
        {
            //Conexión DB
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = new Dictionary<string, object>();
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_dbConfiguracionTipoFactor_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

    }
}
