﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiciosController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public ServiciosController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta todos los Servicios
        [Route("[action]")]
        [HttpPost]
        public IActionResult getServiciosLista([FromBody] getServiciosListaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catServicio_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para obtener un Servicio por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getServicioById([FromBody] getServicioIdModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catServicio_ServicioById", postParams).Tables[0];
            string responseJSON = page.DataTableRowToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para Insertar - Actualizar un Servicio
        [Route("[action]")]
        [HttpPost]
        public IActionResult setServicio([FromBody] setServicioModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            dbAccess.ExecuteNonQuery("sp_set_catServicio", postParams);
            return Ok(JsonSerializer.Serialize(postParams));
        }
    }
}
