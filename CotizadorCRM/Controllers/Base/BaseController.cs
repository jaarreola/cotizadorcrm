﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;


namespace CotizadorCRM.Controllers.Base
{
    public class BaseController : ControllerBase
    {
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;


        public BaseController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }
    }
}
