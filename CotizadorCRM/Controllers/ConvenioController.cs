﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using RestSharp;
using Newtonsoft.Json;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConvenioController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public ConvenioController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //Funciones API para Cotizaciones
        //API Para Insertar - Actualizar
        [Route("[action]")]
        [HttpPost]
        public IActionResult setConvenio([FromBody] setConvenioModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString()));
            dbAccess.ExecuteNonQuery("sp_set_prConvenio", postParams);
            //Convierte el detalle de Model Contactos de Cliente a tipo Dictionary: envía la lista de detalle (contactos) y el listado de propiedades. 
            if (!postParams["idConvenio"].ToString().Equals("") && !postParams["idConvenio"].ToString().Equals("0"))
            {
                List<Dictionary<string, object>> dictDetalle = page.ListModelToDictionary<setConvenioSegmentosModel>((List<setConvenioSegmentosModel>)postParams["segmentos"], typeof(setConvenioSegmentosModel).GetProperties());
                //Inserta detalle de SEGMENTOS: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
                page.InsertDetail(dbAccess, "idConvenio", dictDetalle, "sp_set_prConvenioSegmentos", "sp_delete_prConvenioSegmentos", postParams["idConvenio"].ToString(), true);
                dictDetalle = page.ListModelToDictionary<setConvenioProductosModel>((List<setConvenioProductosModel>)postParams["productos"], typeof(setConvenioProductosModel).GetProperties());
                //Inserta detalle de COSTOS: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
                page.InsertDetail(dbAccess, "idConvenio", dictDetalle, "sp_set_prConvenioProductos", "sp_delete_prConvenioProductos", postParams["idConvenio"].ToString(), true);
            }
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }

        //API Consulta por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getConvenioById([FromBody] getConvenioByIdModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prConvenio_ConvenioById", postParams).Tables[0];
            Dictionary<string, object> dicResult = page.DataTableToMapRow(dtInfo);
            DataTable dtInfoDetail = dbAccess.ExecuteQuery("sp_get_prConvenioSegmentos_SegmentosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "segmentos");
            dtInfoDetail = dbAccess.ExecuteQuery("sp_get_prConvenioProductos_CostosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "productos");
            string responseJSON = System.Text.Json.JsonSerializer.Serialize(dicResult);
            return Ok(responseJSON);
        }

        //API Consulta listado
        [Route("[action]")]
        [HttpPost]
        public IActionResult getConvenioLista([FromBody] getConveniosListaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prConvenio_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult setContratoToConvenio([FromBody] setContratoToConvenioModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString()));
            dbAccess.ExecuteNonQuery("sp_set_prConvenio_generaConvenio", postParams);
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult updateFechaVigenciaContrato([FromBody] updateFechaVigenciaContratoModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString()));
            dbAccess.ExecuteNonQuery("sp_set_prCotizacion_updateFechaVigencia", postParams);
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }
    }

}
