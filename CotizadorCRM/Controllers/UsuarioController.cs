﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public UsuarioController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            string dbType = Configuration.GetConnectionString("DBType");
            switch (dbType)
            {
                case "SQLSERVER":
                    if (env.IsDevelopment())
                    {
                        connStr = Configuration.GetConnectionString("strConnSQLServerDev");
                    }
                    else
                    {
                        connStr = Configuration.GetConnectionString("strConnSQLServerProd");
                    }
                    break;
                case "MYSQL":
                    
                    if (env.IsDevelopment())
                    {
                        connStr = Configuration.GetConnectionString("strConnMySQLDev");
                    }
                    else
                    {
                        connStr = Configuration.GetConnectionString("strConnMySQLProd");
                    }
                    break;
            }
            
        }

        //API Para obtener un Personal por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getUsuarioLogin([FromBody] getUsuarioLogin getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catUsuario_Login", postParams).Tables[0];
            string responseJSON = page.DataTableRowToJSON(dtInfo);
            return Ok(responseJSON);
        }
    }
}
