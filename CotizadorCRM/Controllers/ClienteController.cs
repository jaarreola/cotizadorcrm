﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public ClienteController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta todos los Personales
        [Route("[action]")]
        [HttpPost]
        public IActionResult getClientesLista([FromBody] getClientesListaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catCliente_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para obtener un Personal por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getClienteById([FromBody] getClienteByIdModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catCliente_ClienteById", postParams).Tables[0];
            Dictionary<string, object> dicResult = page.DataTableToMapRow(dtInfo);
            DataTable dtInfoDetail = dbAccess.ExecuteQuery("sp_get_catClienteContacto_ClienteContactosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "contactos");
            string responseJSON = JsonSerializer.Serialize(dicResult);
            return Ok(responseJSON);
        }

        //API Para Insertar - Actualizar un Cliente
        [Route("[action]")]
        [HttpPost]
        public IActionResult setCliente([FromBody] setClienteModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            dbAccess.ExecuteNonQuery("sp_set_catCliente", postParams);
            //Convierte el detalle de Model Contactos de Cliente a tipo Dictionary: envía la lista de detalle (contactos) y el listado de propiedades. 
            List<Dictionary<string, object>> dictContactos = page.ListModelToDictionary<setClienteContactoModel>((List<setClienteContactoModel>)postParams["contactos"], typeof(setClienteContactoModel).GetProperties());
            //Inserta detalle: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
            page.InsertDetail(dbAccess, "idCliente", dictContactos, "sp_set_catClienteContacto", "sp_delete_catClienteContacto", postParams["idCliente"].ToString(), true);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Consulta general
        [Route("[action]")]
        [HttpPost]
        public IActionResult getClientesListaAutocomplete([FromBody] getInfoByEmpresaModel getListaParams)
        {
            //Asigna la variable de conexión a la clase que maneja la transacción de datos (DataBaseAccess)
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catCliente_ListadoByIdEmpresaAutocomplete", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

    }
}
