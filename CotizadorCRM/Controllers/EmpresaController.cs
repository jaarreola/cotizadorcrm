﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public EmpresaController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Consulta general
        [Route("[action]")]
        [HttpPost]
        public IActionResult getEmpresaLista([FromBody] getInfoByEmpresaModel getListaParams)
        {
            //Asigna la variable de conexión a la clase que maneja la transacción de datos (DataBaseAccess)
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catEmpresa_ListadoEmpresas", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }
    }
}
