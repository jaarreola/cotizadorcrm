﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresaConfiguracionController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();

        //Método que inicializa las variables de conexión
        public EmpresaConfiguracionController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //API Para obtener registro por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getEmpresaConfiguracion([FromBody] getEmpresaConfiguracionModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracion", postParams).Tables[0];
            Dictionary<string, object> dicResult = page.DataTableToMapRow(dtInfo);
            DataTable dtInfoDetail = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracionVariablesSueldos_SueldosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "sueldos");
            string responseJSON = JsonSerializer.Serialize(dicResult);
            return Ok(responseJSON);
        }

        //API Agregar / Modificar registro
        [Route("[action]")]
        [HttpPost]
        public IActionResult setEmpresaConfigVariables([FromBody] setEmpresaConfiguracionVariablesModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            dbAccess.ExecuteNonQuery("sp_set_catEmpresaConfiguracion_costosVariables", postParams);
            //Convierte el detalle de Model Contactos de Cliente a tipo Dictionary: envía la lista de detalle (contactos) y el listado de propiedades. 
            List<Dictionary<string, object>> dictSueldos = page.ListModelToDictionary<setEmpresaConfiguracioVariablesSueldosModel>((List<setEmpresaConfiguracioVariablesSueldosModel>)postParams["sueldos"], typeof(setEmpresaConfiguracioVariablesSueldosModel).GetProperties());
            //Inserta detalle: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
            page.InsertDetail(dbAccess, "idEmpresa", dictSueldos, "sp_set_catEmpresaConfiguracionVariablesSueldos", "sp_delete_catEmpresaConfiguracionVariablesSueldos", postParams["idEmpresa"].ToString(), true);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Agregar / Modificar registro
        [Route("[action]")]
        [HttpPost]
        public IActionResult setEmpresaConfigFijos([FromBody] setEmpresaConfiguracioFijosModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            dbAccess.ExecuteNonQuery("sp_set_catEmpresaConfiguracion_costosFijos", postParams);
            return Ok(JsonSerializer.Serialize(postParams));
        }

        //API Obtiene Costos Variables de Empresa
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCostosVariablesEmpresa([FromBody] getCostosVariablesEmpresaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracion_CostosVariablesByEmpresa", postParams).Tables[0];
            string responseJSON = page.DataTableRowToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para obtener un Tipo de Operacion por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCostosFijos([FromBody] getCostosVariablesEmpresaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracionCostosFijos", postParams).Tables[0];
            Dictionary<string, object> dicResult = page.DataTableToMapRow(dtInfo);
            DataTable dtInfoDetail = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracionCostosFijosDetalle", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "gastos");
            string responseJSON = JsonSerializer.Serialize(dicResult);
            return Ok(responseJSON);
        }
    }
}
