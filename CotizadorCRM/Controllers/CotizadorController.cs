﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CotizadorCRM.Models;
using System.Text.Json;
using CotizadorCRM.Data.logic;
using Microsoft.Extensions.Configuration;
using System.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using RestSharp;
using Newtonsoft.Json;
using System.Net.Http;
using HiQPdf;
using System.IO;

namespace CotizadorCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CotizadorController : ControllerBase
    {
        //Variables de conexión
        public IConfiguration Configuration { get; }
        public string connStr = String.Empty;
        private BasePage page = new BasePage();
        //Variables para consumo API INEGI (documentación en: https://www.inegi.org.mx/servicios/Ruteo/Default.html)
        private string TokenINEGI = "";
        private string TokenINEGIAux = "";
        private string buscaDestinoURL = "http://gaia.inegi.org.mx/sakbe_v3.1/buscadestino";
        private string buscaLineaURL = "http://gaia.inegi.org.mx/sakbe_v3.1/buscalinea";
        private string calculoRutaLibre = "http://gaia.inegi.org.mx/sakbe_v3.1/libre";
        private string calculoRutaCuota = "http://gaia.inegi.org.mx/sakbe_v3.1/cuota";
        private string calculoRutaOptima = "http://gaia.inegi.org.mx/sakbe_v3.1/optima";
        private string calculoDetalleRutaLibre = "http://gaia.inegi.org.mx/sakbe_v3.1/detalle_l";
        private string calculoDetalleRutaCuota = "http://gaia.inegi.org.mx/sakbe_v3.1/detalle_c";
        private string calculoDetalleRutaOptima = "http://gaia.inegi.org.mx/sakbe_v3.1/detalle_o";
        private string combustibleURL = "http://gaia.inegi.org.mx/sakbe_v3.1/combustible";

        private string hiQpdfSerial = "";
        /*
         ********APIS INEGI***********
         *Búsqueda de Destinos: Busca un punto en base a un nombre del lugar y obtener el identificador para búsqueda de RUTAS; se implementa por cada punto a trazar (orgien, destino), los segmentos ya envía éste identificador
         *- Entrada: parámetros especiales; buscar (nombre de búsqueda), num (cantidad de resultados máximos esperados)
         *- Respuesta: coordenatas (lat y lon), id_dest (ID requerido para otras API)
         *
         *Búsqueda de líneas: Para hacer búsquedas de línea en mapa INEGI y obtener los identificadores necesarios para búsqueda de RUTAS; se implementa por cada punto a trazar (orgien, destino y segmentos)
         *- Entrada: parámetros especiales; x (coordenada de longitud obtenia en API Búsqueda de Destinos; tiene que ser coordenada exacta del response), y (coordenada de latitud obtenia en API Búsqueda de Destinos; tiene que ser coordenada exacta del response)
         *- Salida: id_routing_net (identificador de la línea; requerido para consultar ruta), source (source de la línea; requerido para consultar ruta), target (target de la línea; requerido para consultar ruta) 
         *
         *Cálculo de Ruta: 
         *- Entrada (Si viene de línea): parámetros especiales; id_i: identificador (id_rounting_net; de API Búsqueda de Línea Origen), source_i (source ; de API Búsqueda de Línea Origen),
         *           target_i (target ; de API Búsqueda de Línea Origen), id_f: identificador (id_rounting_net; de API Búsqueda de Línea Destino),
         *           source_f (source ; de API Búsqueda de Línea Destino), target_f (target ; de API Búsqueda de Línea Destino)
         * - Entrada (Si viene de destino): parámetros especiales; dest_i (identificador; de API Búsqueda de Destinos Origen), dest_f (identificador; de API Búsqueda de Destinos Destino)
         * - Entrada (Requeridos COMMON): 
         *      - v: especifica el tipo de vehículo 
         *           v=	Descripción
         *           0	Motocicleta
         *           1	Automóvil
         *           2	Autobús dos ejes
         *           3	Autobús tres ejes
         *           4	Autobús cuatro ejes
         *           5	Camión dos ejes
         *           6	Camión tres ejes
         *           7	Camión cuatro ejes
         *           8	Camión cinco ejes
         *           9	Camión seis ejes
         *           10	Camión siete ejes
         *           11	Camión ocho ejes
         *           12	Camión nueve ejes
         *       - e: especifica el número de ejes excedentes; por default 0 hasta que se implemente funcionalidad.
         *           e=	Descripción
         *           0	Sin ejes excedentes
         *           1	un eje excedente
         *           2	dos ejes excedentes
         *           3	tres ejes excedentes
         *           4	cuatro ejes excedentes
         *           5	cinco ejes excedentes
         *       - b: cadena de caracteres que especifica los id_routing_net de las líneas separado por comas, líneas por las cuales la ruta no pasará por algún motivo param Opcional (sin implementar aún)
         * - API a consultar: 
         *        P=	Descripción
         *       0	Preferentemente libre
         *       1	Preferentemente cuota
         *       2	Ruta sugerida
         *- Salida: advertencias, costo_casetas, geojson (coordenadas de trazado de la ruta), long_km, peajes, tiempo_min (tiempo estimado de ruta)
         *
         *Cálculo de Detalle de la Ruta: 
         *- Entrada: Se utilizarán los mismos parámetros de API Cálculo de ruta
         *- API a consultar: Mismo criterio que el API de Cálculo de Ruta
         *- Salida: detalle por punto:
         *      - direccion: Dirección de giro y nombre de la carretera, camino o vialidad.
         *      - long_m: Longitud del segmento en metros.
         *      - tiempo_min: Tiempo de recorrido del segmento en minutos.
         *      - costo_caseta:Cantidad en pesos a pagar por transitar por el segmento.
         *      - punto_caseta: Geometría en formato geoJson de la caseta asociada al segmento (en caso de existir una asociación).
         *      - eje_excedente: Cantidad en pesos a pagar por los ejes excedentes.
         *      - giro: Valor numérico que indica hacia donde se debe girar.
         *
         */

        //Método que inicializa las variables de conexión
        public CotizadorController(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            TokenINEGI = Configuration.GetConnectionString("TokenINEGI");
            TokenINEGIAux = Configuration.GetConnectionString("TokenINEGIAux");
            hiQpdfSerial = Configuration.GetConnectionString("HiQpdfSerialNumber");
            if (env.IsDevelopment())
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerDev");
            }
            else
            {
                connStr = Configuration.GetConnectionString("strConnSQLServerProd");
            }
        }

        //****************Funciones de Búsqueda API INEGI**********************
        //DATOS: Función para llamado de API INEGI con respuesta en formato response a Front
        private IActionResult callApiINEGI(string URL, Dictionary<string, string> paramsAPI)
        {
            try
            {
                var client = new RestClient(URL);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                // request.AddHeader("Cookie", "NSC_MC_Hbjb-jofhj-psh-ny=ffffffff0990153b45525d5f4f58455e445a4a423660");
                request.AddParameter("key", TokenINEGI);
                request.AddParameter("type", "json");
                foreach (KeyValuePair<string, string> row in paramsAPI)
                {
                    request.AddParameter(row.Key, row.Value);
                }
                IRestResponse response = client.Execute(request);
                return Ok(response.Content);
            }
            catch (HttpRequestException e)
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre)
            {
                throw new Exception("API INEGI no disponible.");
            }
        }

        //DATOS: Función para llamado de API INEGI con respuesta deserializada para trabajar con los datos
        private Dictionary<string, object> callApiINEGIDeserialize(string URL, Dictionary<string, string> paramsAPI)
        {
            try
            {
                var client = new RestClient(URL);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                //request.AddHeader("Cookie", "NSC_MC_Hbjb-jofhj-psh-ny=ffffffff0990153b45525d5f4f58455e445a4a423660");
                request.AddParameter("key", TokenINEGI);
                request.AddParameter("type", "json");
                foreach (KeyValuePair<string, string> row in paramsAPI)
                {
                    request.AddParameter(row.Key, row.Value);
                }
                IRestResponse response = client.Execute(request);
                return JsonConvert.DeserializeObject<Dictionary<string, object>>(response.Content);
            }
            catch (HttpRequestException e)
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre)
            {
                throw new Exception("API INEGI no disponible.");
            }
        }

        private Dictionary<string, object> callApiINEGIDeserializeAux(string URL, Dictionary<string, string> paramsAPI)
        {
            try
            {
                var client = new RestClient(URL);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                //request.AddHeader("Cookie", "NSC_MC_Hbjb-jofhj-psh-ny=ffffffff0990153b45525d5f4f58455e445a4a423661");
                request.AddParameter("key", TokenINEGIAux);
                request.AddParameter("type", "json");
                foreach (KeyValuePair<string, string> row in paramsAPI)
                {
                    request.AddParameter(row.Key, row.Value);
                }
                IRestResponse response = client.Execute(request);
                return JsonConvert.DeserializeObject<Dictionary<string, object>>(response.Content);
            }
            catch (HttpRequestException e)
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre)
            {
                throw new Exception("API INEGI no disponible.");
            }
        }

        //CONSULT: API Para consultar destino INEGI
        [Route("[action]")]
        [HttpPost]
        public IActionResult buscaDestino([FromBody] INEGIApiBuscaDestioRequest getListaParams)
        {
            try
            {
                var requestAPIPArams = new Dictionary<string, string>();
                requestAPIPArams.Add("buscar", getListaParams.lugarBusqueda);
                requestAPIPArams.Add("proj", "GRS80");
                requestAPIPArams.Add("num", "5");
                return Ok(callApiINEGI(buscaDestinoURL, requestAPIPArams));
            }
            catch (HttpRequestException e)
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre)
            {
                throw new Exception("API INEGI no disponible.");
            }
        }

        //CONSULTA: API Para consultar lineas INEGI
        [Route("[action]")]
        [HttpPost]
        public IActionResult buscaLinea([FromBody] INEGIApiBuscaLineaRequest getListaParams)
        {
            try
            {
                var requestAPIPArams = new Dictionary<string, string>();
                requestAPIPArams.Add("x", getListaParams.posX);
                requestAPIPArams.Add("y", getListaParams.posY);
                requestAPIPArams.Add("escala", getListaParams.escala); //"10000"
                return Ok(callApiINEGI(buscaLineaURL, requestAPIPArams));
            }
            catch (HttpRequestException e)
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre)
            {
                throw new Exception("API INEGI no disponible.");
            }

        }


        //*************************Funciones de COTIZADOR*************************************
        //API Consulta costos iniciales 
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCostosIniciales([FromBody] CotizadorModel getProductosListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getProductosListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prCotizador_CostosIniciales", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo); 
            return Ok(responseJSON);
        }

        //API Consulta costos actualizados por KMS
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCostosPorKMS([FromBody] getCostosPorKMSModel getProductosListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getProductosListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prCotizador_CostosPorKMS", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Para consultar destino INEGI: Se utiliza para el autocomplete de direcciones
        [Route("[action]")]
        [HttpPost]
        public IActionResult getDireccionSegmento([FromBody] INEGIApiBuscaDestioRequest getListaParams)
        {
            try
            {
                var dbAccess = new DataBaseAccess(connStr);
                List<INEGIApiBuscaDestioResponse> responseDirecciones = new List<INEGIApiBuscaDestioResponse>();
                if (getListaParams.lugarBusqueda != null)
                {
                    if (!getListaParams.lugarBusqueda.Equals(""))
                    {
                        //Valida si está buscando Dirección para Plaza y ya existe en DB
                        if(getListaParams.idPlaza > 0)
                        {
                            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
                            DataTable dtPlazas = dbAccess.ExecuteQuery("sp_get_catPlazas_GetPlazaINEGI", postParams).Tables[0];
                            if(dtPlazas.Rows.Count > 0)
                            {
                                INEGIApiBuscaDestioResponse responseRow = new INEGIApiBuscaDestioResponse();
                                responseRow.lon = Decimal.Parse(dtPlazas.Rows[0]["LonINEGI"].ToString());
                                responseRow.lat = Decimal.Parse(dtPlazas.Rows[0]["LatINEGI"].ToString());
                                responseRow.id = int.Parse(dtPlazas.Rows[0]["IdINEGI"].ToString());
                                responseRow.direccion = dtPlazas.Rows[0]["DireccionINEGI"].ToString();
                                responseDirecciones.Add(responseRow);
                            }
                            else
                            {
                                //Busca la plaza en INEGI; al encontrarla, guarda la información en DB para futuras búsquedas
                                var requestAPIPArams = new Dictionary<string, string>();
                                requestAPIPArams.Add("buscar", getListaParams.lugarBusqueda);
                                requestAPIPArams.Add("proj", "GRS80");
                                requestAPIPArams.Add("num", "1");
                                //Llama el API de búsqueda de Lugares INEGI
                                var dicResponseAPI = callApiINEGIDeserialize(buscaDestinoURL, requestAPIPArams);
                                //Obtiene el diccionario de búsqueda
                                if (dicResponseAPI.Count > 1)
                                {
                                    var dicResponseData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dicResponseAPI["data"].ToString());
                                    foreach (Dictionary<string, object> direccions in dicResponseData)
                                    {
                                        INEGIApiBuscaDestioResponse responseRow = new INEGIApiBuscaDestioResponse();
                                        foreach (KeyValuePair<string, object> detalleDireccion in direccions)
                                        {
                                            var coordendas = JsonConvert.DeserializeObject<Dictionary<string, object>>(direccions["geojson"].ToString());
                                            responseRow.lon = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).First.ToString());
                                            responseRow.lat = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).Last.ToString());
                                            responseRow.id = int.Parse(direccions["id_dest"].ToString());
                                            responseRow.direccion = direccions["nombre"].ToString();
                                        }
                                        responseDirecciones.Add(responseRow);
                                        //Ingresa el resultado de la plaza
                                        Dictionary<string, object> paramsActualizaPlaza = new Dictionary<string, object>();
                                        paramsActualizaPlaza.Add("idPlaza", getListaParams.idPlaza);
                                        paramsActualizaPlaza.Add("IdINEGI", responseRow.id);
                                        paramsActualizaPlaza.Add("DireccionINEGI", responseRow.direccion);
                                        paramsActualizaPlaza.Add("LatINEGI", responseRow.lat);
                                        paramsActualizaPlaza.Add("LonINEGI", responseRow.lon);
                                        dbAccess.ExecuteNonQuery("sp_update_catPlazas_InfoINEGI", paramsActualizaPlaza);
                                    }
                                }
                                else
                                {
                                    throw new Exception("API INEGI no disponible.");
                                }
                            }
                        }
                        else
                        {
                            //Busqueda de autocomplete y despliegue de 5 resultados
                            var requestAPIPArams = new Dictionary<string, string>();
                            requestAPIPArams.Add("buscar", getListaParams.lugarBusqueda);
                            requestAPIPArams.Add("proj", "GRS80");
                            requestAPIPArams.Add("num", "5");
                            //Llama el API de búsqueda de Lugares INEGI
                            var dicResponseAPI = callApiINEGIDeserialize(buscaDestinoURL, requestAPIPArams);
                            //Obtiene el diccionario de búsqueda
                            if (dicResponseAPI.Count > 1)
                            {
                                var dicResponseData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dicResponseAPI["data"].ToString());
                                foreach (Dictionary<string, object> direccions in dicResponseData)
                                {
                                    INEGIApiBuscaDestioResponse responseRow = new INEGIApiBuscaDestioResponse();
                                    foreach (KeyValuePair<string, object> detalleDireccion in direccions)
                                    {
                                        var coordendas = JsonConvert.DeserializeObject<Dictionary<string, object>>(direccions["geojson"].ToString());
                                        responseRow.lon = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).First.ToString());
                                        responseRow.lat = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).Last.ToString());
                                        responseRow.id = int.Parse(direccions["id_dest"].ToString());
                                        responseRow.direccion = direccions["nombre"].ToString();
                                    }
                                    responseDirecciones.Add(responseRow);
                                }
                            }
                            else
                            {
                                throw new Exception("API INEGI no disponible.");
                            }
                        }
                    }
                }
                return Ok(JsonConvert.SerializeObject(responseDirecciones));
            }
            catch (HttpRequestException e) 
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre) 
            {
                throw new Exception("API INEGI no disponible.");
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult getDireccionSegmentoAux([FromBody] INEGIApiBuscaDestioRequest getListaParams)
        {
            try
            {
                var dbAccess = new DataBaseAccess(connStr);
                List<INEGIApiBuscaDestioResponse> responseDirecciones = new List<INEGIApiBuscaDestioResponse>();
                if (getListaParams.lugarBusqueda != null)
                {
                    if (!getListaParams.lugarBusqueda.Equals(""))
                    {
                        //Valida si está buscando Dirección para Plaza y ya existe en DB
                        if (getListaParams.idPlaza > 0)
                        {
                            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
                            DataTable dtPlazas = dbAccess.ExecuteQuery("sp_get_catPlazas_GetPlazaINEGI", postParams).Tables[0];
                            if (dtPlazas.Rows.Count > 0)
                            {
                                INEGIApiBuscaDestioResponse responseRow = new INEGIApiBuscaDestioResponse();
                                responseRow.lon = Decimal.Parse(dtPlazas.Rows[0]["LonINEGI"].ToString());
                                responseRow.lat = Decimal.Parse(dtPlazas.Rows[0]["LatINEGI"].ToString());
                                responseRow.id = int.Parse(dtPlazas.Rows[0]["IdINEGI"].ToString());
                                responseRow.direccion = dtPlazas.Rows[0]["DireccionINEGI"].ToString();
                                responseDirecciones.Add(responseRow);
                            }
                            else
                            {
                                //Busca la plaza en INEGI; al encontrarla, guarda la información en DB para futuras búsquedas
                                var requestAPIPArams = new Dictionary<string, string>();
                                requestAPIPArams.Add("buscar", getListaParams.lugarBusqueda);
                                requestAPIPArams.Add("proj", "GRS80");
                                requestAPIPArams.Add("num", "1");
                                //Llama el API de búsqueda de Lugares INEGI
                                var dicResponseAPI = callApiINEGIDeserializeAux(buscaDestinoURL, requestAPIPArams);
                                //Obtiene el diccionario de búsqueda
                                if (dicResponseAPI.Count > 1)
                                {
                                    var dicResponseData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dicResponseAPI["data"].ToString());
                                    foreach (Dictionary<string, object> direccions in dicResponseData)
                                    {
                                        INEGIApiBuscaDestioResponse responseRow = new INEGIApiBuscaDestioResponse();
                                        foreach (KeyValuePair<string, object> detalleDireccion in direccions)
                                        {
                                            var coordendas = JsonConvert.DeserializeObject<Dictionary<string, object>>(direccions["geojson"].ToString());
                                            responseRow.lon = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).First.ToString());
                                            responseRow.lat = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).Last.ToString());
                                            responseRow.id = int.Parse(direccions["id_dest"].ToString());
                                            responseRow.direccion = direccions["nombre"].ToString();
                                        }
                                        responseDirecciones.Add(responseRow);
                                        //Ingresa el resultado de la plaza
                                        Dictionary<string, object> paramsActualizaPlaza = new Dictionary<string, object>();
                                        paramsActualizaPlaza.Add("idPlaza", getListaParams.idPlaza);
                                        paramsActualizaPlaza.Add("IdINEGI", responseRow.id);
                                        paramsActualizaPlaza.Add("DireccionINEGI", responseRow.direccion);
                                        paramsActualizaPlaza.Add("LatINEGI", responseRow.lat);
                                        paramsActualizaPlaza.Add("LonINEGI", responseRow.lon);
                                        dbAccess.ExecuteNonQuery("sp_update_catPlazas_InfoINEGI", paramsActualizaPlaza);
                                    }
                                }
                                else
                                {
                                    throw new Exception("API INEGI no disponible.");
                                }
                            }
                        }
                        else
                        {
                            //Busqueda de autocomplete y despliegue de 5 resultados
                            var requestAPIPArams = new Dictionary<string, string>();
                            requestAPIPArams.Add("buscar", getListaParams.lugarBusqueda);
                            requestAPIPArams.Add("proj", "GRS80");
                            requestAPIPArams.Add("num", "5");
                            //Llama el API de búsqueda de Lugares INEGI
                            var dicResponseAPI = callApiINEGIDeserializeAux(buscaDestinoURL, requestAPIPArams);
                            //Obtiene el diccionario de búsqueda
                            if (dicResponseAPI.Count > 1)
                            {
                                var dicResponseData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dicResponseAPI["data"].ToString());
                                foreach (Dictionary<string, object> direccions in dicResponseData)
                                {
                                    INEGIApiBuscaDestioResponse responseRow = new INEGIApiBuscaDestioResponse();
                                    foreach (KeyValuePair<string, object> detalleDireccion in direccions)
                                    {
                                        var coordendas = JsonConvert.DeserializeObject<Dictionary<string, object>>(direccions["geojson"].ToString());
                                        responseRow.lon = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).First.ToString());
                                        responseRow.lat = Decimal.Parse(((Newtonsoft.Json.Linq.JContainer)coordendas["coordinates"]).Last.ToString());
                                        responseRow.id = int.Parse(direccions["id_dest"].ToString());
                                        responseRow.direccion = direccions["nombre"].ToString();
                                    }
                                    responseDirecciones.Add(responseRow);
                                }
                            }
                            else
                            {
                                throw new Exception("API INEGI no disponible.");
                            }
                        }
                    }
                }
                return Ok(JsonConvert.SerializeObject(responseDirecciones));
            }
            catch (HttpRequestException e)
            {
                throw new Exception("API INEGI no disponible.");
            }
            catch (JsonReaderException jre)
            {
                throw new Exception("API INEGI no disponible.");
            }
        }

        //API para obtener los costos y trazado de ruta en cotizador
        [Route("[action]")]
        [HttpPost]
        public IActionResult getRuta([FromBody] getRutaRequest getListaParams)
        {
            
            var dbAccess = new DataBaseAccess(connStr);
            var dicAPIRequest = new Dictionary<string, string>();
            var dicEmpresaConfig = new Dictionary<string, object>();
            string strPlazaOrigen = "";
            string strPlazaDestino = "";
            string dbLatPlazaOrigen = "";
            string dbLonPlazaOrigen = "";
            string dbLatPlazaDestino = "";
            string dbLonPlazaDestino = "";
            int idCombustibleTarifaOrigen = 0;
            int idCombustibleTarifaDestino = 0;
            int vehiculoIdCombustibleTipo = 0;
            int vehiculoNoEjes = 0;
            int vehiculoTipoUnidad = 0;
            double costoCasetasAPI = 0;
            double totalRutaKms = 0;
            double tiempoRuta = 0;
            string infoAdvertencias = "Ninguna.";

            #region Info Plazas
            //Convierte Parámetros de entrada en Dic
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            //Obtiene la información de plaza Origen y Plaza Destino
            DataTable dtPlazas = dbAccess.ExecuteQuery("sp_get_catPlazas_getPosicionesOrigenDestino", postParams).Tables[0];
            if (dtPlazas.Rows.Count > 0)
            {
                //Datos plaza Origen
                strPlazaOrigen = dtPlazas.Rows[0]["plazaOrigen"].ToString();
                dbLatPlazaOrigen = dtPlazas.Rows[0]["origenLat"].ToString(); 
                dbLonPlazaOrigen = dtPlazas.Rows[0]["origenLon"].ToString();
                idCombustibleTarifaOrigen = Int32.Parse(dtPlazas.Rows[0]["idCombustibleTarifaOrigen"].ToString());
                //Datos plaza Destino
                strPlazaDestino = dtPlazas.Rows[0]["plazaDestino"].ToString();
                dbLatPlazaDestino = dtPlazas.Rows[0]["destinoLat"].ToString(); 
                dbLonPlazaDestino = dtPlazas.Rows[0]["destinoLon"].ToString();
                idCombustibleTarifaDestino = Int32.Parse(dtPlazas.Rows[0]["idCombustibleTarifaDestino"].ToString());
            }
            #endregion

            //Valida si hay respuesta del origen y el destino para continuar*/
            getRutaResponse rutaResponse = new getRutaResponse();
            if (getListaParams.idSegmentoOrigen > 0 && getListaParams.idSegmentoDestino > 0)
            {
                //Test Errors
                //dbAccess.getErrorMessage("XR-00156", new object[] { "Monterrey NL", "Ruta Monterrey - Torreon" }); //Error en el origen de la ruta

                //throw new Exception("Exception directa!.");

                //Cálculo de Ruta y Costos
                #region URL a utilizar
                //Obtiene los identificadores e Origen / Destino
                string strIdINEGIOrigen = getListaParams.idSegmentoOrigen.ToString();
                string strIdINEGIDestino = getListaParams.idSegmentoDestino.ToString();
                string urlCalculoRuta = "";
                string urlCalculoDetalleRuta = "";
                //Determina URL a utilizar al Tipo de Carretera seleccionado
                switch (getListaParams.idTipoCarretera)
                {
                    case 1: //Cuota
                        urlCalculoRuta = calculoRutaCuota;
                        urlCalculoDetalleRuta = calculoDetalleRutaCuota;
                        break;
                    case 2: //Libre
                        urlCalculoRuta = calculoRutaLibre;
                        urlCalculoDetalleRuta = calculoDetalleRutaLibre;
                        break;
                    case 3: //Sugerida
                        urlCalculoRuta = calculoRutaOptima;
                        urlCalculoDetalleRuta = calculoDetalleRutaOptima;
                        break;
                    default:
                        break;
                }
                #endregion

                #region Información Vehículo
                //Obtiene el número de ejes del vehículo seleccionado y tipo de combustible
                DataTable dtVehiculoInfo = dbAccess.ExecuteQuery("sp_get_catVehiculoArmado_GetEjesById", postParams).Tables[0];
                if (dtVehiculoInfo.Rows.Count > 0)
                {
                    vehiculoIdCombustibleTipo = int.Parse(dtVehiculoInfo.Rows[0]["idCombustibleTipo"].ToString());
                    vehiculoNoEjes = int.Parse(dtVehiculoInfo.Rows[0]["numEjes"].ToString());
                    vehiculoTipoUnidad = int.Parse(dtVehiculoInfo.Rows[0]["tipoUnidad"].ToString());
                }
                #endregion

                #region Información Configuración de Empresa
                //Obtiene la información de la Configuración de Empresa para saber los valores a calcular
                DataTable dtConfiguracionEmpresa = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracion_CostosVariablesByEmpresa", postParams).Tables[0];
                if (dtConfiguracionEmpresa.Rows.Count > 0)
                {
                    dicEmpresaConfig = page.DataTableToMapRow(dtConfiguracionEmpresa);
                }
                #endregion

                #region Calcula Ruta con Puntos Intermedios
                //Obtiene un diccionario para todos los tramos 
                var dicPuntosRutaINEGI = new Dictionary<string, string>();
                int contParadas = 1;
                dicPuntosRutaINEGI.Add("origen", strIdINEGIOrigen);
                foreach (getRutaSegmentosRequest parada in getListaParams.segmentos)
                {
                    dicPuntosRutaINEGI.Add("parada" + contParadas.ToString(), parada.idSegmentoINEGI.ToString());
                    contParadas++;
                }
                dicPuntosRutaINEGI.Add("destino", strIdINEGIDestino);

                //Pocesa la ruta en base a Origen, Destino y Puntos Intermedios
                string puntoINEGIAnterior = "";
                var procesaDetalleRuta = false;
                foreach (KeyValuePair<string, string> point in dicPuntosRutaINEGI)
                {
                    //Valida si es el primer punto para continuar y tener siempre la secuencia de anterior y actual para cálculo de ruta
                    if (puntoINEGIAnterior.Equals(""))
                    {
                        puntoINEGIAnterior = point.Value;
                        continue;
                    }
                    #region Obtiene la Ruta entre puntos y los costos de Combustible y Casetas
                    //Llama cálculo de ruta Origen / Destino
                    dicAPIRequest = new Dictionary<string, string>();
                    dicAPIRequest.Add("dest_i", puntoINEGIAnterior);
                    dicAPIRequest.Add("dest_f", point.Value);
                    dicAPIRequest.Add("proj", "MERC");
                    dicAPIRequest.Add("v", vehiculoNoEjes.ToString()); //Tipo de Vehículo
                    var responseCalculoRuta = callApiINEGIDeserialize(urlCalculoRuta, dicAPIRequest);
                    var costosCalculoRuta = new Dictionary<string, object>();
                    var procesoRutaAutomatico = true;
                    try
                    {
                        if (responseCalculoRuta.Count > 1)
                        {
                            costosCalculoRuta = JsonConvert.DeserializeObject<Dictionary<string, object>>(responseCalculoRuta["data"].ToString());
                        }
                        else
                        {
                            procesoRutaAutomatico = false;
                        }
                    }
                    catch (HttpRequestException e)
                    {
                        procesoRutaAutomatico = false;
                    }
                    catch (JsonReaderException jre)
                    {
                        procesoRutaAutomatico = false;
                    }
                    //Suma variables de cálculo de rutas
                    if (procesoRutaAutomatico)
                    {
                        totalRutaKms += double.Parse(costosCalculoRuta["long_km"].ToString());
                        costoCasetasAPI += double.Parse(costosCalculoRuta["costo_caseta"].ToString());
                        tiempoRuta += double.Parse(costosCalculoRuta["tiempo_min"].ToString());
                        if (!costosCalculoRuta["advertencia"].ToString().Equals(""))
                        {
                            if (infoAdvertencias.Equals("Ninguna."))
                            {
                                infoAdvertencias = costosCalculoRuta["advertencia"].ToString();
                            }
                            else
                            {
                                infoAdvertencias += ", " + costosCalculoRuta["advertencia"].ToString();
                            }
                        }
                    }
                    else
                    {
                        totalRutaKms = double.Parse(getListaParams.kms.ToString());
                    }
                    #endregion

                    #region Detalle de Ruta
                    dicAPIRequest = new Dictionary<string, string>();
                    dicAPIRequest.Add("dest_i", puntoINEGIAnterior);
                    dicAPIRequest.Add("dest_f", point.Value);
                    dicAPIRequest.Add("proj", "MERC");
                    dicAPIRequest.Add("v", vehiculoNoEjes.ToString()); //Tipo de Vehículo
                    var responseCalculoRutaDetalle = callApiINEGIDeserialize(urlCalculoDetalleRuta, dicAPIRequest);
                    //var detalleRuta = new Dictionary<string, object>();
                    
                    double auxcostoCasetasAPI = 0;
                    try
                    {
                        if (responseCalculoRutaDetalle.Count > 1)
                        {
                            procesaDetalleRuta = true;
                            rutaResponse.casetasRuta = "";
                            var detalleRuta = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseCalculoRutaDetalle["data"].ToString());
                            int countRowCaseta = 1;
                            foreach (Dictionary<string, object> puntoDetalleRuta in detalleRuta)
                            {
                                var puntoCostoCaseta = int.Parse(puntoDetalleRuta["costo_caseta"].ToString());
                                if (puntoCostoCaseta > 0)
                                {
                                    //Está pasando por una caseta y obtiene la información
                                    var cruceCaseta = puntoDetalleRuta["direccion"].ToString();
                                    if (rutaResponse.casetasRuta.Equals(""))
                                    {
                                        rutaResponse.casetasRuta = "(" + countRowCaseta.ToString() + ") " + cruceCaseta + " $ " + puntoCostoCaseta.ToString();
                                    }
                                    else
                                    {
                                        rutaResponse.casetasRuta += ". (" + countRowCaseta.ToString() + ") " + cruceCaseta + " $ " + puntoCostoCaseta.ToString();
                                    }
                                    auxcostoCasetasAPI += double.Parse(puntoCostoCaseta.ToString());
                                    countRowCaseta++;
                                }
                            }
                        }
                        else
                        {
                            procesaDetalleRuta = false;
                        }
                    }
                    catch (HttpRequestException e)
                    {
                        procesaDetalleRuta = false;
                    }
                    catch (JsonReaderException jre)
                    {
                        procesaDetalleRuta = false;
                    }
                    //Valida si obtiene costo de caseta
                    //costoCasetasAPI
                    if (costoCasetasAPI == 0 && auxcostoCasetasAPI > 0)
                    {
                        costoCasetasAPI = auxcostoCasetasAPI;
                    }
                    #endregion
                }
                #endregion

                #region Obtiene los costos de Combustible de API
                //Se llama API de Combustible para obtener la información de costos
                dicAPIRequest = new Dictionary<string, string>();
                var responseCombustible = callApiINEGIDeserializeAux(combustibleURL, dicAPIRequest);
                var procesoCombustibleAutomatico = true;
                var costosCombustible = new List<Dictionary<string, object>>();
                try
                {
                    if (responseCombustible.Count > 1)
                    {
                        costosCombustible = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseCombustible["data"].ToString());
                    }
                    else
                    {
                        procesoCombustibleAutomatico = false;
                    }
                }
                catch (HttpRequestException e)
                {
                    procesoCombustibleAutomatico = false;
                }
                catch (JsonReaderException jre)
                {
                    procesoCombustibleAutomatico = false;
                }
                double precioCombustible = 0;
                string infoPrecioCombustible = "";
                /* Tipos de Combustible en configuración de Armado de Vehículo (IdCombustibleTipo)
                 * 1	Diesel
                 * 2	Premium
                 * 3	Regular
                 * 4	Gas
                 */
                if (procesoCombustibleAutomatico)
                {
                    foreach (Dictionary<string, object> combustible in costosCombustible)
                    {
                        switch (combustible["tipo"].ToString())
                        {
                            case "Magna":
                                if (vehiculoIdCombustibleTipo == 3)
                                {
                                    precioCombustible = double.Parse(combustible["costo"].ToString());
                                    infoPrecioCombustible = "$" + String.Format("{0:0.00}", precioCombustible) + " lt. (Regular)";
                                    rutaResponse.costoCombustible = precioCombustible;
                                }
                                break;
                            case "Premium":
                                if (vehiculoIdCombustibleTipo == 2)
                                {
                                    precioCombustible = double.Parse(combustible["costo"].ToString());
                                    infoPrecioCombustible = "$" + String.Format("{0:0.00}", precioCombustible) + " lt. (Premium)";
                                    rutaResponse.costoCombustible = precioCombustible;
                                }
                                break;
                            case "Diésel":
                                if (vehiculoIdCombustibleTipo == 1)
                                {
                                    precioCombustible = double.Parse(combustible["costo"].ToString());
                                    infoPrecioCombustible = "$" + String.Format("{0:0.00}", precioCombustible) + " lt. (Diésel)";
                                    rutaResponse.costoCombustible = precioCombustible;
                                }
                                break;
                            case "Gas":
                                if (vehiculoIdCombustibleTipo == 4)
                                {
                                    precioCombustible = double.Parse(combustible["costo"].ToString());
                                    infoPrecioCombustible = "$" + String.Format("{0:0.00}", precioCombustible) + " lt. (Gas)";
                                    rutaResponse.costoCombustible = precioCombustible;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
                #endregion


                #region Realiza los cálculos de costos Variables
                //****Regresa los valores de los costos ya calculados (procesados en base a configuración y resultados de la ruta)****
                //Obtiene la información de Costos Variabes
                /*
                 * Tipos de Cálculo:
                 * -Combustible: 
                 *      0 - No aplica
                 *      1 - Automático (de APIS INEGI y Rendimiento)
                 *      2 - Cálculo por Tarifa (de Catálogo de Tarifas)
                 *      3 - Histórico ZAM
                 *      4 - Manual (factores configurados)
                 *          * idCostoTipo 1: Costo KMS
                 *          * idCostoTipo 2: Tipo Fijo
                 */
                //Variables Combustible
                //double costoCombustible = 0.0;
                double costoCombustibleAutomatico = 0.0;
                string tooltipCostoCombustibleAutomatico = "";
                double costoCombustibleTarifa = 0.0;
                string tooltipCostoCombustibleTarifa = "";
                double costoCombustibleHistorico = 0.0;
                string tooltipCostoCombustibleHistorico = "";
                double costoCombustibleManual = 0.0;
                string tooltipCostoCombustibleManual = "";
                double historicoCombustible = 0.0;
                double historicoCaseta = 0.0;
                double historicoSueldo = 0.0;
                //Obtiene los históricos 
                var paramsHistoricosEmpresa = new Dictionary<string, object>();
                paramsHistoricosEmpresa.Add("idEmpresa", getListaParams.idEmpresa);
                DataTable dtHistoricosEmpresa = dbAccess.ExecuteQuery("sp_get_catEmpresaHistoricos_GetHistoricos", paramsHistoricosEmpresa).Tables[0];
                //Obtiene el combustible
                string infoRutaRendimiento = String.Format("{0:0.0}", getListaParams.rendimiento) + " km/lt.";
                if (int.Parse(dicEmpresaConfig["Var_ComCalculoAuto"].ToString()) == 1 && procesoCombustibleAutomatico == true) //getListaParams.tipoCalculoCombustible == 1
                {
                    //El calculo es autómatico y lo obtiene de API: (KMS / Rendimiento) * CostoCombustible
                    rutaResponse.costoCombustibleAutomatico = (Convert.ToInt32(totalRutaKms) / getListaParams.rendimiento) * precioCombustible;
                    //Genera el Tooltip
                    rutaResponse.tooltipCostoCombustibleAutomatico = "Total KMS de Ruta " + totalRutaKms + " / Rendimiento de Unidad " + getListaParams.rendimiento + " * Precio de Combustible: " + precioCombustible;
                }
                if (int.Parse(dicEmpresaConfig["Var_ComTarifa"].ToString()) == 1) //getListaParams.tipoCalculoCombustible == 2
                {
                    //Cálculo por Tarifa; obtiene del tarifario el costo del Combustible
                    var postParamsTarifas = new Dictionary<string, object>();
                    postParamsTarifas.Add("idCombustibleTarifa", getListaParams.idCombustibleTarifa);
                    postParamsTarifas.Add("idCombustibleTipo", vehiculoIdCombustibleTipo);
                    DataTable dtTarifaCombustible = dbAccess.ExecuteQuery("sp_get_catCombustibleTarifas_GetTarifa", postParamsTarifas).Tables[0];
                    if (dtTarifaCombustible.Rows.Count > 0)
                    {
                        rutaResponse.costoCombustibleTarifa = (Convert.ToInt32(totalRutaKms) / getListaParams.rendimiento) * double.Parse(dtTarifaCombustible.Rows[0]["costoCombustible"].ToString());
                        //Genera el Tooltip
                        rutaResponse.tooltipCostoCombustibleTarifa = "Total KMS de Ruta " + totalRutaKms + " / Rendimiento de Unidad " + getListaParams.rendimiento + " * Precio de Combustible: " + String.Format("{0:0.00}", double.Parse(dtTarifaCombustible.Rows[0]["costoCombustible"].ToString()));
                    }
                    else
                    {
                        //No se encontro la tarifa y obtiene el combustible con el costo de API 
                        rutaResponse.costoCombustibleTarifa = (Convert.ToInt32(totalRutaKms) / getListaParams.rendimiento) * precioCombustible;
                        //Genera el Tooltip
                        rutaResponse.tooltipCostoCombustibleTarifa = "Total KMS de Ruta " + totalRutaKms + " / Rendimiento de Unidad " + getListaParams.rendimiento + " * Precio de Combustible: " + precioCombustible;
                    }
                    //Si no se pudo calcúlar el combustible automático, obtiene la descripción en base al de Tarifas.
                    if (infoPrecioCombustible.Equals(""))
                    {
                        infoPrecioCombustible = "$" + String.Format("{0:0.00}", double.Parse(dtTarifaCombustible.Rows[0]["costoCombustible"].ToString())) + " lt. (Diésel)";
                    }
                }
                if (int.Parse(dicEmpresaConfig["Var_ComHistorico"].ToString()) == 1) 
                {
                    //Cálculo de tarifa por Histórico ZAM
                    if(dtHistoricosEmpresa.Rows.Count > 0)
                    {
                        rutaResponse.costoCombustibleHistorico = (Convert.ToInt32(totalRutaKms) / getListaParams.rendimiento) * double.Parse(dtHistoricosEmpresa.Rows[0]["historicoCombustible"].ToString());
                        //Genera el Tooltip
                        rutaResponse.tooltipCostoCombustibleHistorico = "Total KMS de Ruta " + totalRutaKms + " / Rendimiento de Unidad " + getListaParams.rendimiento + " * Precio de Combustible: " + double.Parse(dtHistoricosEmpresa.Rows[0]["historicoCombustible"].ToString());
                    }
                    else
                    {
                        rutaResponse.costoCombustibleHistorico = (Convert.ToInt32(totalRutaKms) / getListaParams.rendimiento) * 0;
                    }
                }
                if (int.Parse(dicEmpresaConfig["Var_ComManual"].ToString()) == 1) 
                {
                    //Cálculo de Combustible Manual
                    if (int.Parse(dicEmpresaConfig["Var_ComIdCostoTipo"].ToString()) == 1)
                    {
                        //Obtiene el Costo por kms de la ruta: Factor * KMS
                        rutaResponse.costoCombustibleManual = (Convert.ToInt32(totalRutaKms) * double.Parse(dicEmpresaConfig["Var_ComFactor"].ToString())); //getListaParams.tipoCalculoCombustibleTarifa
                        infoRutaRendimiento = "No Aplica";
                        //Genera el Tooltip
                        rutaResponse.tooltipCostoCombustibleManual = "Total KMS de Ruta " + totalRutaKms + " * Factor configurado por KM " + double.Parse(dicEmpresaConfig["Var_ComFactor"].ToString());
                    }
                    else
                    {
                        //Monto Fijo; no realiza ningún cálculo
                        rutaResponse.costoCombustibleManual = double.Parse(dicEmpresaConfig["Var_ComFactor"].ToString());
                        infoRutaRendimiento = "No Aplica";
                        rutaResponse.tooltipCostoCombustibleManual = "Factor configurado Fijo " + double.Parse(dicEmpresaConfig["Var_ComFactor"].ToString());
                    }
                }
                //Variables Casetas
                /*
                 * -Casetas:
                 *      0 - No aplica
                 *      1 - Automático (de APIS INEGI)
                 *      2 - Histótico ZAM
                 *      3 - Archivo (archivos de IAVE o TXT General)
                 *      4 - Manual (factores configurados)
                 *          * idCostoTipo 1: Costo KMS
                 *          * idCostoTipo 2: Tipo Fijo
                */
                //double costoCasetas = 0.0;
                double costoCasetasAutomatico = 0.0;
                double costoCasetasHistorico = 0.0;
                double costoCasetasArchivo = 0.0;
                double costoCasetasManual = 0.0;
                //Obtiene Casetas
                if (int.Parse(dicEmpresaConfig["Var_CasCalculoAuto"].ToString()) == 1)
                {
                    //Cálculo automático (obtenido de API INEGI)
                    rutaResponse.costoCasetasAutomatico = costoCasetasAPI;
                    //Genera el Tooltip
                    if (procesaDetalleRuta)
                    {
                        rutaResponse.tooltipCostoCasetasAutomatico = "Costo obtenido de la Ruta: " + String.Format("{0:0.00}", costoCasetasAPI) + ". Detalle de Casetas: " + rutaResponse.casetasRuta;
                    }
                    else
                    {
                        rutaResponse.tooltipCostoCasetasAutomatico = "Costo obtenido de la Ruta: " + String.Format("{0:0.00}", costoCasetasAPI);
                    }
                }
                if (int.Parse(dicEmpresaConfig["Var_CasHistorico"].ToString()) == 1)
                {
                    //Cálculo de costo por Histórico ZAM
                    if (dtHistoricosEmpresa.Rows.Count > 0)
                    {
                        rutaResponse.costoCasetasHistorico = double.Parse(dtHistoricosEmpresa.Rows[0]["historicoCaseta"].ToString());
                        //Genera el Tooltip
                        rutaResponse.tooltipCostoCasetasHistorico = "Cálculo obtenido de Histórico en ZAM.";
                    }
                    else
                    {
                        rutaResponse.costoCasetasHistorico = 0;
                    }
                }
                if (int.Parse(dicEmpresaConfig["Var_CasArchivo"].ToString()) == 1)
                {
                    //Cálculo de costo por archivos cargados (IAVE, TXT, etc...)
                    var postParamsCasetasRuta = new Dictionary<string, object>();
                    postParamsCasetasRuta.Add("idRuta", getListaParams.idRuta);
                    DataTable dtCostoCasetasRuta = dbAccess.ExecuteQuery("sp_get_catRuta_GetCostoCasetas", postParamsCasetasRuta).Tables[0];
                    if(dtCostoCasetasRuta.Rows.Count > 0)
                    {
                        rutaResponse.costoCasetasArchivo = double.Parse(dtCostoCasetasRuta.Rows[0]["costoCasetas"].ToString());
                        //Genera el Tooltip
                        rutaResponse.tooltipCostoCasetasArchivo = "Cálculo obtenido de los costos configurados en Rutas y Casetas de ZAM.";
                    }
                    else
                    {
                        rutaResponse.costoCasetasArchivo = 0; //TODO: Aún no existe carga de archivos
                    }
                    
                }
                if (int.Parse(dicEmpresaConfig["Var_CasManual"].ToString()) == 1)
                {
                    //Cálculo de Casetas Manual
                    if (int.Parse(dicEmpresaConfig["Var_CasIdCostoTipo"].ToString()) == 1)
                    {
                        //Obtiene el Costo por kms de la ruta: Factor * KMS
                        rutaResponse.costoCasetasManual = (Convert.ToInt32(totalRutaKms) * double.Parse(dicEmpresaConfig["Var_CasFactor"].ToString())); //getListaParams.tipoCalculoCasetasTarifa
                    }
                    else
                    {
                        //Monto Fijo; no realiza ningún cálculo
                        rutaResponse.costoCasetasManual = double.Parse(dicEmpresaConfig["Var_CasFactor"].ToString());
                    }
                }
                /*
                 * -Sueldos:
                 *      0 - No aplica
                 *      1 - Histórico ZAM
                 *      2 - Manual (factores configurados)
                 *          * idCostoTipo 1: Costo KMS
                 *          * idCostoTipo 2: Tipo Fijo
                */
                //Variables Sueldos
                //double costoSueldo = 0.0;
                double costoSueldoHistorico = 0.0;
                double costoSueldoManual = 0.0;
                //Obtiene los sueldos
                /*if (int.Parse(dicEmpresaConfig["Var_SueHistorico"].ToString()) == 1)
                {
                    //Cálculo de costo por Histórico ZAM
                    if (dtHistoricosEmpresa.Rows.Count > 0)
                    {
                        rutaResponse.costoSueldoHistorico = double.Parse(dtHistoricosEmpresa.Rows[0]["historicoSueldo"].ToString());
                    }
                    else
                    {
                        rutaResponse.costoSueldoHistorico = 0; 
                    }
                }
                if (int.Parse(dicEmpresaConfig["Var_SueManual"].ToString()) == 1)
                {
                    //Cálculo de Sueldo Manual
                    if (int.Parse(dicEmpresaConfig["Var_SueIdCostoTipo"].ToString()) == 1)
                    {
                        //Obtiene el Sueldo por kms de la ruta: Factor * KMS
                        rutaResponse.costoSueldoManual = (Convert.ToInt32(totalRutaKms) * double.Parse(dicEmpresaConfig["Var_SueFactor"].ToString())); //getListaParams.tipoCalculoSueldoTarifa
                    }
                    else
                    {
                        //Monto Fijo; no realiza ningún cálculo
                        rutaResponse.costoSueldoManual = double.Parse(dicEmpresaConfig["Var_SueFactor"].ToString());
                    }
                }*/
                //Obtiene el sueldo en base a rangos configurados y parámetros actuales de viaje (KMS y TipoUnidad)
                var paramsSueldos = new Dictionary<string, object>();
                paramsSueldos.Add("idEmpresa", getListaParams.idEmpresa);
                paramsSueldos.Add("tipoUnidad", vehiculoTipoUnidad);
                paramsSueldos.Add("kms", totalRutaKms);
                DataTable dtConfigSueldos = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracionVariablesSueldos_GetConfiguracionSueldos", paramsSueldos).Tables[0];
                if(dtConfigSueldos.Rows.Count > 0)
                {
                    //double.Parse(dtConfigSueldos.Rows[0]["factor"].ToString()
                    rutaResponse.costoSueldoManual = (Convert.ToInt32(totalRutaKms) * double.Parse(dtConfigSueldos.Rows[0]["factor"].ToString()));
                    //Genera el Tooltip
                    rutaResponse.tooltipCostoSueldoManual = "Total de KMS de ruta  " + totalRutaKms + " * Factor configurado por rango " + double.Parse(dtConfigSueldos.Rows[0]["factor"].ToString());
                }
                #endregion

                List<getRutaCostosResponse> rutaCostosResponse = new List<getRutaCostosResponse>();
                getRutaCostosResponse rutaCosto = new getRutaCostosResponse();

                #region Realiza los cálculos de costos Fijos
                //Obtiene la información de Costos Fijos
                double costoMantenimiento = 0.0;
                double costoLlantas = 0.0;
                double costoInsumos = 0.0;

                var paramsCostosFijos = new Dictionary<string, object>();
                paramsCostosFijos.Add("idEmpresa", getListaParams.idEmpresa);
                paramsCostosFijos.Add("kms", totalRutaKms);
                DataTable dtCostosFijosConfigurados = dbAccess.ExecuteQuery("sp_get_catEmpresaConfiguracionCostosFijos_getCostos", paramsSueldos).Tables[0];
                if (dtCostosFijosConfigurados.Rows.Count > 0)
                {
                    foreach (DataRow costoFijo in dtCostosFijosConfigurados.Rows)
                    {
                        rutaCosto = new getRutaCostosResponse();
                        rutaCosto.id = costoFijo["id"].ToString();
                        rutaCosto.monto = String.Format("{0:0.00}", double.Parse(costoFijo["monto"].ToString())); //costoCombustible;
                        rutaCosto.tipo = costoFijo["tipo"].ToString();
                        rutaCostosResponse.Add(rutaCosto);
                    }
                    
                }

                //TODO: Se omite costos fijos ya que se desarrollará un nuevo catálogo para sus cálculos
                /*
                var reqParamsCostos = new Dictionary<string, object>();
                reqParamsCostos.Add("idEmpresa", 1); //Agregar a parámetro
                reqParamsCostos.Add("kms", Convert.ToInt32(totalRutaKms));
                DataTable dtInfoCostos = dbAccess.ExecuteQuery("sp_get_prCotizador_CostosIniciales", reqParamsCostos).Tables[0];
                foreach (DataRow costo in dtInfoCostos.Rows)
                {
                    switch (costo["id"].ToString())
                    {
                        case "mantenimiento":
                            costoMantenimiento = double.Parse(costo["monto"].ToString());
                            break;
                        case "llantas":
                            costoLlantas = double.Parse(costo["monto"].ToString());
                            break;
                        case "insumos":
                            costoInsumos = double.Parse(costo["monto"].ToString());
                            break;
                    }
                }
                */
                #endregion

                #region Agrega Costos a Response
                
                //Combustible
                rutaCosto = new getRutaCostosResponse();
                rutaCosto.id = "Combustible";
                rutaCosto.monto = String.Format("{0:0.00}", 0); //costoCombustible;
                rutaCosto.tipo = "variable";
                rutaCostosResponse.Add(rutaCosto);
                //Casetas
                rutaCosto = new getRutaCostosResponse();
                rutaCosto.id = "Casetas";
                rutaCosto.monto = String.Format("{0:0.00}", 0);  //costoCasetas;
                rutaCosto.tipo = "variable";
                rutaCostosResponse.Add(rutaCosto);
                //Sueldo
                rutaCosto = new getRutaCostosResponse();
                rutaCosto.id = "Sueldo";
                rutaCosto.monto = String.Format("{0:0.00}", 0); //costoSueldo
                rutaCosto.tipo = "variable";
                rutaCostosResponse.Add(rutaCosto);
                //Valida si calcula UREA
                if(getListaParams.idUreaTarifa > 0)
                {
                    var paramsCostoUREA = new Dictionary<string, object>();
                    paramsCostoUREA.Add("idEmpresa", getListaParams.idEmpresa);
                    paramsCostoUREA.Add("idUreaTarifa", getListaParams.idUreaTarifa);
                    DataTable dtCostoUREA = dbAccess.ExecuteQuery("sp_get_catUreaTarifas_costoById", paramsCostoUREA).Tables[0];
                    if (dtCostoUREA.Rows.Count > 0)
                    {
                        rutaCosto = new getRutaCostosResponse();
                        rutaCosto.id = "UREA";
                        rutaCosto.monto = String.Format("{0:0.00}", (Convert.ToInt32(totalRutaKms) / getListaParams.rendimientoUrea) * double.Parse(dtCostoUREA.Rows[0]["costo"].ToString())); 
                        rutaCosto.tipo = "variable";
                        rutaCostosResponse.Add(rutaCosto);
                    }
                }
                //Mantenimiento
                //rutaCosto = new getRutaCostosResponse();
                //rutaCosto.id = "Mantenimiento";
                //rutaCosto.monto = String.Format("{0:0.00}", costoMantenimiento);
                //rutaCosto.tipo = "fijo";
                //rutaCostosResponse.Add(rutaCosto);
                ////Llantas
                //rutaCosto = new getRutaCostosResponse();
                //rutaCosto.id = "Llantas";
                //rutaCosto.monto = String.Format("{0:0.00}", costoLlantas);
                //rutaCosto.tipo = "fijo";
                //rutaCostosResponse.Add(rutaCosto);
                ////Insumos
                //rutaCosto = new getRutaCostosResponse();
                //rutaCosto.id = "Insumos";
                //rutaCosto.monto = String.Format("{0:0.00}", costoInsumos);
                //rutaCosto.tipo = "fijo";
                //rutaCostosResponse.Add(rutaCosto);
                //Agrega todos los costos calculados a Response
                rutaResponse.costos = rutaCostosResponse;
                #endregion

                #region Agrega Coordenadas de Ruta a Response
                //Regresa las indicaciones de ruta; las coordenadas regresadas en este apartado serán las que se trazarán en mapa
                List<getRutaCoordenadasResponse> rutaCoordenadasResponse = new List<getRutaCoordenadasResponse>();
                getRutaCoordenadasResponse coordenadas = new getRutaCoordenadasResponse();
                coordenadas.lat = getListaParams.idSegmentoOrigenLat;
                coordenadas.lon = getListaParams.idSegmentoOrigenLon;
                rutaCoordenadasResponse.Add(coordenadas);
                foreach (getRutaSegmentosRequest parada in getListaParams.segmentos)
                {
                    coordenadas = new getRutaCoordenadasResponse();
                    coordenadas.lat = Double.Parse(parada.posLat.ToString());
                    coordenadas.lon = Double.Parse(parada.posLon.ToString());
                    rutaCoordenadasResponse.Add(coordenadas);
                }
                coordenadas = new getRutaCoordenadasResponse();
                coordenadas.lat = getListaParams.idSegmentoDestinoLat;
                coordenadas.lon = getListaParams.idSegmentoDestinoLon;
                rutaCoordenadasResponse.Add(coordenadas);
                #endregion

                #region Agrega Infomración de Ruta a Response
                //Response de detalle (informativo) de la Ruta calculada
                rutaResponse.coordenadas = rutaCoordenadasResponse;
                rutaResponse.kms = Convert.ToInt32(totalRutaKms);
                rutaResponse.rendimiento = infoRutaRendimiento; //infoRutaRendimiento //Se determina dependiendo de la configuración del cálculo de Combustible
                int hours = (int)((tiempoRuta - tiempoRuta % 60) / 60);
                rutaResponse.tiempoEstimado = hours.ToString() + " hr. " + String.Format("{0:0}", (int)(tiempoRuta - (hours * 60))) + " min.";
                rutaResponse.costoCombustibleInfo = infoPrecioCombustible;
                rutaResponse.advertencias = infoAdvertencias;
                #endregion
            }
            else
            {
                //No se encontro Origen y/o Destino por lo menos; no se puede continuar
                if (getListaParams.idSegmentoOrigen == 0)
                {
                    throw new Exception("No se encontró el punto de Origen en Mapa. Favor de intentar llenando el campo de Referencia Destino");
                }
                else if (getListaParams.idSegmentoDestino == 0)
                {
                    throw new Exception("No se encontró el punto de Destino en Mapa. Favor de intentar llenando el campo de Referencia Origen");
                }
            }
            return Ok(JsonConvert.SerializeObject(rutaResponse));
        }

        //Funciones API para Cotizaciones
        //API Para Insertar - Actualizar
        [Route("[action]")]
        [HttpPost]
        public IActionResult setCotizacion([FromBody] setCotizacionModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString())); 
            dbAccess.ExecuteNonQuery("sp_set_prCotizacion", postParams);
            //Convierte el detalle de Model Contactos de Cliente a tipo Dictionary: envía la lista de detalle (contactos) y el listado de propiedades. 
            if (!postParams["idCotizacion"].ToString().Equals("") && !postParams["idCotizacion"].ToString().Equals("0"))
            {
                List<Dictionary<string, object>> dictDetalle = page.ListModelToDictionary<setCotizacionSegmentosModel>((List<setCotizacionSegmentosModel>)postParams["segmentos"], typeof(setCotizacionSegmentosModel).GetProperties());
                //Inserta detalle de SEGMENTOS: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
                page.InsertDetail(dbAccess, "idCotizacion", dictDetalle, "sp_set_prCotizacionSegmentos", "sp_delete_prCotizacionSegmentos", postParams["idCotizacion"].ToString(), true);
                dictDetalle = page.ListModelToDictionary<setCotizacionCostosModel>((List<setCotizacionCostosModel>)postParams["costos"], typeof(setCotizacionCostosModel).GetProperties());
                //Inserta detalle de COSTOS: parámetros (conexión, Id Llave de cabecera, lista diccionario de detalle, nombre SP Inserta, nombre SP elimina detalle, valor de ID Llave cabecera, booleano para indicar si asigna llave o no a detalle)
                page.InsertDetail(dbAccess, "idCotizacion", dictDetalle, "sp_set_prCotizacionCostos", "sp_delete_prCotizacionCostos", postParams["idCotizacion"].ToString(), true);
            }
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }

        //API Consulta listado
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCotizacionesLista([FromBody] getCotizacionesListaModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prCotizacion_Listado", postParams).Tables[0];
            string responseJSON = page.DataTableToJSON(dtInfo);
            return Ok(responseJSON);
        }

        //API Consulta por ID
        [Route("[action]")]
        [HttpPost]
        public IActionResult getCotizacionById([FromBody] getCotizacionByIdModel getListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(getListaParams);
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prCotizacion_CotizacionById", postParams).Tables[0];
            Dictionary<string, object> dicResult = page.DataTableToMapRow(dtInfo);
            DataTable dtInfoDetail = dbAccess.ExecuteQuery("sp_get_prCotizacionSegmentos_SegmentosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "segmentos");
            dtInfoDetail = dbAccess.ExecuteQuery("sp_get_prCotizacionCostos_CostosById", postParams).Tables[0];
            page.DataTableDetailListToJSONRowDictionary(dtInfoDetail, dicResult, "costos");
            string responseJSON = System.Text.Json.JsonSerializer.Serialize(dicResult);
            return Ok(responseJSON);
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult setCotizacionEstatus([FromBody] setCotizacionEstatusModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString()));
            dbAccess.ExecuteNonQuery("sp_set_prCotizacion_editaEstatus", postParams);
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult setCotizacionEstatusRechazado([FromBody] setCotizacionEstatusRechazadoModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString()));
            dbAccess.ExecuteNonQuery("sp_set_prCotizacion_rechazaCotizacion", postParams);
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult updateRefPlaza([FromBody] updateRefPlazaModel setListaParams)
        {
            var dbAccess = new DataBaseAccess(connStr);
            Dictionary<string, object> postParams = page.paramsDataClassToDic(setListaParams);
            //Inserta cabecera
            //postParams.Add("idUsuario", Int32.Parse(Request.Headers["idUsuario"].ToString()));
            dbAccess.ExecuteNonQuery("sp_update_catPlazas_InfoINEGI", postParams);
            return Ok(System.Text.Json.JsonSerializer.Serialize(postParams));
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult imprimirCotizacion([FromQuery] string idCotizaciones)
        {
            string html = generarHTML(idCotizaciones);

            HtmlToPdf converter = new HtmlToPdf();
            converter.Document.PageSize = PdfPageSize.A4;
            converter.Document.PageOrientation = PdfPageOrientation.Portrait;
            converter.Document.Margins = new PdfMargins(5);
            converter.SerialNumber = hiQpdfSerial;
            SetFooter(converter.Document);
            Byte[] pdfBuffer = converter.ConvertHtmlToMemory(html, "");

            string b64 = "data:application/pdf;base64," + Convert.ToBase64String(pdfBuffer);
            
            return Ok(JsonConvert.SerializeObject(b64));
        }

        

        public string generarHTML(string cotizaciones)
        {
            //Cotizaciones separadas por ","
            var dbAccess = new DataBaseAccess(connStr);
            var paramsCotizaciones = new Dictionary<string, string>();
            paramsCotizaciones.Add("idCotizaciones", cotizaciones);
            //Arreglo con cotizaciones de detalle en impresión
            DataTable dtInfo = dbAccess.ExecuteQuery("sp_get_prCotizacion_CotizacionesImpresion", paramsCotizaciones).Tables[0];

            string workingDirectory = Environment.CurrentDirectory;
            string pathImg = workingDirectory + "\\Imagenes\\";

            string destNombre = "Oscar Rivera Gaytan";
            string destPuesto = "Logistics Projects Coordinator";
            string destEmpresa = "MEXICHEM";

            string styles = "" +
                "<style>" +
                    "div, p, dd {font-family: sans-serif; font-size: 22pt} " +
                    "th, td {font-family: sans-serif; font-size: 16pt; border: 1px solid black; text-align: center;} " +
                "</style>";

            string html = "" +
                "<div style=\"width: 100%; text-align: right\"><img style=\"width: 65%\" src=\"" + pathImg + "autoquimicos_logo.png" + "\"></div><br><br>" +
                "<div style=\"width: 100%; text-align: right\"><b>Cordoba, Ver. " + DateTime.Now.ToString("M") + " del " + DateTime.Now.ToString("yyyy") + "</b></div><br>" +
                "<div><b>" + destNombre + "</b></div>" +
                "<div>" + destPuesto + "</div>" +
                "<div>" + destEmpresa + "</div>" +
                "<p>Estimado " + destNombre.Split(" ")[0].ToString() + ".</p>" +
                "<p><dd>Reciba un cordial saludo y a la vez me permito enviar nuestra cotización de servicio para el" +
                    "transporte de ácido fosfórico de Lázaro Cárdenas, Mich.a su planta Quimir en México.</dd></p>" +
                "<p><dd>La tarifa es considerada por viaje cargando en sencillo 37 tn y full 50 tn.</dd></p>" +
                "<table style=\"border-collapse:collapse; border:1px solid black; margin: 5% 0\">" +
                "<tr>" +
                    "<th>ORIGEN</th>" +
                    "<th>DESTINO</th>" +
                    "<th>TONS</th>" +
                    "<th>EJES</th>" +
                    "<th>MODALIDAD</th>" +
                    "<th>TARIFA X TON</th>" +
                    "<th>PRODUCTO</th>" +
                    "<th>TIPO DE EQUIPO</th>" +
                    "<th>FECHA</th>" +
                "</tr>";
            foreach (DataRow row in dtInfo.Rows) 
            {
                html +=
                    "<tr>" +
                        "<td>" + row["origen"] + "</td>" +
                        "<td>" + row["destino"] + "</td>" +
                        "<td>" + row["tons"] + "</td>" +
                        "<td>" + row["ejes"] + "</td>" +
                        "<td>" + row["modalidad"] + "</td>" +
                        "<td>" + row["tarifa"] + "</td>" +
                        "<td>" + row["producto"] + "</td>" +
                        "<td>" + row["tipoEquipo"] + "</td>" +
                        "<td>" + row["fecha"] + "</td>" +
                    "</tr>";
            }
            html += "</table>" +
                "<div>Precio vigente a Ago. del 2022</div>" +
                "<div>Estos precios son más IVA menos retención.</div>" +
                "<div>Los cambios en los precios de combustible, peajes y tipo de cambio del dólar afectan al precio cotizado.</div>" +
                "<div>La capacidad mínima para facturar, son las toneladas indicadas en la cotización.</div>" +
                "<div>El producto viaja por cuenta y riesgo del cliente.</div>" +
                "<div>Maniobras: Sin maniobra de carga o descarga</div>" +
                "<div>El viaje cuenta con 12 horas libres para cargar y 12 libres para descargar.</div>" +
                "<div>Después de este tiempo, se cobran demoras en carga o descarga a razón de $4,500.00 por cada 24 horas o fracción" +
                "excedente.</div><br><br>" +
                "<div style=\"width: 100%; text-align: center\"><b>Atentamente<b/></div>" +
                "<div style=\"width: 100%; text-align: center\"><img style=\"width: 20%\" src=\"" + pathImg + "firma.png" + "\"></div>" +
                "<div style=\"width: 100%; text-align: center\"><b>Juan Felipe Cortés Zamudio.</b></div>" +
                "<div style=\"width: 100%; text-align: center\"><b>Gerente de tráfico.</b></div>" +
                "<div style=\"width: 100%; text-align: center\"><b>Autoquimicos.</b<</div>";
            return styles + html;
        }

        private void SetFooter(PdfDocumentControl htmlToPdfDocument)
        {
            htmlToPdfDocument.Footer.Enabled = true;

            // set footer height
            htmlToPdfDocument.Footer.Height = 50;

            // set footer background color
            htmlToPdfDocument.Footer.BackgroundColor = System.Drawing.Color.White;
            float pdfPageWidth =
                    htmlToPdfDocument.PageOrientation == PdfPageOrientation.Portrait ?
                    htmlToPdfDocument.PageSize.Width : htmlToPdfDocument.PageSize.Height;

            float footerWidth = pdfPageWidth - htmlToPdfDocument.Margins.Left - htmlToPdfDocument.Margins.Right;
            float footerHeight = htmlToPdfDocument.Footer.Height;

            string workingDirectory = Environment.CurrentDirectory;
            string pathImg = workingDirectory + "\\Imagenes\\";
            string htmlFooter =
                "<div style=\"width: 100%; text-align: left\"><img style=\"width: 100%\" src=\"" + pathImg + "footer.jpg" + "\"></div>";

            // layout HTML in footer
            PdfHtml footerHtml = new PdfHtml(5, 5, htmlFooter, null);
            footerHtml.FitDestHeight = true;
            htmlToPdfDocument.Footer.Layout(footerHtml);
        }
    }
}
