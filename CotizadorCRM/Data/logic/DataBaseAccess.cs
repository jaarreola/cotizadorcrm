﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CotizadorCRM.Data.logic
{
    public class DataBaseAccess
    {
        private static logic.BasePage Base = new BasePage();
        SqlConnection sqlConection;
        static string conexionString;

        public DataBaseAccess(String strConn)
        {
            this.sqlConection = new SqlConnection(strConn);
            conexionString = strConn;
        }

        public DataSet ExecuteQuery(string p_procedure, Dictionary<string, string> p_datos)
        {
            DataSet ds = new DataSet();
            BasePage basePage = new BasePage();
            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                string sNomParametro = string.Empty;
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(p_procedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(cmd);
                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, string> entry in p_datos)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    param.Value = entry.Value;//BasePage.ReplaceSpecialCharacter(entry.Value);
                                    break;
                                }
                            }

                            //Si el parametro es de tipo numero y se recibio un '' desde codigo se envia null a la base de datos.
                            if ((BasePage.ToString(param.Value) == "") && (param.SqlDbType == SqlDbType.Int || param.SqlDbType == SqlDbType.Float || param.SqlDbType == SqlDbType.Decimal))
                            {
                                param.Value = null;
                            }
                            if (param.Value != null && param.SqlDbType.ToString().ToUpper() == "VARCHAR")
                            {
                                param.Value = param.Value.ToString();
                            }
                        }
                    }

                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);


                    return ds;
                }
            }
        }

        public DataSet ExecuteQuery(string p_procedure, Dictionary<string, object> p_datos)
        {
            DataSet ds = new DataSet();
            BasePage basePage = new BasePage();
            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                string sNomParametro = string.Empty;
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(p_procedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(cmd);
                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, object> entry in p_datos)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    param.Value = entry.Value;//BasePage.ReplaceSpecialCharacter(entry.Value);
                                    break;
                                }
                            }

                            //Si el parametro es de tipo numero y se recibio un '' desde codigo se envia null a la base de datos.
                            if ((BasePage.ToString(param.Value) == "") && (param.SqlDbType == SqlDbType.Int || param.SqlDbType == SqlDbType.Float || param.SqlDbType == SqlDbType.Decimal))
                            {
                                param.Value = null;
                            }
                            if (param.Value != null && param.SqlDbType.ToString().ToUpper() == "VARCHAR")
                            {
                                param.Value = param.Value.ToString();
                            }
                        }
                    }

                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);


                    return ds;
                }
            }
        }

        public void ExecuteNonQuery(string nombreSP, Dictionary<string, object> parameters)
        {
            BasePage basePage = new BasePage();

            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                string sNomParametro = string.Empty;
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(nombreSP, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(cmd);
                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, object> entry in parameters)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    param.Value = entry.Value;
                                    break;
                                }
                            }

                            //Si el parametro es de tipo numero y se recibio un '' desde codigo se envia null a la base de datos.
                            if ((BasePage.ToString(param.Value) == "") && (param.SqlDbType == SqlDbType.Int || param.SqlDbType == SqlDbType.Float || param.SqlDbType == SqlDbType.Decimal))
                            {
                                param.Value = null;
                            }
                            if (param.Value != null && param.SqlDbType.ToString().ToUpper() == "VARCHAR")
                            {
                                param.Value = param.Value.ToString();
                            }
                        }
                    }

                    cmd.ExecuteNonQuery();

                    var key = string.Empty;

                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Output || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, object> entry in parameters)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    key = entry.Key.ToString();
                                    break;
                                }
                            }

                            if (param.Value != null && (param.SqlDbType.ToString().ToUpper() == "VARCHAR" || param.SqlDbType.ToString().ToUpper() == "CHAR"))
                            {
                                parameters[key] = param.Value.ToString();
                            }
                            else
                            {
                                parameters[key] = param.Value;
                            }
                            
                        }
                    }
                }
            }
        }

        public void ExecuteNonQuery(string nombreSP, Dictionary<string, string> parameters)
        {
            BasePage basePage = new BasePage();
            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                string sNomParametro = string.Empty;
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(nombreSP, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(cmd);
                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, string> entry in parameters)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    param.Value = entry.Value; // BasePage.ReplaceSpecialCharacter(entry.Value);
                                    break;
                                }
                            }

                            //Si el parametro es de tipo numero y se recibio un '' desde codigo se envia null a la base de datos.
                            if ((BasePage.ToString(param.Value) == "") && (param.SqlDbType == SqlDbType.Int || param.SqlDbType == SqlDbType.Float || param.SqlDbType == SqlDbType.Decimal))
                            {
                                param.Value = null;
                            }
                            if (param.Value != null && param.SqlDbType.ToString().ToUpper() == "VARCHAR")
                            {
                                if (sNomParametro.Contains("Contrasenia"))
                                {
                                    param.Value = param.Value;
                                }
                                else
                                {
                                    param.Value = param.Value.ToString();
                                }
                            }
                        }
                    }

                    cmd.ExecuteNonQuery();

                    var key = string.Empty;

                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Output || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, string> entry in parameters)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    key = entry.Key.ToString();
                                    break;
                                }
                            }

                            parameters[key] = param.Value.ToString();
                        }
                    }
                }
            }
        }

        public void ExecuteNonQuerySimple(string nombreSP, Dictionary<string, string> parameters)
        {
            BasePage basePage = new BasePage();

            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                string sNomParametro = string.Empty;
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(nombreSP, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters(cmd);
                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Input || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, string> entry in parameters)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    param.Value = entry.Value; // BasePage.ReplaceSpecialCharacter(entry.Value);
                                    break;
                                }
                            }

                            //Si el parametro es de tipo numero y se recibio un '' desde codigo se envia null a la base de datos.
                            if ((BasePage.ToString(param.Value) == "") && (param.SqlDbType == SqlDbType.Int || param.SqlDbType == SqlDbType.Float || param.SqlDbType == SqlDbType.Decimal))
                            {
                                param.Value = null;
                            }
                        }
                    }

                    cmd.ExecuteNonQuery();

                    var key = string.Empty;

                    foreach (SqlParameter param in cmd.Parameters)
                    {
                        if (param.Direction == ParameterDirection.Output || param.Direction == ParameterDirection.InputOutput)
                        {
                            sNomParametro = param.ParameterName.Substring(1, param.ParameterName.Length - 1);

                            foreach (KeyValuePair<string, string> entry in parameters)
                            {
                                if (entry.Key.ToString().ToLower() == sNomParametro.ToLower())
                                {
                                    key = entry.Key.ToString();
                                    break;
                                }
                            }

                            parameters[key] = param.Value.ToString();
                        }
                    }
                }
            }
        }

        public void ExecuteComandTextQuery(string comando)
        {
            BasePage basePage = new BasePage();

            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DataSet ExecuteComandTextNonQuery(string comando)
        {
            DataSet ds = new DataSet();
            BasePage basePage = new BasePage();
            using (SqlConnection conn = new SqlConnection(conexionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = comando;
                    cmd.Connection = conn;

                    SqlDataAdapter adp = new SqlDataAdapter(cmd);
                    adp.Fill(ds);


                    return ds;
                }
            }
        }

        public void getErrorMessage(string errorCode, object[] errorParams)
        {
            //Obtiene el mensaje de error de DB en base a código
            Dictionary<string, object> dicParams = new Dictionary<string, object>();
            dicParams.Add("code", errorCode);
            DataTable dtErrorDetail = ExecuteQuery("sp_get_errorCode", dicParams).Tables[0];
            //Valida si existe el mensaje de error
            if (dtErrorDetail.Rows.Count > 0)
            {
                //Valida que la cantidad de parametros esperados en el mensaje de error coincida con los enviados
                if(int.Parse(dtErrorDetail.Rows[0]["params"].ToString()) == errorParams.Length)
                {
                    throw new Exception(string.Format(dtErrorDetail.Rows[0]["message"].ToString(), errorParams));
                }
                else
                {
                    throw new Exception("getErrorMessage(): La cantidad de parámetros recibidos no coinciden con los esperados por el mensaje de error. Parámetros esperados: "+ dtErrorDetail.Rows[0]["params"].ToString() +", parámetros recibidos: " + errorParams.Length.ToString() + ".");
                }
            }
            else
            {
                throw new Exception("getErrorMessage(): No existe un mensaje de error con el código "+ errorCode + ". Favor de validar el código de error en DB.");
            }
        }
    }
}
