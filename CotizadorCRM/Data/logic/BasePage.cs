﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text.Json;

namespace CotizadorCRM.Data.logic
{
    public class BasePage
    {
        public static string ToString(object value)
        {
            if (value is DBNull)
                return string.Empty;
            return Convert.ToString(value);
        }

        public List<Dictionary<string, object>> DataTableToMap(DataTable p_dt)
        {
            List<Dictionary<string, object>> maps = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            if (p_dt != null)
            {
                foreach (DataRow dr in p_dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in p_dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    maps.Add(row);
                }
            }
            return maps;
        }

        public Dictionary<string, object> DataTableToMapRow(DataTable p_dt)
        {
            List<Dictionary<string, object>> maps = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            if (p_dt != null)
            {
                foreach (DataRow dr in p_dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in p_dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    maps.Add(row);
                }
            }
            return maps[0];
        }

        public string DataTableToJSON(DataTable p_dt)
        {
            List<Dictionary<string, object>> maps = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            if (p_dt != null)
            {
                foreach (DataRow dr in p_dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in p_dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    maps.Add(row);
                }
            }
            return System.Text.Json.JsonSerializer.Serialize(maps);
        }

        public void DataTableDetailListToJSONRowDictionary(DataTable p_dt, Dictionary<string, object> dic, string columnName)
        {
            List<Dictionary<string, object>> maps = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            if (p_dt != null)
            {
                foreach (DataRow dr in p_dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in p_dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    maps.Add(row);
                }
            }
            //Agrega el dictionary detalle al dictionary principal
            dic.Add(columnName, maps);
            //return System.Text.Json.JsonSerializer.Serialize(maps);
        }

        public string DataTableRowToJSON(DataTable p_dt)
        {
            List<Dictionary<string, object>> maps = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            if (p_dt != null)
            {
                foreach (DataRow dr in p_dt.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in p_dt.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    maps.Add(row);
                }
            }
            if(maps.Count == 0)
            {
                return System.Text.Json.JsonSerializer.Serialize(maps);
            }else
            {
                return System.Text.Json.JsonSerializer.Serialize(maps[0]);
            }
            
        }

        public Dictionary<string, object> paramsDataClassToDic(Object paramsClass)
        {
            PropertyInfo[] infos = paramsClass.GetType().GetProperties();
            Dictionary<string, object> dix = new Dictionary<string, object>();
            foreach (PropertyInfo info in infos)
            {
                dix.Add(info.Name, info.GetValue(paramsClass, null));
            }
            return dix;
        }


        public void InsertDetail(DataBaseAccess dbAccess, string pkName, List<Dictionary<string, object>> lista, string spInsertName, string spDeleteName, string pkKeyValue, Boolean bolLlave = true)
        {
            var detailSet = new Dictionary<string, object>();
            //Inserta el detalle; una transacción por cada registro de la lista.
            if(lista.Count == 0)
            {
                dbAccess.ExecuteNonQuery(spDeleteName, detailSet);
            }
            else
            {
                for (var index = 0; index < lista.Count; index++)
                {
                    detailSet = lista[index];
                    if (bolLlave)
                    {
                        detailSet[pkName] = pkKeyValue;
                    }
                    //Si existem algun registro, se elimina el detalle actual 
                    if (index == 0)
                    {
                        dbAccess.ExecuteNonQuery(spDeleteName, detailSet);
                    }
                    dbAccess.ExecuteNonQuery(spInsertName, detailSet);
                }
            }
            
        }

        public List<Dictionary<string, object>> Deserialize(string json)
        {
            //System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //serializer.MaxJsonLength = Int32.MaxValue;
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
            //JsonConvert.DeserializeObject(json);
            items = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);
            return items;
        }

        public List<Dictionary<string, object>> ListModelToDictionary<T>(List<T> modelList, PropertyInfo[] propertiesModel)
        {
            //var properties = typeof(Person).GetProperties();
            List<Dictionary<string, object>> listOfDictionary = new List<Dictionary<string, object>>();
            foreach (var row in modelList)
            {
                var dict = new Dictionary<string, object>();
                foreach (var propierie in propertiesModel)
                {
                    dict.Add(propierie.Name, propierie.GetValue(row));
                }
                listOfDictionary.Add(dict);
            }
            return listOfDictionary;
        }
    }
}
