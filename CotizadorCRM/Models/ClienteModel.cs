﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{

    //*********Definición de tabla Cabecera***********
    public class setClienteModel
    {
        public int idEmpresa { get; set; }
        public int idCliente { get; set; }
        public int idPlaza { get; set; }
        public int idClienteTipo { get; set; }
        public string nombre { get; set; }
        public string aliasCliente { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string movil { get; set; }
        public string contacto { get; set; }
        public string correo { get; set; }
        public string rfc { get; set; }
        public string observaciones { get; set; }
        public decimal posLat { get; set; }
        public decimal posLon { get; set; }
        public string estatus { get; set; }
        public List<setClienteContactoModel> contactos { get; set; }
    }

    //*********Definición de tabla detalle***********
    public class setClienteContactoModel
    {
        public int idEmpresa { get; set; }
        public int idCliente { get; set; }
        public string departamento { get; set; }
        public string puesto { get; set; }
        public string nombre { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string movil { get; set; }
        public string nota { get; set; }
        public string estatus { get; set; }
    }

    public class getClientesListaModel
    {
        public int idEmpresa { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getClienteByIdModel

    {
        public int idEmpresa { get; set; }
        public int idCliente { get; set; }

    }

}
