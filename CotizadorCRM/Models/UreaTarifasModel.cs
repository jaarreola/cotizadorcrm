﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    public class getUreaTarifaListaModel
    {
        public int idEmpresa { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getUreaTarifaByIdModel
    {
        public int idEmpresa { get; set; }
        public int idUreaTarifa { get; set; }
    }

    public class setUreaTarifaModel
    {
        public int idEmpresa { get; set; }
        public int idUreaTarifa { get; set; }
        public string nombre { get; set; }
        public double costo { get; set; }
        public string estatus { get; set; }
    }

    public class getUreaTarifasAutocomplete
    {
        public int idEmpresa { get; set; }
    }
}
