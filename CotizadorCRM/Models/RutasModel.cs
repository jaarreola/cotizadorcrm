﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla Cabecera***********
    public class setRutaModel
    {
        public int idEmpresa { get; set; }
        public int idRuta { get; set; }
        public int idPlazaOrigen { get; set; }
        public int idPlazaDestino { get; set; }
        public int idRutaTipo { get; set; }
        public int idRutaTipoVehiculo { get; set; }
        public int idVehiculoEje { get; set; }
        public int idRutaCondicionOrigen { get; set; }
        public string nombre { get; set; }
        public int kms { get; set; }
        public string estatus { get; set; }
        public List<setRutaSegmentosModel> segmentos { get; set; }
    }

    //*********Definición de tabla detalle***********
    public class setRutaSegmentosModel
    {
        public int idEmpresa { get; set; }
        public int idRuta { get; set; }
        public int idRutaTipoParada { get; set; }
        public int idRutaMovimiento { get; set; }
        public string direccionSegmento { get; set; }
        public decimal posLat { get; set; }
        public decimal posLon { get; set; }
    }

    public class getRutaByIdModel
    {
        public int idEmpresa { get; set; }
        public int idRuta { get; set; }
    }


    public class getRutasListaModel
    {
        public int idEmpresa { get; set; }
        public int idPlazaOrigen { get; set; }
        public int idPlazaDestino { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }
}
