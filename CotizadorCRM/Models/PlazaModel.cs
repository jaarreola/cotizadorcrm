﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setPlazaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idPlaza { get; set; }
        public int idPais { get; set; }
        public int idEstado { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public decimal posLat { get; set; }
        public decimal posLon { get; set; }
        public string estatus { get; set; }
        public int idCombustibleTarifa { get; set; }
    }


    public class getPlazaIdModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idPlaza { get; set; }
    }

    public class getPlazalListaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idEstado { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getPlazaListaByEmpresaAreaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
    }
}
