﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    public class getEmpresaConfiguracionModel
    {
        public int idEmpresa { get; set; }
        public int filtro { get; set; }
    }

    public class setEmpresaConfiguracionVariablesModel
    {
        public int idEmpresa { get; set; }
        public int combustible_chk_automatico { get; set; }
        public int combustible_chk_historico { get; set; }
        public int combustible_chk_manual { get; set; }
        public int combustible_chk_tarifa { get; set; }
        public int combustible_IdCostoTipo { get; set; }
        public decimal combustible_factor { get; set; }
        public int caseta_chk_automatico { get; set; }
        public int caseta_chk_historico { get; set; }
        public int caseta_chk_manual { get; set; }
        public int caseta_chk_archivo { get; set; }
        public int caseta_IdCostoTipo { get; set; }
        public decimal caseta_factor { get; set; }
        public int sueldo_chk_historico { get; set; }
        public int sueldo_chk_manual { get; set; }
        public int sueldo_IdCostoTipo { get; set; }
        public decimal sueldo_factor { get; set; }
        public List<setEmpresaConfiguracioVariablesSueldosModel> sueldos { get; set; }
    }
    public class setEmpresaConfiguracioVariablesSueldosModel
    {
        public int idEmpresa { get; set; }
        public string tipoUnidad { get; set; }
        public string rangoInicio { get; set; }
        public string rangoFin { get; set; }
        public string factor { get; set; }
    }

    public class setEmpresaConfiguracioFijosModel
    {
        public int idEmpresa { get; set; }
        public int operativo_mtto_chk_analisis { get; set; }
        public decimal operativo_mtto_monto { get; set; }
        public int operativo_mtto_IdCostoTipo { get; set; }
        public int operativo_llantas_chk_analisis { get; set; }
        public decimal operativo_llantas_monto { get; set; }
        public int operativo_llantas_IdCostoTipo { get; set; }
        public decimal operativo_admin_monto { get; set; }
        public int operativo_admin_IdCostoTipo { get; set; }

    }

    public class getCostosVariablesEmpresaModel
    {
        public int idEmpresa { get; set; }
    }
}
