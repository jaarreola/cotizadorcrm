﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    public class getTarifaListaModel
    {
        public int idEmpresa { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getTarifaByIdModel
    {
        public int idEmpresa { get; set; }
        public int idCombustibleTarifa { get; set; }
    }

    public class setTarifaModel
    {
        public int idEmpresa { get; set; }
        public int idCombustibleTarifa { get; set; }
        public string nombre { get; set; }
        public double costoRegular { get; set; }
        public double costoPremium { get; set; }
        public double costoDiesel { get; set; }
        public string estatus { get; set; }
    }

    public class getCombustibleTarifasAutocomplete
    {
        public int idEmpresa { get; set; }
        public int idCombustibleTarifaOrigen { get; set; }
        public int idCombustibleTarifaDestino { get; set; }
    }
}
