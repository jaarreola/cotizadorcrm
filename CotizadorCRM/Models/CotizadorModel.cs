﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    public class CotizadorModel
    {
        public int idEmpresa { get; set; }

    }

    public class getCostosPorKMSModel
    {
        public int idEmpresa { get; set; }
        public int kms { get; set; }
    }

    public class INEGIApiBuscaDestioRequest
    {
        public string lugarBusqueda { get; set; }
        public int idPlaza { get; set; }
    }

    public class INEGIApiBuscaDestioResponse
    {
        public string direccion { get; set; }
        public int id { get; set; }
        public decimal lat { get; set; }
        public decimal lon { get; set; }
    }


    public class INEGIApiBuscaLineaRequest
    {
        public string posX { get; set; }
        public string posY { get; set; }
        public string escala { get; set; }
    }

    public class getRutaRequest
    {
        public int idEmpresa { get; set; }
        public int idPlazaOrigen { get; set; }
        public int idSegmentoOrigen { get; set; }
        public double idSegmentoOrigenLat { get; set; }
        public double idSegmentoOrigenLon { get; set; }
        public int idPlazaDestino { get; set; }
        public int idSegmentoDestino { get; set; }
        public double idSegmentoDestinoLat { get; set; }
        public double idSegmentoDestinoLon { get; set; }
        public int idVehiculoArmado { get; set; }
        public int idTipoCarretera { get; set; }
        public double rendimiento { get; set; }
        public int idCombustibleTarifa { get; set; }
        //Variables para recalcular tarifas
        public int tipoCalculoCombustible { get; set; }
        public double tipoCalculoCombustibleTarifa { get; set; }
        public int tipoCalculoCombustibleIdCostoTipo { get; set; }
        public int tipoCalculoCasetas { get; set; }
        public double tipoCalculoCasetasTarifa { get; set; }
        public int tipoCalculoCasetasIdCostoTipo { get; set; }
        public int tipoCalculoSueldo { get; set; }
        public double tipoCalculoSueldoTarifa { get; set; }
        public int tipoCalculoSueldoIdCostoTipo { get; set; }
        //Variables con múltiples tarifas
        public int kms { get; set; }
        public int idRuta { get; set; }
        //Ejes ecedentes
        public int ejesExcedentes { get; set; }
        public double rendimientoUrea { get; set; }
        public int idUreaTarifa { get; set; }
        public List<getRutaSegmentosRequest> segmentos { get; set; }
    }

    public class getRutaSegmentosRequest
    {
        public int idSegmentoINEGI { get; set; }
        public decimal posLat { get; set; }
        public decimal posLon { get; set; }
    }

    public class getRutaResponse
    {
        public int kms { get; set; }
        public string rendimiento { get; set; }
        public string costoCombustibleInfo { get; set; }
        public double costoCombustible { get; set; }
        public string tiempoEstimado { get; set; }
        public string advertencias { get; set; }
        //Costos por Configuracion
        public double costoCombustibleAutomatico { get; set; }
        public string tooltipCostoCombustibleAutomatico { get; set; }
        public double costoCombustibleTarifa { get; set; }
        public string tooltipCostoCombustibleTarifa { get; set; }
        public double costoCombustibleHistorico { get; set; }
        public string tooltipCostoCombustibleHistorico { get; set; }
        public double costoCombustibleManual { get; set; }
        public string tooltipCostoCombustibleManual { get; set; }
        public double costoCasetasAutomatico { get; set; }
        public string tooltipCostoCasetasAutomatico { get; set; }
        public double costoCasetasHistorico { get; set; }
        public string tooltipCostoCasetasHistorico { get; set; }
        public double costoCasetasArchivo { get; set; }
        public string tooltipCostoCasetasArchivo { get; set; }
        public double costoCasetasManual { get; set; }
        public string tooltipCostoCasetasManual { get; set; }
        public double costoSueldoHistorico { get; set; }
        public string tooltipCostoSueldoHistorico { get; set; }
        public double costoSueldoManual { get; set; }
        public string tooltipCostoSueldoManual { get; set; }
        public string casetasRuta { get; set; }
        public List<getRutaCostosResponse> costos { get; set; }
        public List<getRutaCoordenadasResponse> coordenadas { get; set; }
    }

    public class getRutaCostosResponse
    {
        public string id { get; set; }
        public string monto { get; set; }
        public string tipo { get; set; }
    }

    public class getRutaCoordenadasResponse
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }

    public class setCotizacionModel
    {
        public int idCotizacion { get; set; }
        public string folio { get; set; }
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idCliente { get; set; }
        public string nombreCliente { get; set; }
        public int idTipoOperacion { get; set; }
        public int idTipoServicio { get; set; }
        public int idVehiculoArmado { get; set; }
        public int idMoneda { get; set; }
        public decimal tipoCambio { get; set; }
        public int idPlazaOrigen { get; set; }
        public string origenDireccionSegmento { get; set; }
        public double origenPosLat { get; set; }
        public double origenPosLon { get; set; }
        public int origenIdSegmentoINEGI { get; set; }
        public int idPlazaDestino { get; set; }
        public string destinoDireccionSegmento { get; set; }
        public double destinoPosLat { get; set; }
        public double destinoPosLon { get; set; }
        public int destinoIdSegmentoINEGI { get; set; }
        public int idRuta { get; set; }
        public int idRutaTipo { get; set; }
        public int idRutaCondicionOrigen { get; set; }
        public int idTipoCarretera { get; set; }
        public int idFrecuenciasServicio { get; set; }
        public int rutaKms { get; set; }
        public decimal rendimiento { get; set; }
        public int idCombustibleTarifa { get; set; }
        public string rutaRendimientoEstimado { get; set; }
        public string rutaTiempoEsperado { get; set; }
        public string rutaCostoCombustible { get; set; }
        public string rutaAdvertencias { get; set; }
        public int idTipoCobro { get; set; }
        public int idUnidadMedida { get; set; }
        public decimal subtotal { get; set; }
        public decimal descuento { get; set; }
        public decimal ajuste { get; set; }
        public decimal utilidad { get; set; }
        public decimal totalFijos { get; set; }
        public decimal totalVariables { get; set; }
        public decimal totalOtros { get; set; }
        public int cantidadFrecuencia { get; set; }
        public int idCotizacionEstatus { get; set; }
        public DateTime fecha { get; set; }
        public int tipoCalculoCombustible { get; set; }
        public decimal tipoCalculoCombustibleTarifa { get; set; }
        public int tipoCalculoCombustibleIdCostoTipo { get; set; }
        public int tipoCalculoCasetas { get; set; }
        public decimal tipoCalculoCasetasTarifa { get; set; }
        public int tipoCalculoCasetasIdCostoTipo { get; set; }
        public int tipoCalculoSueldo { get; set; }
        public decimal tipoCalculoSueldoTarifa { get; set; }
        public int tipoCalculoSueldoIdCostoTipo { get; set; }
        public int unidadMedidaCarga { get; set; }
        public decimal cantidadCarga { get; set; }
        public int idProducto { get; set; }
        public string nombreProducto { get; set; }
        public decimal rendimientoUrea { get; set; }
        public int idUreaTarifa { get; set; }
        public string observaciones { get; set; }
        public List<setCotizacionSegmentosModel> segmentos { get; set; }
        public List<setCotizacionCostosModel> costos { get; set; }
    }

    public class setCotizacionSegmentosModel
    {
        public int idCotizacion { get; set; }
        public int idEmpresa { get; set; }
        public int idRutaTipoParada { get; set; }
        public int idRutaMovimiento { get; set; }
        public string direccionSegmento { get; set; }
        public decimal posLat { get; set; }
        public decimal posLon { get; set; }
        public int idSegmentoINEGI { get; set; }
    }

    public class setCotizacionCostosModel
    {
        public int idCotizacion { get; set; }
        public int idEmpresa { get; set; }
        public string id { get; set; }
        public decimal monto { get; set; }
        public string tipo { get; set; }
    }

    public class getCotizacionesListaModel
    {
        public int idEmpresa { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public int idCliente { get; set; }
        public int folio { get; set; }
        public int idCotizacionEstatus { get; set; }
        public int idPersonal { get; set; }
    }

    public class getCotizacionByIdModel
    {
        public int idEmpresa { get; set; }
        public int idCotizacion { get; set; }
    }

    public class setCotizacionEstatusModel
    {
        public int idCotizacion { get; set; }
        public int idEmpresa { get; set; }
        public string nombre { get; set; }
        public int idClienteOrigen { get; set; }
        public int idClienteDestino { get; set; }
        public DateTime fecha { get; set; }
        public DateTime fechaFin { get; set; }
        public int idCotizacionEstatus { get; set; }
    }

    public class setCotizacionEstatusRechazadoModel
    {
        public int idCotizacion { get; set; }
        public int idEmpresa { get; set; }
        public int idRechazo { get; set; }
        public string rechazoOtro { get; set; }
        public int idCotizacionEstatus { get; set; }
    }

    public class updateRefPlazaModel
    {
        public int idPlaza { get; set; }
        public int IdINEGI { get; set; }
        public string DireccionINEGI { get; set; }
        public double LatINEGI { get; set; }
        public double LonINEGI { get; set; }
    }

    public class cotizadorToPDFModel 
    { 
        public string cotizacion { get; set; }
        public string cliente { get; set; }
        public string fecha { get; set; }
        public string estatus { get; set; }
        public double totalVariables { get; set; }
        public double totalFijos { get; set; }
        public double totalOtros { get; set; }
        public double tarifa { get; set; }
    }

    public class cotizadorTableToPDFModel
    {
        public string origen { get; set; }
        public string destino { get; set; }
        public int tons { get; set; }
        public int ejes { get; set; }
        public string modalidad { get; set; }
        public double tarifa { get; set; }
        public string producto { get; set; }
        public string tipoEquipo { get; set; }
        public string fecha { get; set; }
    }
}
