﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setProductosModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idProducto { get; set; }
        public int idEmbalaje { get; set; }
        public string nombre { get; set; }
        public string clave { get; set; }
        public string estatus { get; set; }
    }

    public class getProductosListaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idEmbalaje { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getProductosByIdModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idProducto { get; set; }
    }

    public class getProductosAutocompleteModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
    }
}
