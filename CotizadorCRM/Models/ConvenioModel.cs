﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    public class ConvenioModel
    {
    }

    public class setConvenioModel
    {
        public int idConvenio { get; set; }
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idCotizacion { get; set; }
        public int idCliente { get; set; }
        public string nombre { get; set; }
        public int idClienteRemitente { get; set; }
        public int idClienteDestinatario { get; set; }
        public int idRuta { get; set; }
        public int rutaKms { get; set; }
        public int folio { get; set; }
        public int idVehiculoArmado { get; set; }
        public int idMoneda { get; set; }
        public decimal tipoCambio { get; set; }
        public int idTipoOperacion { get; set; }
        public int idPlazaOrigen { get; set; }
        public int idPlazaDestino { get; set; }
        public int idTipoServicio { get; set; }
        public decimal valorDeclarado { get; set; }
        public int idTipoCobro { get; set; }
        public int idTipoPago { get; set; }
        public string recogerEn { get; set; }
        public string entregarEn { get; set; }
        public int controlarPago { get; set; }
        public decimal valorOperador { get; set; }
        public decimal valorPermisionario { get; set; }
        public decimal flete { get; set; }
        public decimal seguro { get; set; }
        public decimal maniobras { get; set; }
        public decimal autopistas { get; set; }
        public decimal otros { get; set; }
        public decimal subtotal { get; set; }
        public decimal total { get; set; }
        public int idImpuestoIva { get; set; }
        public int idImpuestoRetencion { get; set; }
        public int idConvenioEstatus { get; set; }
        public List<setConvenioSegmentosModel> segmentos { get; set; }
        public List<setConvenioProductosModel> productos { get; set; }
    }

    public class setConvenioSegmentosModel
    {
        public int idConvenio { get; set; }
        public int idEmpresa { get; set; }
        public int idRutaTipoParada { get; set; }
        public int idRutaMovimiento { get; set; }
        public int idPlaza { get; set; }
    }

    public class setConvenioProductosModel
    {
        public int idConvenio { get; set; }
        public int idEmpresa { get; set; }
        public int idProducto { get; set; }
        public decimal cantidad { get; set; }
        public decimal peso { get; set; }
        public decimal volumen { get; set; }
        public decimal pesoEstimado { get; set; }
        public decimal importe { get; set; }

    }

    public class getConvenioByIdModel
    {
        public int idEmpresa { get; set; }
        public int idConvenio { get; set; }
    }

    public class getConveniosListaModel
    {
        public int idEmpresa { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
        public int idCliente { get; set; }
        public int idRuta { get; set; }
        public int idConvenioEstatus { get; set; }
        public string nombre { get; set; }
    }

    public class setContratoToConvenioModel
    {
        public int idConvenio { get; set; }
        public int idCotizacion { get; set; }
        
    }

    public class updateFechaVigenciaContratoModel
    {
        public int idConvenio { get; set; }
        public int idCotizacion { get; set; }
        public DateTime fechaVigencia { get; set; }
    }
}
