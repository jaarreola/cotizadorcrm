﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setTipoCambioModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idTipoCambio { get; set; }
        public int idMoneda { get; set; }
        public int idTipoFuente { get; set; }
        public decimal tipoCambio { get; set; }
        public DateTime fecha { get; set; }
    }


    public class getTipoCambioIdModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idTipoCambio { get; set; }
    }

    public class getTipoCambioListaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idMoneda { get; set; }
        public DateTime fechaInicio { get; set; }
        public DateTime fechaFin { get; set; }
    }
}
