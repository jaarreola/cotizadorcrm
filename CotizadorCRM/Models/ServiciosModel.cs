﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setServicioModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idServicio { get; set; }
        public int idTipoServicio { get; set; }
        public int idTipoOperacion { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getServicioIdModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idServicio { get; set; }
    }

    public class getServiciosListaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idTipoServicio { get; set; }
        public int idTipoOperacion { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }
}
