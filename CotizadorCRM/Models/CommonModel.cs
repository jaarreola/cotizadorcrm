﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    public class getEstadosListaListaModel
    {
        public int idPais { get; set; }
    }

    public class getInfoByEmpresaModel
    {
        public int idEmpresa { get; set; }
    }
    public class getInfoByEmpresaAndAreaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
    }
}
