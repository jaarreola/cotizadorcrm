﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setPersonalModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idPersonal { get; set; }
        public int idPersonalTipo { get; set; }
        public string nombre { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public DateTime fechaIngreso { get; set; }
        public string estatus { get; set; }
        public string usuario { get; set; }
        public string password { get; set; }

    }

    public class getPersonalIdModel
    {
        public int idEmpresa { get; set; }
        public int idPersonal { get; set; }
    }

    public class getPersonalListaModel
    {
        public int idEmpresa { get; set; }
        public int idPersonalTipo { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }
}
