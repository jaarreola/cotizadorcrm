﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setVehiculoModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idVehiculoArmado { get; set; }
        public int idVehiculoClasificacion { get; set; }
        public int idCombustibleTipo { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
        public decimal rendimiento { get; set; }
        public int unidadMedida { get; set; }
        public decimal cargaMin { get; set; }
        public decimal cargaMax { get; set; }
        public decimal rendimientoUrea { get; set; }
        public List<setVehiculoProductosModel> productos { get; set; }
    }

    public class setVehiculoProductosModel
    {
        public int idVehiculoArmado { get; set; }
        public int idProducto { get; set; }
    }

    public class getVehiculoIdModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idVehiculoArmado { get; set; }
    }

    public class getVehiculosListaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idVehiculoClasificacion { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getVehiculoListaByEmpresaAreaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int unidadMedida { get; set; }
        public decimal carga { get; set; }
        public int idProducto { get; set; }
    }
}
