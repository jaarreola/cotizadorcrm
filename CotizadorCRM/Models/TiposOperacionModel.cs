﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CotizadorCRM.Models
{
    //*********Definición de tabla***********
    public class setTipoOperacionModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idTipoOperacion { get; set; }
        public int idClasificacionTipoOperacion { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }


    public class getTipoOperacionIdModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idTipoOperacion { get; set; }
    }

    public class getTiposOperacionListaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
        public int idClasificacionTipoOperacion { get; set; }
        public string nombre { get; set; }
        public string estatus { get; set; }
    }

    public class getTiposOperacionByEmpresaAreaModel
    {
        public int idEmpresa { get; set; }
        public int idArea { get; set; }
    }
}
