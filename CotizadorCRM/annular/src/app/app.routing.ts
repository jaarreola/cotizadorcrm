import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { PagesComponent } from './pages/pages.component';
import { NotFoundComponent } from './pages/errors/not-found/not-found.component';
import { ErrorComponent } from './pages/errors/error/error.component';

export const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { 
        path: 'pages', 
        component: PagesComponent, children: [
            { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardModule', data: { breadcrumb: 'Dashboard' } },
            { path: 'maps', loadChildren: './pages/maps/maps.module#MapsModule', data: { breadcrumb: 'Maps' } },
            //PANTALLAS DE PROCESOS
            { path: 'seguimiento-clientes', loadChildren: './pages/procesos/seguimiento-clientes/seguimiento-clientes.module#SeguimientoClientesModule', data: { breadcrumb: 'Seguimiento a clientes' } },
            { path: 'cotizador', loadChildren: './pages/procesos/cotizador/cotizador.module#CotizadorModule', data: { breadcrumb: 'Cotizador' } },
            { path: 'contratos', loadChildren: './pages/procesos/contratos/contratos.module#ContratosModule', data: { breadcrumb: 'Contratos' } },
            { path: 'convenios', loadChildren: './pages/procesos/convenios/convenios.module#ConveniosModule', data: { breadcrumb: 'Convenios' } },
            //PANTALLAS DE CATALOGOS
            { path: 'clientes', loadChildren: './pages/catalogos/clientes/clientes.module#ClientesModule', data: { breadcrumb: 'Clientes' } },
            { path: 'plazas', loadChildren: './pages/catalogos/plazas/plazas.module#PlazasModule', data: { breadcrumb: 'Plazas' } },
            { path: 'rutas', loadChildren: './pages/catalogos/rutas/rutas.module#RutasModule', data: { breadcrumb: 'Rutas' } },
            { path: 'servicios', loadChildren: './pages/catalogos/servicios/servicios.module#ServiciosModule', data: { breadcrumb: 'Servicios' } },
            { path: 'productos', loadChildren: './pages/catalogos/productos/productos.module#ProductosModule', data: { breadcrumb: 'Productos' } },
            { path: 'tipo-operaciones', loadChildren: './pages/catalogos/tipo-operaciones/tipo-operaciones.module#TipoOperacionesModule', data: { breadcrumb: 'Tipo Operaciones' } },
            { path: 'vehiculos', loadChildren: './pages/catalogos/vehiculos/vehiculos.module#VehiculosModule', data: { breadcrumb: 'Vehiculos' } },
            { path: 'tipos-cambio', loadChildren: './pages/catalogos/tipo-cambio/tipo-cambio.module#TipoCambioModule', data: { breadcrumb: 'Tipo de Cambio' } },
            { path: 'personal-agentes', loadChildren: './pages/catalogos/personal-agentes/personal-agentes.module#PersonalAgentesModule', data: { breadcrumb: 'Personal/Agentes' } },
            { path: 'configuracion-costos-variables', loadChildren: './pages/catalogos/configuracion-costos-variables/configuracion-costos-variables.module#ConfiguracionCostosVariablesModule', data: { breadcrumb: 'Configuración Costos Variables' } },
            { path: 'configuracion-costos-fijos', loadChildren: './pages/catalogos/configuracion-costos-fijos/configuracion-costos-fijos.module#ConfiguracionCostosFijosModule', data: { breadcrumb: 'Configuración Costos Fijos' } },
            { path: 'tarifas-combustible', loadChildren: './pages/catalogos/tarifas-combustible/tarifas-combustible.module#TarifasCombustibleModule', data: { breadcrumb: 'Tarifas Combustible' } },
            { path: 'tarifas-urea', loadChildren: './pages/catalogos/tarifas-urea/tarifas-urea.module#TarifasUreaModule', data: { breadcrumb: 'Tarifas UREA' } },
        ]
    },
    { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
    { path: 'error', component: ErrorComponent, data: { breadcrumb: 'Error' } },
    { path: '**', component: NotFoundComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
   preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
   // useHash: true
});