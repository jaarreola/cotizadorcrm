﻿import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBarConfig, MatSnackBar, MatCheckboxChange } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { fcGeneral } from '../../../services/general.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-confirmar-configuracion',
    templateUrl: './confirmar-configuracion.component.html',
    styleUrls: ['./confirmar-configuracion.component.scss']
})

export class ConfirmarConfiguracionComponent implements OnInit{
    private urlApiCliente = 'api/Cliente';
    private urlApiCotizador = 'api/Cotizador';
    //Vars
    clientes: any[] = [];
    config = new MatSnackBarConfig();
    empresaConfiguracion: any = {};
    rutaCalculada: any = {};
    //Combustible
    boolShowFormCombustible: boolean = false;
    boolShowChkCombAutomatico: boolean = false;
    strLblChkCombAutomatico: string = "Cálculo Automático";
    boolShowChkCombTarifa: boolean = false;
    strLblChkCombTarifa: string = "Cálculo por Tarifa";
    boolShowChkCombHistorico: boolean = false;
    strLblChkCombHistorico: string = "Análisis Histórico ZAM";
    //Casetas
    boolShowFormCasetas: boolean = false;
    boolShowChkCasAutomatico: boolean = false;
    strLblChkCasAutomatico: string = "Cálculo Automático";
    boolShowChkCasHistorico: boolean = false;
    strLblChkCasHistorico: string = "Análisis Histórico ZAM";
    boolShowChkCasArchivo: boolean = false;
    strLblChkCasArchivo: string = "Costo Rutas ZAM";
    //Forms
    formConfiguracionCombustible: FormGroup;
    formConfiguracionCasetas: FormGroup;
    empresaConfigReturn: EmpresaConfigModel = new EmpresaConfigModel();
    //Tooltips
    public tooltipCostoCombustibleAutomatico: string = "";
    public tooltipCostoCombustibleTarifa: string = "";
    public tooltipCostoCombustibleHistorico: string = "";
    public tooltipCostoCombustibleManual: string = "";
    public tooltipCostoCasetasAutomatico: string = "";
    public tooltipCostoCasetasHistorico: string = "";
    public tooltipCostoCasetasArchivo: string = "";


    constructor(public dialogRef: MatDialogRef<ConfirmarConfiguracionComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: fcGeneral, public formBuilder: FormBuilder, private _snackBar: MatSnackBar, private router: Router) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        console.log(this.data.configuracion);
        this.buildFormCombustible();
        this.buildFormCasetas();
        //Obtiene las variables enviadas al modal
        this.empresaConfiguracion = this.data.configuracion;
        this.rutaCalculada = this.data.rutaCalculada;
    }

    ngAfterContentInit() {
        console.log(this.empresaConfiguracion);
        //Combustible
        if (this.empresaConfiguracion.showCombustible) {
            this.boolShowFormCombustible = true;
            if (this.empresaConfiguracion.Var_ComCalculoAuto && this.rutaCalculada.costoCombustibleAutomatico > 0) {
                this.boolShowChkCombAutomatico = true;
                this.strLblChkCombAutomatico += ": $" + (this.rutaCalculada.costoCombustibleAutomatico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));// + " (" + this.rutaCalculada.tooltipCostoCombustibleAutomatico + ")";
                this.tooltipCostoCombustibleAutomatico = this.rutaCalculada.tooltipCostoCombustibleAutomatico;
            }
            if (this.empresaConfiguracion.Var_ComHistorico && this.rutaCalculada.costoCombustibleHistorico > 0) {
                this.boolShowChkCombHistorico = true;
                this.strLblChkCasHistorico += " $" + (this.rutaCalculada.costoCombustibleHistorico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));// + " (" + this.rutaCalculada.tooltipCostoCombustibleHistorico + ")";
                this.tooltipCostoCombustibleHistorico = this.rutaCalculada.tooltipCostoCombustibleHistorico; 
            }
            if (this.empresaConfiguracion.Var_ComTarifa && this.rutaCalculada.costoCombustibleTarifa > 0) {
                this.boolShowChkCombTarifa = true;
                this.strLblChkCombTarifa += " $" + (this.rutaCalculada.costoCombustibleTarifa.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));// + " (" + this.rutaCalculada.tooltipCostoCombustibleTarifa + ")";
                this.tooltipCostoCombustibleTarifa = this.rutaCalculada.tooltipCostoCombustibleTarifa;
            }
        }
        //Casetas
        if (this.empresaConfiguracion.showCasetas) {
            this.boolShowFormCasetas = true;
            if (this.empresaConfiguracion.Var_CasCalculoAuto && this.rutaCalculada.costoCasetasAutomatico > 0) {
                this.boolShowChkCasAutomatico = true;
                this.strLblChkCasAutomatico += " $" + (this.rutaCalculada.costoCasetasAutomatico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));// + " (" + this.rutaCalculada.tooltipCostoCasetasAutomatico + ")";
                this.tooltipCostoCasetasAutomatico = this.rutaCalculada.tooltipCostoCasetasAutomatico; 
            }
            if (this.empresaConfiguracion.Var_CasHistorico && this.rutaCalculada.costoCasetasHistorico > 0) {
                this.boolShowChkCasHistorico = true;
                this.strLblChkCasHistorico += " $" + (this.rutaCalculada.costoCasetasHistorico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));// + " (" + this.rutaCalculada.tooltipCostoCasetasHistorico + ")";
                this.tooltipCostoCasetasHistorico = this.rutaCalculada.tooltipCostoCasetasHistorico; 
            }
            if (this.empresaConfiguracion.Var_CasArchivo && this.rutaCalculada.costoCasetasArchivo > 0) {
                this.boolShowChkCasArchivo = true;
                this.strLblChkCasArchivo += " $" + (this.rutaCalculada.costoCasetasArchivo.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));// + " (" + this.rutaCalculada.tooltipCostoCasetasArchivo + ")";
                this.tooltipCostoCasetasArchivo = this.rutaCalculada.tooltipCostoCasetasArchivo; 
            }
        }
    }

    buildFormCombustible() {
        this.formConfiguracionCombustible = this.formBuilder.group({
            formCombAutomatico: this.formBuilder.control(null, []),
            formCombTarifa: this.formBuilder.control(null, []),
            formCombHistorico: this.formBuilder.control(null, []),
        });
    }

    buildFormCasetas() {
        this.formConfiguracionCasetas = this.formBuilder.group({
            formCasAutomatico: this.formBuilder.control(null, []),
            formCasHistorico: this.formBuilder.control(null, []),
            formCasArchivo: this.formBuilder.control(null, []),
        });
    }

    validateOptions(event: MatCheckboxChange, nameController: string): void {
        console.log(event.checked + '. ' + nameController);
        switch (nameController) {
            //*****COMBUSTIBLE*******
            case 'formCombAutomatico': {
                this.formConfiguracionCombustible.get("formCombTarifa").setValue(+0);
                this.formConfiguracionCombustible.get("formCombHistorico").setValue(+0);
                break;
            }
            case 'formCombTarifa': {
                this.formConfiguracionCombustible.get("formCombAutomatico").setValue(+0);
                this.formConfiguracionCombustible.get("formCombHistorico").setValue(+0);
                break;
            }
            case 'formCombHistorico': {
                this.formConfiguracionCombustible.get("formCombAutomatico").setValue(+0);
                this.formConfiguracionCombustible.get("formCombTarifa").setValue(+0);
                break;
            }
            //*****CASETAS*******
            case 'formCasAutomatico': {
                this.formConfiguracionCasetas.get("formCasHistorico").setValue(+0);
                break;
            }
            case 'formCasHistorico': {
                this.formConfiguracionCasetas.get("formCasAutomatico").setValue(+0);
                break;
            }
            default: {
                //statements; 
                break;
            }
        }
    }

    cerrarModal() {
        this.dialogRef.close();
        this.router.navigate(['pages/configuracion-costos-variables']);
    }

    aceptar() {
        var boolValid: boolean = false;
        if (this.empresaConfiguracion.showCombustible) {
            boolValid = false;
            if ((+this.formConfiguracionCombustible.get("formCombAutomatico").value) || (+this.formConfiguracionCombustible.get("formCombHistorico").value) || (+this.formConfiguracionCombustible.get("formCombTarifa").value)) {
                boolValid = true;
            }
        }
        //Casetas
        if (this.empresaConfiguracion.showCasetas) {
            boolValid = false;
            if ((+this.formConfiguracionCasetas.get("formCasHistorico").value) || (+this.formConfiguracionCasetas.get("formCasAutomatico").value) || (+this.formConfiguracionCasetas.get("formCasArchivo").value)) {
                boolValid = true;
            }
        }
        if (boolValid) {
            /*
             * Tipos de Cálculo:
             * -Combustible:
             *      0 - No aplica
             *      1 - Automático (de APIS INEGI y Rendimiento)
             *      2 - Cálculo por Tarifa (de Catálogo de Tarifas)
             *      3 - Histórico ZAM
             *      4 - Manual (factores configurados)
             * -Casetas:
             *      0 - No aplica
             *      1 - Automático (de APIS INEGI)
             *      2 - Histótico ZAM
             *      3 - Archivo (archivos de IAVE o TXT General)
             *      4 - Manual (factores configurados)
             * -Sueldos:
             *      0 - No aplica
             *      1 - Histórico ZAM
             *      2 - Manual (factores configurados)
             */
            //Valida el rubro seleccionado de Combustible
            if (+this.formConfiguracionCombustible.get("formCombAutomatico").value) {
                this.empresaConfigReturn.tipoCalculoCombustible = 1;
            } else if (+this.formConfiguracionCombustible.get("formCombTarifa").value) {
                this.empresaConfigReturn.tipoCalculoCombustible = 2;
            } else if (+this.formConfiguracionCombustible.get("formCombHistorico").value) {
                this.empresaConfigReturn.tipoCalculoCombustible = 3;
            }else {
                //Valida si por costo no omitió el por default el valor a seleccionar
                if (this.empresaConfiguracion.Var_ComCalculoAuto && this.rutaCalculada.costoCombustibleAutomatico > 0) {
                    this.empresaConfigReturn.tipoCalculoCombustible = 1;
                }
                if (this.empresaConfiguracion.Var_ComHistorico && this.rutaCalculada.costoCombustibleHistorico > 0) {
                    this.empresaConfigReturn.tipoCalculoCombustible = 3;
                }
                if (this.empresaConfiguracion.Var_ComTarifa && this.rutaCalculada.costoCombustibleTarifa > 0) {
                    this.empresaConfigReturn.tipoCalculoCombustible = 2;
                }
            }
            //Valida el rubro seleccionado de Casetas
            if (+this.formConfiguracionCasetas.get("formCasAutomatico").value) {
                this.empresaConfigReturn.tipoCalculoCasetas = 1;
            } else if (+this.formConfiguracionCasetas.get("formCasHistorico").value) {
                this.empresaConfigReturn.tipoCalculoCasetas = 2;
            } else if (+this.formConfiguracionCasetas.get("formCasArchivo").value) {
                this.empresaConfigReturn.tipoCalculoCasetas = 3;
            }else {
                //Valida si por costo no omitió el por default el valor a seleccionar
                if (this.empresaConfiguracion.Var_CasCalculoAuto && this.rutaCalculada.costoCasetasAutomatico > 0) {
                    this.empresaConfigReturn.tipoCalculoCasetas = 1;
                }
                if (this.empresaConfiguracion.Var_CasHistorico && this.rutaCalculada.costoCasetasHistorico > 0) {
                    this.empresaConfigReturn.tipoCalculoCasetas = 2;
                }
                if (this.empresaConfiguracion.Var_CasArchivo && this.rutaCalculada.costoCasetasArchivo > 0) {
                    this.empresaConfigReturn.tipoCalculoCasetas = 3;
                }
            }
            this.dialogRef.close({ data: this.empresaConfigReturn });
        } else {
            this.openSnackBar("Es necesario seleccionar una opción de cada rubro.", "");
        }
    }

    openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 2000,
        });
    }
}

export class EmpresaConfigModel {
    tipoCalculoCombustible: number;
    tipoCalculoCasetas: number;
    tipoCalculoSueldo: number;

    constructor() {
        this.tipoCalculoCombustible = 0;
        this.tipoCalculoCasetas = 0;
        this.tipoCalculoSueldo = 0;
    }
}