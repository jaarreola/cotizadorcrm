import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MatSnackBarConfig, MAT_DIALOG_DATA } from '@angular/material';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-aumentar-vigencia',
  templateUrl: './aumentar-vigencia.component.html',
  styleUrls: ['./aumentar-vigencia.component.scss']
})
export class AumentarVigenciaComponent implements OnInit {
    private urlApiConvenio = 'api/Convenio';

    config = new MatSnackBarConfig();

    fechaVigencia = new FormControl(null, [Validators.required]);

    constructor(public dialogRef: MatDialogRef<AumentarVigenciaComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: fcGeneral, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        console.log(this.data);
    }

    guardar() {
        if (this.fechaVigencia.valid) {
            this.updateFechaVigencia();
        } else {
            this.fechaVigencia.markAsTouched();
        }
    }

    updateFechaVigencia() {
        let vigenciaModel = {
            idConvenio: this.data.idConvenio,
            idCotizacion: this.data.idCotizacion,
            fechaVigencia: this.fechaVigencia.value
        }

        let json = JSON.stringify(vigenciaModel);

        this._service.postAPI(this.urlApiConvenio, "updateFechaVigenciaContrato", json).subscribe(
            result => {
                this._snackBar.open('Vigencia actualizada.', '', this.config);
                this.cerrarModal();
            },
            error => {
                console.log(error);
            }
        );
    }

    cerrarModal() {
        this.dialogRef.close();
    }

}
