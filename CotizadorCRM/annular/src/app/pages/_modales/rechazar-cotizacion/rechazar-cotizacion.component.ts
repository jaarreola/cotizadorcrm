﻿import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { fcGeneral } from '../../../services/general.service';


@Component({
    selector: 'app-rechazar-cotizacion',
    templateUrl: './rechazar-cotizacion.component.html',
    styleUrls: ['./rechazar-cotizacion.component.scss']
})
/** rechazar-cotizacion component*/
export class RechazarCotizacionComponent implements OnInit{
    private urlApiCommon = 'api/Common';
    private urlApiCotizador = 'api/Cotizador';
    public habilitaOtro: boolean = false;
    formRechazo: FormGroup;
    config = new MatSnackBarConfig();
    motivosRechazo: any[] = [];


    /** rechazar-cotizacion ctor */
    constructor(public dialogRef: MatDialogRef<RechazarCotizacionComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: fcGeneral,
        public formBuilder: FormBuilder,
        private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 3000;
    }

    ngOnInit() {
        this.consultarMonedas();
        this.buildFormContrato();
    }

    buildFormContrato() {
        this.formRechazo = this.formBuilder.group({
            formRechazo: this.formBuilder.control(null, [Validators.required]),
            formOtro: this.formBuilder.control(null, null)
        });
    }

    consultarMonedas() {
        this._service.getAPI(this.urlApiCommon, 'getCotizacionMotivosRechazoListado').subscribe(
            result => {
                this.motivosRechazo = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    guardar() {
        if (this.formRechazo.valid) {
            var otroRechazo: string = '';
            if (this.formRechazo.get("formOtro").value) {
                otroRechazo = this.formRechazo.get("formOtro").value;
            }
            //Valida si es OTRO; para requerir la descripción de otro
            if (+this.formRechazo.get("formRechazo").value == 99 && otroRechazo == '') {
                this._snackBar.open('Es necesario complementar la descripción de Otro', '', this.config);
            } else {
                let contrato = {
                    idCotizacion: +this.data.idCotizacion,
                    idEmpresa: +sessionStorage.getItem("idEmpresa"),
                    rechazoOtro: otroRechazo,
                    idCotizacionEstatus: 3,
                    idRechazo: +this.formRechazo.get("formRechazo").value
                };
                let json = JSON.stringify(contrato);
                console.log(json);
                this.metodoPost(json, 'setCotizacionEstatusRechazado');
            }
        }
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiCotizador, funcName, jsonParams).subscribe(
            result => {
                if (result == 0) {
                    this._snackBar.open('Verifique la informacion y vuelva a intentarlo', '', this.config);
                } else {
                    this._snackBar.open('Cotización actualziada correctamente.', '', this.config);
                    this.cerrarModal();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    selectedRechazo() {
        console.log(+this.formRechazo.get("formRechazo").value);
        if (+this.formRechazo.get("formRechazo").value == 99) {
            this.habilitaOtro = true;
        } else {
            this.habilitaOtro = false;
        }
    }

    cerrarModal() {
        this.dialogRef.close();
    }

}