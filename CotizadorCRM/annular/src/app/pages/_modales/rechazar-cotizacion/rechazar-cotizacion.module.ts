﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

import { RechazarCotizacionComponent } from './rechazar-cotizacion.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        RechazarCotizacionComponent
    ],
    providers: [
        fcGeneral
    ],
    entryComponents: [RechazarCotizacionComponent]
})
export class RechazarCotizacionModule { }