import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-generar-contrato',
  templateUrl: './generar-contrato.component.html',
  styleUrls: ['./generar-contrato.component.scss']
})
export class GenerarContratoComponent implements OnInit {
    private urlApiCliente = 'api/Cliente';
    private urlApiCotizador = 'api/Cotizador';

    clientes: any[] = [];
    config = new MatSnackBarConfig();
    camposVisibles = true;

    formContrato: FormGroup;
    constructor(public dialogRef: MatDialogRef<GenerarContratoComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
        private _service: fcGeneral, public formBuilder: FormBuilder, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.buildFormContrato();

        this.consultarClientes();
    }

    buildFormContrato() {
        this.formContrato = this.formBuilder.group({
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formRemitente: this.formBuilder.control(null, [Validators.required]),
            formDestinatario: this.formBuilder.control(null, [Validators.required]),
            formFecha: this.formBuilder.control(null, [Validators.required]),
            formFechaFin: this.formBuilder.control(null, [Validators.required]),
        });
    }

    consultarClientes() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiCliente, 'getClientesListaAutocomplete', json).subscribe(
            result => {
                this.clientes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        let contrato = {
            idCotizacion: +this.data.idCotizacion,
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            nombre: this.formContrato.get("formNombre").value,
            idClienteOrigen: +this.formContrato.get("formRemitente").value,
            idClienteDestino: +this.formContrato.get("formDestinatario").value,
            fecha: this.formContrato.get("formFecha").value,
            fechaFin: this.formContrato.get("formFechaFin").value,
            idCotizacionEstatus: 2
        };

        let json = JSON.stringify(contrato);
        this.metodoPost(json, 'setCotizacionEstatus');
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiCotizador, funcName, jsonParams).subscribe(
            result => {
                if (result == 0) {
                    this._snackBar.open('Verifique la informacion y vuelva a intentarlo', '', this.config);
                } else {
                    this._snackBar.open('Registro guardado con exito', '', this.config);
                    this.cerrarModal();
                }
            },
            error => {
                console.log(error);
            }
        );
    }

    cerrarModal() {
        this.dialogRef.close();
    }

}
