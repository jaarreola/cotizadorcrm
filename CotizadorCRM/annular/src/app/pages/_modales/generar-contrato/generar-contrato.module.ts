﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { GenerarContratoComponent } from './generar-contrato.component';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        GenerarContratoComponent
    ],
    providers: [
        fcGeneral
    ],
    entryComponents: [GenerarContratoComponent]
})
export class GenerarContratoModule { }