import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.scss']
})
export class ServiciosComponent implements OnInit {
    private urlApiServicios = 'api/Servicios';
    private urlApiTiposOperacion = 'api/TiposOperacion';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'idServicio',
        'servicio',
        'descripcion',
        'tipoOperacion',
        'estatus'
    ];

    public servicios: any[] = [];
    public tiposServicio: any[] = [];
    public tiposOperacion: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        idTipoServicio: 0,
        idTipoOperacion: 0,
        nombre: "",
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    filtros = true;
    constructor(private router: Router, private _service: fcGeneral) { }

    ngOnInit() {
        this.consultarTiposServicio();
        this.consultarTiposOperacion();
        this.consultarServicios();
    }

    consultarTiposServicio() {
        this._service.getAPI(this.urlApiCommon, 'getTiposServiciosListado').subscribe(
            result => {
                this.tiposServicio = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposOperacion() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiTiposOperacion, 'getTiposOperacionByEmpresaArea', json).subscribe(
            result => {
                this.tiposOperacion = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarServicios() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiServicios, 'getServiciosLista', json).subscribe(
            result => {
                this.servicios = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.servicios);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    nuevo() {
        this.router.navigate(['pages/servicios/servicio', 'N']);
    }

    editar(servicio) {
        this.router.navigate(['pages/servicios/servicio', servicio.idServicio]);
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idTipoServicio: 0,
            idTipoOperacion: 0,
            nombre: "",
            estatus: ""
        }
    }

}
