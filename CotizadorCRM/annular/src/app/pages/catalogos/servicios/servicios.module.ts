﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ServiciosComponent } from './servicios.component';
import { AgregarEditarServicioComponent } from './agregar-editar-servicio/agregar-editar-servicio.component';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

export const routes = [
    { path: '', component: ServiciosComponent, pathMatch: 'full' },
    { path: 'servicio/:id', component: AgregarEditarServicioComponent, data: { breadcrumb: 'Servicio' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        ServiciosComponent,
        AgregarEditarServicioComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class ServiciosModule { }