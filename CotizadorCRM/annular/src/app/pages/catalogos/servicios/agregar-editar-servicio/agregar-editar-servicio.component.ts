import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { fcGeneral } from '../../../../services/general.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-agregar-editar-servicio',
  templateUrl: './agregar-editar-servicio.component.html',
  styleUrls: ['./agregar-editar-servicio.component.scss']
})
export class AgregarEditarServicioComponent implements OnInit {
    private urlApiServicios = 'api/Servicios';
    private urlApiTiposOperacion = 'api/TiposOperacion';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    public servicio: ServicioModel = new ServicioModel();
    public idServicio: any;

    public tiposServicio: any[] = [];
    public tiposOperacion: any[] = [];
    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    formServicio: FormGroup;
    constructor(private router: Router, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.buildForm();
        this.consultarTiposServicio();
        this.consultarTiposOperacion();

        this.route.params.subscribe(params => {
            this.idServicio = params['id'];

            if (this.idServicio != "N") {
                this.idServicio = +params['id'];

                this.consultarServicio();
            } else {
                this.idServicio = 0;
            }
        });
    }

    buildForm() {
        this.formServicio = this.formBuilder.group({
            formNoServicio: this.formBuilder.control(null, []),
            formTipoServicio: this.formBuilder.control(null, [Validators.required]),
            formTipoOperacion: this.formBuilder.control(null, [Validators.required]),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required])
        });

        const controlNoServicio = this.formServicio.get("formNoServicio");
        controlNoServicio.disable();
    }

    consultarTiposServicio() {
        this._service.getAPI(this.urlApiCommon, 'getTiposServiciosListado').subscribe(
            result => {
                this.tiposServicio = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposOperacion() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiTiposOperacion, 'getTiposOperacionByEmpresaArea', json).subscribe(
            result => {
                this.tiposOperacion = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarServicio() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idServicio: this.idServicio
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiServicios, 'getServicioById', json).subscribe(
            result => {
                this.servicio = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        if (this.formServicio.valid) {
            if (this.idServicio == 0) {
                this.servicio.idServicio = 0;
            }
            let json = JSON.stringify(this.servicio);

            this.metodoPost(json, 'setServicio');
        } else {
            Object.keys(this.formServicio.controls).forEach(key => {
                const ctrl = this.formServicio.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    regresar() {
        this.router.navigate(['pages/servicios']);
    }

    nuevo() {
        this.servicio = new ServicioModel();
        this.router.navigate(['pages/servicios/servicio', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiServicios, funcName, jsonParams).subscribe(
            result => {
                this.idServicio = +result.idServicio;
                this.servicio.idServicio = this.idServicio;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}

export class ServicioModel {
    idServicio: number;
    idEmpresa: number;
    idArea: number;
    idTipoServicio: number;
    idTipoOperacion: number;
    nombre: string;
    estatus: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idArea = 1;
    }
}
