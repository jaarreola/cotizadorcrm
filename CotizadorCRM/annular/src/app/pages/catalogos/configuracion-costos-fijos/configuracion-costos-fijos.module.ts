﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { ConfiguracionCostosFijosComponent } from './configuracion-costos-fijos.component';

export const routes = [
    { path: '', component: ConfiguracionCostosFijosComponent, pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule
    ],
    declarations: [
        ConfiguracionCostosFijosComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class ConfiguracionCostosFijosModule { }