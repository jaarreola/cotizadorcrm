import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-configuracion-costos-fijos',
  templateUrl: './configuracion-costos-fijos.component.html',
  styleUrls: ['./configuracion-costos-fijos.component.scss']
})
export class ConfiguracionCostosFijosComponent implements OnInit {
    private urlApiConfiguracion = 'api/EmpresaConfiguracion';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    tiposFactores: any[] = [];
    configuracion: any = {};
    costosFijos: CostosFijosModel = new CostosFijosModel();
    formCostosFijosCabecera: FormGroup;

    //listadoGastos: GastoModel[] = new Array<GastoModel>();
    formGastos: FormArray;

    constructor(private _service: fcGeneral, private _snackBar: MatSnackBar, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.consultarTiposFactores();
        this.consultarConfiguracion();
        //Inicializa listado de sueldos dinámico
        //this.buildFormGastos();
        this.buildForm();
        this.consultaCostosFijos();
    }

    buildForm() {
        this.formCostosFijosCabecera = this.formBuilder.group({
            formEjercicio: this.formBuilder.control(null, [Validators.required]),
            formTotalViajes: this.formBuilder.control({ disabled: true }, [Validators.required]),
            formTotalKms: this.formBuilder.control({ disabled: true }, [Validators.required]),
            //formFactorInflacion: this.formBuilder.control(null, [Validators.required]),
            //formFactorProteccion: this.formBuilder.control(null, [Validators.required])
        });
    }

    consultaCostosFijos() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiConfiguracion, 'getCostosFijos', json).subscribe(
            result => {
                console.log(result);
                this.costosFijos = result;
                this.buildFormGastos();
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposFactores() {
        this._service.getAPI(this.urlApiCommon, 'getConfiguracionTipoFactor').subscribe(
            result => {
                this.tiposFactores = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarConfiguracion() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            filtro: 2
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiConfiguracion, 'getEmpresaConfiguracion', json).subscribe(
            result => {
                this.configuracion = result;
                console.log(this.configuracion);
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        this.configuracion.operativo_mtto_monto = +this.configuracion.operativo_mtto_monto;
        this.configuracion.operativo_mtto_chk_analisis = +this.configuracion.operativo_mtto_chk_analisis;
        this.configuracion.operativo_llantas_monto = +this.configuracion.operativo_llantas_monto;
        this.configuracion.operativo_llantas_chk_analisis = +this.configuracion.operativo_llantas_chk_analisis;
        this.configuracion.operativo_admin_monto = +this.configuracion.operativo_admin_monto;

        let json = JSON.stringify(this.configuracion);

        this._service.postAPI(this.urlApiConfiguracion, 'setEmpresaConfigFijos', json).subscribe(
            result => {
                this._snackBar.open('Configuración guardada con exito', '', this.config);
                this.configuracion = result;
                console.log(this.configuracion);
            },
            error => {
                console.log(error);
            }
        );
    }

    /*Funciones para Listado de Sueldos Dinpamicos*/
    agregarGasto() {
        this.costosFijos.gastos.push(new GastoModel());

        let group = new FormGroup({
            descripcion: new FormControl(null, [Validators.required]),
            total: new FormControl(null, [Validators.required]),
            idTipoFactor: new FormControl(null, [Validators.required]),
            factorViaje: new FormControl(null, [Validators.required]),
            factorKms: new FormControl(null, [Validators.required]),
        });
        this.formGastos.push(group);
    }

    buildFormGastos() {
        let groups = this.costosFijos.gastos.map(sueldo => {
            return new FormGroup({
                descripcion: new FormControl(sueldo.descripcion, [Validators.required]),
                total: new FormControl(sueldo.total, [Validators.required]),
                idTipoFactor: new FormControl(sueldo.idTipoFactor, [Validators.required]),
                factorKms: new FormControl(sueldo.factorKms, [Validators.required]),
                factorViaje: new FormControl(sueldo.factorViaje, [Validators.required]),
            });
        });
        this.formGastos = new FormArray(groups);
    }

    getControl(index, campo) {
        return this.formGastos.at(index).get(campo) as FormControl;
    }

    seleccionaFactor(index) {
        (this.formGastos.at(index).get('factorKms') as FormControl).setValue("1");
        //console.log();
    }

    eliminaGasto(index) {
        this.costosFijos.gastos.splice(index, 1);
    }
}

export class CostosFijosModel {
    idEmpresa: number;
    ejercicio: number;
    totalViajes: number;
    totalKms: number;
    factorInflacion: number;
    factorProteccion: number;
    gastos: GastoModel[];

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.ejercicio = 0;
        this.totalKms = 0;
        this.totalViajes = 0;
        this.factorInflacion = 0;
        this.factorProteccion = 0;
        this.gastos = new Array<GastoModel>();
    }
}


export class GastoModel {
    idEmpresa: number;
    consecutivo: number;
    idTipoFactor: number;
    descripcion: string;
    total: number;
    factorKms: number;
    factorViaje: number;

    constructor() {
        this.total = 0;
        this.consecutivo = 0;
        this.idTipoFactor = 0;
        this.descripcion = "";
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
    }
}
