import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { MatDialog, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';
import { fcGeneral } from '../../../../services/general.service';

@Component({
  selector: 'app-agregar-editar-vehiculo',
  templateUrl: './agregar-editar-vehiculo.component.html',
  styleUrls: ['./agregar-editar-vehiculo.component.scss']
})
export class AgregarEditarVehiculoComponent implements OnInit {
    private urlApiVehiculos = 'api/Vehiculos';
    private urlApiCommon = 'api/Common';
    private urlApiProductos = 'api/Productos';

    config = new MatSnackBarConfig();

    clasificaciones: any[] = [];
    combustibles: any[] = [];
    productos: any[] = [];

    idVehiculoArmado: any;
    vehiculo: VehiculoModel = new VehiculoModel();

    //Agrega Listado Dinámico para Sueldos
    formProductos: FormArray;


    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    public unidadMedida = [
        { id: 1, unidadMedida: 'Tonelada' },
        { id: 2, unidadMedida: 'KG' },
        { id: 3, unidadMedida: 'M3' },
        { id: 4, unidadMedida: 'Litros' },
        { id: 5, unidadMedida: 'Unitario' }
    ];

    guardado = false;
    formVehiculos: FormGroup;
    constructor(private router: Router, public formBuilder: FormBuilder, public dialog: MatDialog, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.buildForm();
        this.consultarClasificaciones();
        this.consultarCombustibles();
        this.consultarProductos();
        this.route.params.subscribe(params => {
            this.idVehiculoArmado = params['id'];

            if (this.idVehiculoArmado != "N") {
                this.idVehiculoArmado = +params['id'];
                console.log(this.idVehiculoArmado);
                this.consultarVehiculo();
            } else {
                this.idVehiculoArmado = 0;
                this.vehiculo.rendimiento = 0;
                this.vehiculo.rendimientoUrea = 0;
                this.vehiculo.cargaMin = 0;
                this.vehiculo.cargaMax = 0;
                this.buildFormProductos();
            }
        });
    }

    buildForm() {
        this.formVehiculos = this.formBuilder.group({
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formTipoCombustible: this.formBuilder.control(null, [Validators.required]),
            formClasificacion: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required]),
            formRendimiento: this.formBuilder.control(null, [Validators.required]),
            formRendimientoUrea: this.formBuilder.control(null, [Validators.required]),
            formUnidadMedida: this.formBuilder.control(null, [Validators.required]),
            formCargaMin: this.formBuilder.control(null, [Validators.required]),
            formCargaMax: this.formBuilder.control(null, [Validators.required])
        });
    }

    buildFormProductos() {
        let groups = this.vehiculo.productos.map(producto => {
            return new FormGroup({
                idProducto: new FormControl(producto.idProducto, [Validators.required]),
                descripcion: new FormControl(producto.descripcion, [Validators.required]),
            });
        });

        this.formProductos = new FormArray(groups);
    }
    getControl(index, campo) {
        return this.formProductos.at(index).get(campo) as FormControl;
    }

    getControlProducto(index, campo) {
        return this.formProductos.at(index).get(campo) as FormControl;
    }

    seleccionarProducto(index) {
        const controlDescripcion = this.getControlProducto(index, 'descripcion');
        let p = this.productos.find(producto => producto.id === controlDescripcion.value);

        const controlProducto = this.getControlProducto(index, 'idProducto');
        if (p != undefined) {
            controlProducto.setValue(p.id);
        } else {
            controlProducto.setValue(null);
        }
    }

    agregarProducto() {
        this.vehiculo.productos.push(new ProductosModel());

        let group = new FormGroup({
            idProducto: new FormControl(null, [Validators.required]),
            descripcion: new FormControl(null, [Validators.required]),
        });
        this.formProductos.push(group);
    }

    consultarClasificaciones() {
        this._service.getAPI(this.urlApiCommon, 'getVehiculoClasificacionListado').subscribe(
            result => {
                this.clasificaciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarCombustibles() {
        this._service.getAPI(this.urlApiCommon, 'getCombustibleTiposListado').subscribe(
            result => {
                this.combustibles = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarVehiculo() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idVehiculoArmado: this.idVehiculoArmado
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiVehiculos, 'getVehiculoById', json).subscribe(
            result => {
                this.vehiculo = result;
                this.buildFormProductos();
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        if (this.formVehiculos.valid) {
            if (this.idVehiculoArmado == 0) {
                this.vehiculo.idVehiculoArmado = 0;
            }
            //Render para request.
            this.vehiculo.rendimiento = +this.vehiculo.rendimiento;
            this.vehiculo.rendimientoUrea = +this.vehiculo.rendimientoUrea;
            this.vehiculo.cargaMin = +this.vehiculo.cargaMin;
            this.vehiculo.cargaMax = +this.vehiculo.cargaMax;
            let json = JSON.stringify(this.vehiculo);
            console.log(json);
            this.metodoPost(json, 'setVehiculo');
        } else {
            Object.keys(this.formVehiculos.controls).forEach(key => {
                const ctrl = this.formVehiculos.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    regresar() {
        if (!this.guardado) {
            const dialogRef = this.dialog.open(ConfirmacionComponent, {
                width: '35%',
                disableClose: true,
                data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.router.navigate(['pages/vehiculos']);
                }
            });
        } else {
            this.router.navigate(['pages/vehiculos']);
        }
    }

    nuevo() {
        this.vehiculo = new VehiculoModel();
        this.router.navigate(['pages/vehiculos/vehiculo', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiVehiculos, funcName, jsonParams).subscribe(
            result => {
                this.idVehiculoArmado = +result.idVehiculoArmado;
                this.vehiculo.idVehiculoArmado = this.idVehiculoArmado;
                this.guardado = true;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarProductos() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiProductos, 'getProductosAutocomplete', json).subscribe(
            result => {
                this.productos = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    eliminaProducto(index) {
        this.vehiculo.productos.splice(index, 1);
    }
}

export class VehiculoModel {
    idVehiculoArmado: number;
    idEmpresa: number;
    idArea: number;
    idVehiculoClasificacion: number;
    idCombustibleTipo: number;
    nombre: string;
    estatus: string;
    rendimiento: number;
    unidadMedida: number;
    cargaMin: number;
    cargaMax: number;
    productos: ProductosModel[];
    rendimientoUrea: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idArea = 1;
        this.rendimiento = 0;
        this.rendimientoUrea = 0;
        this.cargaMin = 0;
        this.cargaMax = 0;
        this.productos = new Array<ProductosModel>();
    }
}

export class ProductosModel {
    idVehiculoArmado: number;
    idEmpresa: number;
    idProducto: number;
    descripcion: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idVehiculoArmado = 0;
        this.idProducto = 0
        this.descripcion = "";
    }
}