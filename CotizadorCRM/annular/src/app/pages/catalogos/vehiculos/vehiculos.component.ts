import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.scss']
})
export class VehiculosComponent implements OnInit {
    private urlApiVehiculos = 'api/Vehiculos';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'armado',
        'clasificacion',
        'tipoCombustible',
        'estatus'
    ];

    vehiculos: any[] = [];

    clasificaciones: any[] = [];
    ejes: any[] = [];
    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        nombre: "",
        idVehiculoClasificacion: 0,
        //idVehiculoEje: 0,
        estatus: ""
    }

    filtros = true;
    constructor(private router: Router, private _service: fcGeneral) { }

    ngOnInit() {
        this.consultarClasificaciones();
        this.consultarEjes();

        this.consultarVehiculos();
    }

    nuevo() {
        this.router.navigate(['pages/vehiculos/vehiculo', 'N']);
    }

    editar(vehiculo) {
        this.router.navigate(['pages/vehiculos/vehiculo', vehiculo.idVehiculoArmado]);
    }

    consultarClasificaciones() {
        this._service.getAPI(this.urlApiCommon, 'getVehiculoClasificacionListado').subscribe(
            result => {
                this.clasificaciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarEjes() {
        this._service.getAPI(this.urlApiCommon, 'getVehiculoEjesListado').subscribe(
            result => {
                this.ejes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarVehiculos() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiVehiculos, 'getVehiculosLista', json).subscribe(
            result => {
                this.vehiculos = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.vehiculos);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            nombre: "",
            idVehiculoClasificacion: 0,
            //idVehiculoEje: 0,
            estatus: ""
        }
    }

}
