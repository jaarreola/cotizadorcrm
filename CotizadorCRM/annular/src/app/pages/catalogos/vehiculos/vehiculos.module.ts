﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { VehiculosComponent } from './vehiculos.component';
import { AgregarEditarVehiculoComponent } from './agregar-editar-vehiculo/agregar-editar-vehiculo.component';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';


export const routes = [
    { path: '', component: VehiculosComponent, pathMatch: 'full' },
    { path: 'vehiculo/:id', component: AgregarEditarVehiculoComponent, data: { breadcrumb: 'Vehiculo' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule
    ],
    declarations: [
        VehiculosComponent,
        AgregarEditarVehiculoComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class VehiculosModule { }