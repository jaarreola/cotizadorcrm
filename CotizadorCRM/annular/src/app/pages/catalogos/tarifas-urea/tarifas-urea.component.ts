﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { fcGeneral } from '../../../services/general.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-tarifas-urea',
    templateUrl: './tarifas-urea.component.html',
    styleUrls: ['./tarifas-urea.component.scss']
})


export class TarifasUreaComponent {
    private urlApiTarifas = 'api/UreaTarifas';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'noTarifa',
        'nombre',
        'costo',
        'estatus'
    ];

    tarifas: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        nombre: "",
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    filtros = true;
    constructor(private _service: fcGeneral, private router: Router) { }

    ngOnInit() {
        this.consultarTarifas();
    }

    consultarTarifas() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiTarifas, 'getUreaTarifasLista', json).subscribe(
            result => {
                this.tarifas = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.tarifas);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    nuevo() {
        this.router.navigate(['pages/tarifas-urea/tarifa-urea', 'N']);
    }

    editar(tarifa) {
        this.router.navigate(['pages/tarifas-urea/tarifa-urea', tarifa.idUreaTarifa]);
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            nombre: "",
            estatus: ""
        }
    }

}