﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { TarifasUreaComponent } from './tarifas-urea.component';
import { TarifaUreaComponent } from './tarifa-urea/tarifa-urea.component';
import { SharedModule } from '../../../shared/shared.module';

export const routes = [
    { path: '', component: TarifasUreaComponent, pathMatch: 'full' },
    { path: 'tarifa-urea/:id', component: TarifaUreaComponent, data: { breadcrumb: 'Tarifa UREA' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        TarifasUreaComponent,
        TarifaUreaComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class TarifasUreaModule { }