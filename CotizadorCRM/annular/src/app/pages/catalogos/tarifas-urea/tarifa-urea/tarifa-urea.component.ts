﻿import { Component, OnInit } from '@angular/core';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { fcGeneral } from '../../../../services/general.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
    selector: 'app-tarifa-urea',
    templateUrl: './tarifa-urea.component.html',
    styleUrls: ['./tarifa-urea.component.scss']
})
/** tarifa-urea component*/
export class TarifaUreaComponent {
    private urlApiTarifa = 'api/UreaTarifas';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    tarifa: any = {};

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    idUreaTarifa: any;
    formTarifa: FormGroup;
    constructor(private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar, private router: Router, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.buildForm();

        this.route.params.subscribe(params => {
            this.idUreaTarifa = params['id'];

            if (this.idUreaTarifa != "N") {
                this.idUreaTarifa = +params['id'];

                this.consultarTarifa();
            } else {
                this.idUreaTarifa = 0;
            }
        });
    }

    buildForm() {
        this.formTarifa = this.formBuilder.group({
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formCosto: this.formBuilder.control(null, [Validators.required, Validators.pattern("\\d+(\\.\\d+)?")]),
            formEstatus: this.formBuilder.control(null, [Validators.required])
        });
    }

    consultarTarifa() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idUreaTarifa: this.idUreaTarifa
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTarifa, 'getUreaTarifaById', json).subscribe(
            result => {
                this.tarifa = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    nuevo() {
        this.tarifa = {};
        this.router.navigate(['pages/tarifas-urea/tarifa-urea', 'N']);
    }

    regresar() {
        this.router.navigate(['pages/tarifas-urea']);
    }

    guardar() {
        if (this.formTarifa.valid) {
            if (this.idUreaTarifa == 0) {
                this.tarifa.idUreaTarifa = 0;
            }
            //Renderea los campos numéricos (deciales, enteros, etc...) para evitar BarRequest 400 en llamado a API
            this.tarifa.costo = +this.tarifa.costo
            //Llama API de Inserta/Actualiza
            this.tarifa.idEmpresa = +sessionStorage.getItem("idEmpresa")
            let json = JSON.stringify(this.tarifa);
            this.metodoPost(json, 'setUreaTarifa');
        } else {
            Object.keys(this.formTarifa.controls).forEach(key => {
                const ctrl = this.formTarifa.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiTarifa, funcName, jsonParams).subscribe(
            result => {
                this.idUreaTarifa = +result.idUreaTarifa;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }
}