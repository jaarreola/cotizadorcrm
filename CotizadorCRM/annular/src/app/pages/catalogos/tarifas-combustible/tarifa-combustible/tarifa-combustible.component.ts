import { Component, OnInit } from '@angular/core';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { fcGeneral } from '../../../../services/general.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-tarifa-combustible',
  templateUrl: './tarifa-combustible.component.html',
  styleUrls: ['./tarifa-combustible.component.scss']
})
export class TarifaCombustibleComponent implements OnInit {
    private urlApiTarifa = 'api/CombustibleTarifas';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    tarifa: any = {};

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    idCombustibleTarifa: any;
    formTarifa: FormGroup;
    constructor(private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar, private router: Router, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.buildForm();

        this.route.params.subscribe(params => {
            this.idCombustibleTarifa = params['id'];

            if (this.idCombustibleTarifa != "N") {
                this.idCombustibleTarifa = +params['id'];

                this.consultarTarifa();
            } else {
                this.idCombustibleTarifa = 0;
            }
        });
    }

    buildForm() {
        this.formTarifa = this.formBuilder.group({
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formCostoRegular: this.formBuilder.control(null, [Validators.required, Validators.pattern("\\d+(\\.\\d+)?")]),
            formCostoPremium: this.formBuilder.control(null, [Validators.required, Validators.pattern("\\d+(\\.\\d+)?")]),
            formCostoDiesel: this.formBuilder.control(null, [Validators.required, Validators.pattern("\\d+(\\.\\d+)?")]),
            formEstatus: this.formBuilder.control(null, [Validators.required])
        });
    }

    consultarTarifa() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idCombustibleTarifa: this.idCombustibleTarifa
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTarifa, 'getCombustibleTarifaById', json).subscribe(
            result => {
                this.tarifa = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    nuevo() {
        this.tarifa = {};
        this.router.navigate(['pages/tarifas-combustible/tarifa-combustible', 'N']);
    }

    regresar() {
        this.router.navigate(['pages/tarifas-combustible']);
    }

    guardar() {
        if (this.formTarifa.valid) {
            if (this.idCombustibleTarifa == 0) {
                this.tarifa.idCombustibleTarifa = 0;
            }
            //Renderea los campos numéricos (deciales, enteros, etc...) para evitar BarRequest 400 en llamado a API
            this.tarifa.costoRegular = +this.tarifa.costoRegular
            this.tarifa.costoPremium = +this.tarifa.costoPremium
            this.tarifa.costoDiesel = +this.tarifa.costoDiesel
            //Llama API de Inserta/Actualiza
            this.tarifa.idEmpresa = +sessionStorage.getItem("idEmpresa")
            let json = JSON.stringify(this.tarifa);
            this.metodoPost(json, 'setCombustibleTarifa');
        } else {
            Object.keys(this.formTarifa.controls).forEach(key => {
                const ctrl = this.formTarifa.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiTarifa, funcName, jsonParams).subscribe(
            result => {
                this.idCombustibleTarifa = +result.idCombustibleTarifa;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}
