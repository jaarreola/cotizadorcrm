import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { fcGeneral } from '../../../services/general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tarifas-combustible',
  templateUrl: './tarifas-combustible.component.html',
  styleUrls: ['./tarifas-combustible.component.scss']
})
export class TarifasCombustibleComponent implements OnInit {
    private urlApiTarifas = 'api/CombustibleTarifas';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'noTarifa',
        'nombre',
        'costoRegular',
        'costoPremium',
        'costoDiesel',
        'estatus'
    ];

    tarifas: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        nombre: "",
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    filtros = true;
    constructor(private _service: fcGeneral, private router: Router) { }

    ngOnInit() {
        this.consultarTarifas();
    }

    consultarTarifas() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiTarifas, 'getCombustibleTarifasLista', json).subscribe(
            result => {
                this.tarifas = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.tarifas);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    nuevo() {
        this.router.navigate(['pages/tarifas-combustible/tarifa-combustible', 'N']);
    }

    editar(tarifa) {
        this.router.navigate(['pages/tarifas-combustible/tarifa-combustible', tarifa.idCombustibleTarifa]);
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            nombre: "",
            estatus: ""
        }
    }

}
