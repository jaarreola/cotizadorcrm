﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { TarifasCombustibleComponent } from './tarifas-combustible.component';
import { TarifaCombustibleComponent } from './tarifa-combustible/tarifa-combustible.component';
import { SharedModule } from '../../../shared/shared.module';

export const routes = [
    { path: '', component: TarifasCombustibleComponent, pathMatch: 'full' },
    { path: 'tarifa-combustible/:id', component: TarifaCombustibleComponent, data: { breadcrumb: 'Tarifa Combustible' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        TarifasCombustibleComponent,
        TarifaCombustibleComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class TarifasCombustibleModule { }