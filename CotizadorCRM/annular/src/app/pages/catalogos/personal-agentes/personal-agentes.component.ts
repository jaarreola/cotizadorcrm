import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-personal-agentes',
  templateUrl: './personal-agentes.component.html',
  styleUrls: ['./personal-agentes.component.scss']
})
export class PersonalAgentesComponent implements OnInit {
    private urlApiPersonal = 'api/Personal';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'numero',
        'nombre',
        'tipo',
        'telefono',
        'correo',
        'area',
        'estatus'
    ];

    public agentes: any[] = [];
    public personalTipos: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        nombre: "",
        idPersonalTipo: 0,
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    filtros = true;
    constructor(private router: Router, private _service: fcGeneral) { }

    ngOnInit() {
        this.consultarPersonalTipos();
        this.consultarPersonal();
    }

    nuevo() {
        this.router.navigate(['pages/personal-agentes/personal-agente', 'N']);
    }

    editar(personal) {
        this.router.navigate(['pages/personal-agentes/personal-agente', personal.idPersonal]);
    }

    consultarPersonalTipos() {
        this._service.getAPI(this.urlApiCommon, 'getPersonalTipoListado').subscribe(
            result => {
                this.personalTipos = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPersonal() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiPersonal, 'getPersonalLista', json).subscribe(
            result => {
                this.agentes = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.agentes);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            nombre: "",
            idPersonalTipo: 0,
            estatus: ""
        }
    }

}
