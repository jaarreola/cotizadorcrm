﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { PersonalAgentesComponent } from './personal-agentes.component';;
import { AgregarEditarPersonalAgenteComponent } from './agregar-editar-personal-agente/agregar-editar-personal-agente.component'
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

export const routes = [
    { path: '', component: PersonalAgentesComponent, pathMatch: 'full' },
    { path: 'personal-agente/:id', component: AgregarEditarPersonalAgenteComponent, data: { breadcrumb: 'Personal/Agente' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule
    ],
    declarations: [
        PersonalAgentesComponent,
        AgregarEditarPersonalAgenteComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class PersonalAgentesModule { }