import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';
import { fcGeneral } from '../../../../services/general.service';

@Component({
  selector: 'app-agregar-editar-personal-agente',
  templateUrl: './agregar-editar-personal-agente.component.html',
  styleUrls: ['./agregar-editar-personal-agente.component.scss']
})
export class AgregarEditarPersonalAgenteComponent implements OnInit {
    private urlApiPersonal = 'api/Personal';
    private urlApiCommon = 'api/Common';
    private urlApiAreas = 'api/Areas';

    config = new MatSnackBarConfig();

    idPersonal: any;
    personal: PersonalModel = new PersonalModel();
    areas: any[] = [];
    personalTipos: any[] = [];
    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    esAgente = false;
    guardado = false;
    formPersonal: FormGroup;
    constructor(private router: Router, public formBuilder: FormBuilder, public dialog: MatDialog, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.buildForm();
        this.consultarAreas();
        this.consultarPersonalTipos();

        this.route.params.subscribe(params => {
            this.idPersonal = params['id'];

            if (this.idPersonal != "N") {
                this.idPersonal = +params['id'];

                this.consultarPersonal();
            } else {
                this.idPersonal = 0;
            }
        });
    }

    buildForm() {
        this.formPersonal = this.formBuilder.group({
            //formNoEmpleado: this.formBuilder.control(null, [Validators.required, Validators.pattern('^\\d+$')]),
            formNoEmpleado: this.formBuilder.control(null, []),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required]),
            formArea: this.formBuilder.control(null, [Validators.required]),
            formTipo: this.formBuilder.control(null, [Validators.required]),
            formTelefono: this.formBuilder.control(null, [Validators.pattern('^\\d{10}$')]),
            formCorreo: this.formBuilder.control(null, [Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]),
            formFecha: this.formBuilder.control(null, [Validators.required]),
            formUsuario: this.formBuilder.control(null, []),
            formPass: this.formBuilder.control(null, [])
        });

        const controlNoEmpleado = this.formPersonal.get("formNoEmpleado");
        controlNoEmpleado.disable();
    }

    tipoSeleccionado() {
        const controlUsuario = this.formPersonal.get("formUsuario");
        const controlPass = this.formPersonal.get("formPass");
        if (this.personal.idPersonalTipo != 2) {
            controlUsuario.disable();
            controlPass.disable();
            this.esAgente = false;
        } else {
            controlUsuario.enable();
            controlPass.enable();
            this.esAgente = true;

            if (this.personal.usuario == null) {
                this.personal.usuario = this.personal.correo;
            }
        }
    }

    consultarAreas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiAreas, 'getAreasByEmpresaLista', json).subscribe(
            result => {
                this.areas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPersonalTipos() {
        this._service.getAPI(this.urlApiCommon, 'getPersonalTipoListado').subscribe(
            result => {
                this.personalTipos = result;
                console.log(this.personalTipos);
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPersonal() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idPersonal: this.idPersonal
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPersonal, 'getPersonalById', json).subscribe(
            result => {
                this.personal = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        if (this.formPersonal.valid) {
            if (this.idPersonal == 0) {
                this.personal.idPersonal = 0;
            }
            let json = JSON.stringify(this.personal);

            this.metodoPost(json, 'setPersonal');
        } else {
            Object.keys(this.formPersonal.controls).forEach(key => {
                const ctrl = this.formPersonal.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    regresar() {
        if (!this.guardado) {
            const dialogRef = this.dialog.open(ConfirmacionComponent, {
                width: '35%',
                disableClose: true,
                data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.router.navigate(['pages/personal-agentes']);
                }
            });
        } else {
            this.router.navigate(['pages/personal-agentes']);
        }
    }

    nuevo() {
        this.personal = new PersonalModel();
        this.router.navigate(['pages/personal-agentes/personal-agente', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiPersonal, funcName, jsonParams).subscribe(
            result => {
                this.idPersonal = +result.idPersonal;
                this.personal.idPersonal = this.idPersonal;

                this.guardado = true;
                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}

export class PersonalModel {
    idPersonal: number;
    idEmpresa: number;
    idArea: number;
    idPersonalTipo: number;
    nombre: string;
    correo: string;
    telefono: string;
    estatus: string;
    fechaIngreso: Date;
    usuario: string;
    password: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
    }
}