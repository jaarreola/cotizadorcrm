import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import "leaflet-map";
import "style-loader!leaflet/dist/leaflet.css";
import 'leaflet-routing-machine';
import { MatDialog, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';
import { fcGeneral } from '../../../../services/general.service';
declare var L: any;

@Component({
  selector: 'app-agregar-editar-plaza',
  templateUrl: './agregar-editar-plaza.component.html',
  styleUrls: ['./agregar-editar-plaza.component.scss']
})
export class AgregarEditarPlazaComponent implements OnInit {
    private urlApiPlazas = 'api/Plaza';
    private urlApiCommon = 'api/Common';
    private urlApiTarifas = 'api/CombustibleTarifas';

    config = new MatSnackBarConfig();

    public paises: any[] = [];
    public estados: any[] = [];
    public tarifas: any[] = [];

    public idPlaza: any;
    public plaza: PlazaModel = new PlazaModel();
    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    latitude: number = 23.675;
    longitude: number = -101.030;
    map: any;
    layerGroup: any;

    isSistemaOrigen = false;
    formPlazas: FormGroup;
    constructor(private router: Router, public formBuilder: FormBuilder, public dialog: MatDialog, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.buildForm();
        this.consultarPaises();
        this.consultarTarifas();

        let el = document.getElementById("leaflet-map");

        this.route.params.subscribe(params => {
            this.idPlaza = params['id'];

            if (this.idPlaza != "N") {
                this.idPlaza = +params['id'];

                this.consultarPlaza();
            } else {
                this.idPlaza = 0;
            }
        });

        L.Icon.Default.imagePath = 'assets/img/vendor/leaflet';
        this.map = L.map(el).setView([this.latitude, this.longitude], 5);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);

        this.layerGroup = L.layerGroup().addTo(this.map);
    }

    buildForm() {
        this.formPlazas = this.formBuilder.group({
            formNoPlaza: this.formBuilder.control(null, []),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formDireccion: this.formBuilder.control(null, [Validators.required]),
            formLatitud: this.formBuilder.control(null, [Validators.pattern('^-?\\d+(\\.\\d+)?$')]),
            formLongitud: this.formBuilder.control(null, [Validators.pattern('^-?\\d+(\\.\\d+)?$')]),
            formPais: this.formBuilder.control(null, [Validators.required]),
            formEstado: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required]),
            formTarifa: this.formBuilder.control(null, [Validators.required]),
        });

        const controlNoPlaza = this.formPlazas.get("formNoPlaza");
        controlNoPlaza.disable();

        const controlTarifa = this.formPlazas.get("formTarifa");

        if (this.isSistemaOrigen) {
            this.formPlazas.disable();
            controlTarifa.enable();
        }
    }

    consultarPaises() {
        this._service.getAPI(this.urlApiCommon, 'getPaisesListado').subscribe(
            result => {
                this.paises = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarEstados() {
        let param = {
            idPais: this.plaza.idPais
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiCommon, 'getEstadosLista', json).subscribe(
            result => {
                this.estados = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTarifas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTarifas, 'getCombustibleTarifasAutocomplete', json).subscribe(
            result => {
                this.tarifas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPlaza() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idPlaza: this.idPlaza
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPlazas, 'getPlazaById', json).subscribe(
            result => {
                this.plaza = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.setMapView();
            }
        );
    }

    setMapView() {
        if (this.plaza.posLat != null && this.plaza.posLon != null) {
            this.layerGroup.clearLayers();

            this.map.setView(new L.LatLng(this.plaza.posLat, this.plaza.posLon), 17);
            let marker = L.marker([this.plaza.posLat, this.plaza.posLon], { title: "Plaza" }).addTo(this.layerGroup)
        }
    }

    guardar() {
        if (this.formPlazas.valid) {
            if (this.idPlaza == 0) {
                this.plaza.idPlaza = 0;
            }
            this.plaza.posLat = +this.plaza.posLat;
            this.plaza.posLon = +this.plaza.posLon;
            let json = JSON.stringify(this.plaza);

            this.metodoPost(json, 'setPlaza');
        } else {
            Object.keys(this.formPlazas.controls).forEach(key => {
                const ctrl = this.formPlazas.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    regresar() {
        const dialogRef = this.dialog.open(ConfirmacionComponent, {
            width: '35%',
            disableClose: true,
            data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.router.navigate(['pages/plazas']);
            }
        });
    }

    nuevo() {
        this.plaza = new PlazaModel();
        this.router.navigate(['pages/plazas/plaza', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiPlazas, funcName, jsonParams).subscribe(
            result => {
                this.idPlaza = +result.idPlaza;
                this.plaza.idPlaza = this.idPlaza;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}

export class PlazaModel {
    idPlaza: number;
    idEmpresa: number;
    idArea: number;
    idPais: number;
    idEstado: number;
    nombre: string;
    estatus: string;
    direccion: string;
    posLat: number;
    posLon: number;
    idCombustibleTarifa: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idArea = 1;
    }
}
