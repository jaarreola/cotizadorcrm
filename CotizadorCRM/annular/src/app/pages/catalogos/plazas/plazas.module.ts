﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { PlazasComponent } from './plazas.component';
import { AgregarEditarPlazaComponent } from './agregar-editar-plaza/agregar-editar-plaza.component';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';


export const routes = [
    { path: '', component: PlazasComponent, pathMatch: 'full' },
    { path: 'plaza/:id', component: AgregarEditarPlazaComponent, data: {breadcrumb: 'Plaza'}}
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule
    ],
    declarations: [
        PlazasComponent,
        AgregarEditarPlazaComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class PlazasModule { }