import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-plazas',
  templateUrl: './plazas.component.html',
  styleUrls: ['./plazas.component.scss']
})
export class PlazasComponent implements OnInit {
    private urlApiPlazas = 'api/Plaza';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'noPlaza',
        'nombre',
        'estado',
        'estatus'
    ];

    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        nombre: "",
        idEstado: 0,
        estatus: ""
    }

    public plazas: any[] = [];
    public paises: any[] = [];
    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    isSistemaOrigen = false;
    filtros = true;
    constructor(private router: Router, private _service: fcGeneral) {
        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarPaises();
        this.consultarPlazas();
    }

    consultarPaises() {
        this._service.getAPI(this.urlApiCommon, 'getPaisesListado').subscribe(
            result => {
                this.paises = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPlazas() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiPlazas, 'getPlazaLista', json).subscribe(
            result => {
                this.plazas = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.plazas);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            nombre: "",
            idEstado: 0,
            estatus: ""
        }
    }

    nuevo() {
        this.router.navigate(['pages/plazas/plaza', 'N']);
    }

    editar(plaza) {
        this.router.navigate(['pages/plazas/plaza', plaza.idPlaza]);
    }

}
