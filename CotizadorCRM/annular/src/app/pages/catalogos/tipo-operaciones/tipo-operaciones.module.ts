﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { TipoOperacionesComponent } from './tipo-operaciones.component';
import { AgregarEditarTipoOperacionComponent } from './agregar-editar-tipo-operacion/agregar-editar-tipo-operacion.component';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

export const routes = [
    { path: '', component: TipoOperacionesComponent, pathMatch: 'full' },
    { path: 'tipo-operacion/:id', component: AgregarEditarTipoOperacionComponent, data: { breadcrumb: 'Tipo Operación' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        TipoOperacionesComponent,
        AgregarEditarTipoOperacionComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class TipoOperacionesModule { }