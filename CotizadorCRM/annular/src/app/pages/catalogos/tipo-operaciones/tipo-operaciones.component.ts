import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-tipo-operaciones',
  templateUrl: './tipo-operaciones.component.html',
  styleUrls: ['./tipo-operaciones.component.scss']
})
export class TipoOperacionesComponent implements OnInit {
    private urlApiTipoOperacion = 'api/TiposOperacion';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'idTipoOperacion',
        'tipoOperacion',
        'descripcion',
        'estatus'
    ];

    clasificaciones: any[] = [];
    tiposOperacion: any[] = [];

    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        nombre: "",
        idClasificacionTipoOperacion: 0,
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    filtros = true;
    isSistemaOrigen = false;
    constructor(private router: Router, private _service: fcGeneral) {
        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarClasificaciones();
        this.consultarTiposOperacion();
    }

    nuevo() {
        this.router.navigate(['pages/tipo-operaciones/tipo-operacion', 'N']);
    }

    editar(tipoOperacion) {
        this.router.navigate(['pages/tipo-operaciones/tipo-operacion', tipoOperacion.idTipoOperacion]);
    }

    consultarClasificaciones() {
        this._service.getAPI(this.urlApiCommon, 'getClasificacionTipoOperacionListado').subscribe(
            result => {
                this.clasificaciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposOperacion() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiTipoOperacion, 'getTiposOperacionLista', json).subscribe(
            result => {
                this.tiposOperacion = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.tiposOperacion);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            nombre: "",
            idClasificacionTipoOperacion: 0,
            estatus: ""
        }
    }

}
