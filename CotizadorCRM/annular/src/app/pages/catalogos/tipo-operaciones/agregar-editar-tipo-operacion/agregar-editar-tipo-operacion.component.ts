import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { fcGeneral } from '../../../../services/general.service';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-agregar-editar-tipo-operacion',
  templateUrl: './agregar-editar-tipo-operacion.component.html',
  styleUrls: ['./agregar-editar-tipo-operacion.component.scss']
})
export class AgregarEditarTipoOperacionComponent implements OnInit {
    private urlApiTipoOperacion = 'api/TiposOperacion';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    idTipoOperacion;
    tipoOperacion: TipoOperacionModel = new TipoOperacionModel();

    clasificaciones: any[] = [];
    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    isSistemaOrigen = false;
    formTipoOperacion: FormGroup;
    constructor(private router: Router, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.buildForm();
        this.consultarClasificaciones();

        this.route.params.subscribe(params => {
            this.idTipoOperacion = params['id'];

            if (this.idTipoOperacion != "N") {
                this.idTipoOperacion = +params['id'];

                this.consultarTipoOperacion();
            } else {
                this.idTipoOperacion = 0;
            }
        });
    }

    buildForm() {
        this.formTipoOperacion = this.formBuilder.group({
            formIdTipoOperacion: this.formBuilder.control(null, []),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formClasificacion: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required])
        });

        const controlNoProducto = this.formTipoOperacion.get("formIdTipoOperacion");
        controlNoProducto.disable();

        if (this.isSistemaOrigen) {
            this.formTipoOperacion.disable();
        }
    }

    regresar() {
        this.router.navigate(['pages/tipo-operaciones']);
    }

    consultarClasificaciones() {
        this._service.getAPI(this.urlApiCommon, 'getClasificacionTipoOperacionListado').subscribe(
            result => {
                this.clasificaciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTipoOperacion() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idTipoOperacion: this.idTipoOperacion
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTipoOperacion, 'getTipoOperacionById', json).subscribe(
            result => {
                this.tipoOperacion = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        if (this.formTipoOperacion.valid) {
            if (this.idTipoOperacion == 0) {
                this.tipoOperacion.idTipoOperacion = 0;
            }
            let json = JSON.stringify(this.tipoOperacion);

            this.metodoPost(json, 'setTipoOperacion');
        } else {
            Object.keys(this.formTipoOperacion.controls).forEach(key => {
                const ctrl = this.formTipoOperacion.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }

    }

    nuevo() {
        this.tipoOperacion = new TipoOperacionModel();
        this.router.navigate(['pages/tipo-operaciones/tipo-operacion', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiTipoOperacion, funcName, jsonParams).subscribe(
            result => {
                this.idTipoOperacion = +result.idTipoOperacion;
                this.tipoOperacion.idTipoOperacion = this.idTipoOperacion;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}

export class TipoOperacionModel {
    idTipoOperacion: number;
    idEmpresa: number;
    idArea: number;
    idClasificacionTipoOperacion: number;
    nombre: string;
    estatus: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idArea = 1;
    }
}
