﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { ProductosComponent } from './productos.component';
import { AgregarEditarProductoComponent } from './agregar-editar-producto/agregar-editar-producto.component';
import { fcGeneral } from '../../../services/general.service';


export const routes = [
    { path: '', component: ProductosComponent, pathMatch: 'full' },
    { path: 'producto/:id', component: AgregarEditarProductoComponent, data: { breadcrumb: 'Producto' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        ProductosComponent,
        AgregarEditarProductoComponent
        
    ],
    providers: [
        fcGeneral
    ]
})
export class ProductosModule { }