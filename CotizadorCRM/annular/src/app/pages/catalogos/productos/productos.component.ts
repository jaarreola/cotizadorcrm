import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
    private urlApiProductos = 'api/Productos';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'idProducto',
        'producto',
        'descripcion',
        'embalaje',
        'estatus'
    ];

    public productos: any[] = [];
    public embalajes: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        nombre: "",
        idEmbalaje: 0,
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    filtros = true;
    isSistemaOrigen = false;
    constructor(private router: Router, private _service: fcGeneral) {
        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarEmbalajes();
        this.consultarProductos();
    }

    nuevo() {
        this.router.navigate(['pages/productos/producto', 'N']);
    }

    editar(producto) {
        this.router.navigate(['pages/productos/producto', producto.idProducto]);
    }

    consultarEmbalajes() {
        this._service.getAPI(this.urlApiCommon, 'getEmbalajeListado').subscribe(
            result => {
                this.embalajes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    /*
     -Los llamados son a API's
     -Se hace llamado POST sin importar b�squeda o ABC; para el control de los par�metros de filtrado de informaci�n.
     -Los par�metros van en formato JSON y llevan el nombre de los campos de las tablas definidas (En la carpeta de MODELS se encontrar� definici�n de tablas).
     -El response es en estructura JSON.
     -Siempre se env�a Empresa y Area; ser�n cargados en variables de sesi�n al concluir con el login (usar por default Empresa 1 y �rea 1)
     */
    consultarProductos() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiProductos, 'getProductosLista', json).subscribe(
            result => {
                this.productos = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.productos);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            nombre: "",
            idEmbalaje: 0,
            estatus: ""
        }
    }

}
