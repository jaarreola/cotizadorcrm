import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { fcGeneral } from '../../../../services/general.service';
import { finalize } from 'rxjs/operators';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-agregar-editar-producto',
  templateUrl: './agregar-editar-producto.component.html',
  styleUrls: ['./agregar-editar-producto.component.scss']
})
export class AgregarEditarProductoComponent implements OnInit {
    private urlApiProductos = 'api/Productos';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    private idProducto;
    public producto: ProductoModel = new ProductoModel();

    public embalajes = [];

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    isSistemaOrigen = false;
    formProducto: FormGroup;
    constructor(private router: Router, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.buildForm();
        this.consultarEmbalajes();

        this.route.params.subscribe(params => {
            this.idProducto = params['id'];

            if (this.idProducto != "N") {
                this.idProducto = +params['id'];

                this.consultarProducto();
            } else {
                this.idProducto = 0;
            }
        });
    }

    buildForm() {
        this.formProducto = this.formBuilder.group({
            formNoProducto: this.formBuilder.control(null, []),
            formClave: this.formBuilder.control(null, [Validators.required]),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formEmbalaje: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required])
        });

        const controlNoProducto = this.formProducto.get("formNoProducto");
        controlNoProducto.disable();

        if (this.isSistemaOrigen) {
            this.formProducto.disable();
        }
    }

    consultarEmbalajes() {
        this._service.getAPI(this.urlApiCommon, 'getEmbalajeListado').subscribe(
            result => {
                this.embalajes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarProducto() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idProducto: this.idProducto
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiProductos, 'getProductosById', json).subscribe(
            result => {
                this.producto = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        if (this.formProducto.valid) {
            if (this.idProducto == 0) {
                this.producto.idProducto = 0;
            }
            let json = JSON.stringify(this.producto);

            this.metodoPost(json, 'setProducto');
        } else {
            Object.keys(this.formProducto.controls).forEach(key => {
                const ctrl = this.formProducto.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }
       
    }

    regresar() {
        this.router.navigate(['pages/productos']);
    }

    nuevo() {
        this.producto = new ProductoModel();
        this.router.navigate(['pages/productos/producto', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiProductos, funcName, jsonParams).subscribe(
            result => {
                this.idProducto = +result.idProducto;
                this.producto.idProducto = this.idProducto;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }
}

export class ProductoModel {
    public idProducto: number;
    public idEmpresa: number;
    public idArea: number;
    public idEmbalaje: number;
    public nombre: string;
    public clave: string;
    public estatus: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idArea = 1;
    }
}