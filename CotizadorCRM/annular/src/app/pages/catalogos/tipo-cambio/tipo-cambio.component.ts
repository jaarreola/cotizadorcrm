import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-tipo-cambio',
  templateUrl: './tipo-cambio.component.html',
  styleUrls: ['./tipo-cambio.component.scss']
})
export class TipoCambioComponent implements OnInit {
    private urlApiTipoCambio = 'api/TipoCambio';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'opciones',
        'fecha',
        'moneda',
        'leyenda',
        'tipoCambio',
        'fuente',
        'modulo'
    ];

    tiposCambio: any[] = [];
    monedas: any[] = [];

    hoy = new Date();
    fechaInicio = new Date(new Date().setDate(this.hoy.getDate() - 7))
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        idMoneda: 0,
        fechaInicio: this.fechaInicio,
        fechaFin: new Date()
    }

    filtros = true;
    isSistemaOrigen = false;
    controlFechaInicio = new FormControl(this.filtro.fechaInicio, [Validators.required]);
    controlFechaFin = new FormControl(this.filtro.fechaFin, [Validators.required]);
    constructor(private router: Router, private _service: fcGeneral) {
        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarMonedas();
        this.consultarTiposCambio();
    }

    consultarMonedas() {
        this._service.getAPI(this.urlApiCommon, 'getMonedasListado').subscribe(
            result => {
                this.monedas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposCambio() {
        if (this.controlFechaInicio.valid && this.controlFechaFin.valid) {
            let json = JSON.stringify(this.filtro);

            this._service.postAPI(this.urlApiTipoCambio, 'getTipoCambioLista', json).subscribe(
                result => {
                    this.tiposCambio = result;
                },
                error => {
                    console.log(error);
                },
                () => {
                    this.dataSource = new MatTableDataSource(this.tiposCambio);
                    this.dataSource.paginator = this.paginator;
                }
            );
        } else {
            this.controlFechaInicio.markAsTouched();
            this.controlFechaFin.markAsTouched();
        }
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idMoneda: 0,
            fechaInicio: this.fechaInicio,
            fechaFin: new Date()
        }
    }

    nuevo() {
        this.router.navigate(['pages/tipos-cambio/tipo-cambio', 'N']);
    }

    editar(tipoCambio) {
        this.router.navigate(['pages/tipos-cambio/tipo-cambio', tipoCambio.idTipoCambio]);
    }

}
