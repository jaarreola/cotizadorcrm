﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { TipoCambioComponent } from './tipo-cambio.component';;
import { AgregarEditarTipoCambioComponent } from './agregar-editar-tipo-cambio/agregar-editar-tipo-cambio.component'
import { fcGeneral } from '../../../services/general.service';


export const routes = [
    { path: '', component: TipoCambioComponent, pathMatch: 'full' },
    { path: 'tipo-cambio/:id', component: AgregarEditarTipoCambioComponent, data: { breadcrumb: 'Tipo Cambio' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule
    ],
    declarations: [
        TipoCambioComponent,
        AgregarEditarTipoCambioComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class TipoCambioModule { }