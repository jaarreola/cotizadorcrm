import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';
import { fcGeneral } from '../../../../services/general.service';

@Component({
  selector: 'app-agregar-editar-tipo-cambio',
  templateUrl: './agregar-editar-tipo-cambio.component.html',
  styleUrls: ['./agregar-editar-tipo-cambio.component.scss']
})
export class AgregarEditarTipoCambioComponent implements OnInit {
    private urlApiTipoCambio = 'api/TipoCambio';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    monedas: any[] = [];
    fuentes: any[] = [];

    idTipoCambio: any;
    tipoCambio: TipoCambioModel = new TipoCambioModel();

    guardado = false;
    isSistemaOrigen = false;
    formTipoCambio: FormGroup;
    constructor(private router: Router, public formBuilder: FormBuilder, public dialog: MatDialog, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.buildForm();
        this.consultarMonedas();
        this.consultarFuentes();

        this.route.params.subscribe(params => {
            this.idTipoCambio = params['id'];

            if (this.idTipoCambio != "N") {
                this.idTipoCambio = +params['id'];

                this.consultarTipoCambio();
            } else {
                this.idTipoCambio = 0;
            }
        });
    }

    buildForm() {
        this.formTipoCambio = this.formBuilder.group({
            formFecha: this.formBuilder.control(null, [Validators.required]),
            formTipo: this.formBuilder.control(null, [Validators.required]),
            formMoneda: this.formBuilder.control(null, [Validators.required]),
            formFuente: this.formBuilder.control(null, [Validators.required])
        });

        if (this.isSistemaOrigen) {
            this.formTipoCambio.disable();
        }
    }

    consultarMonedas() {
        this._service.getAPI(this.urlApiCommon, 'getMonedasListado').subscribe(
            result => {
                this.monedas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarFuentes() {
        this._service.getAPI(this.urlApiCommon, 'getFuentesTipoCambioListado').subscribe(
            result => {
                this.fuentes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTipoCambio() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idTipoCambio: this.idTipoCambio
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTipoCambio, 'getTipoCambioById', json).subscribe(
            result => {
                this.tipoCambio = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    guardar() {
        if (this.formTipoCambio.valid) {
            this.tipoCambio.tipoCambio = +this.tipoCambio.tipoCambio;
            if (this.idTipoCambio == 0) {
                this.tipoCambio.idTipoCambio = 0;
            }
            let json = JSON.stringify(this.tipoCambio);

            this.metodoPost(json, 'setTipoCambio');
        } else {
            Object.keys(this.formTipoCambio.controls).forEach(key => {
                const ctrl = this.formTipoCambio.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }
    }

    regresar() {
        if (!this.guardado) {
            const dialogRef = this.dialog.open(ConfirmacionComponent, {
                width: '35%',
                disableClose: true,
                data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.router.navigate(['pages/tipos-cambio']);
                }
            });
        } else {
            this.router.navigate(['pages/tipos-cambio']);
        }
    }

    nuevo() {
        this.tipoCambio = new TipoCambioModel();
        this.router.navigate(['pages/tipos-cambio/tipo-cambio', 'N']);
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiTipoCambio, funcName, jsonParams).subscribe(
            result => {
                this.idTipoCambio = +result.idTipoCambio;
                this.tipoCambio.idTipoCambio = this.idTipoCambio;
                this.guardado = true;

                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }
}

export class TipoCambioModel {
    idTipoCambio: number;
    idEmpresa: number;
    idArea: number;
    idMoneda: number;
    idTipoFuente: number;
    fecha: Date;
    tipoCambio: number;
    estatus: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.idArea = 1;
        this.estatus = "A";
    }
}
