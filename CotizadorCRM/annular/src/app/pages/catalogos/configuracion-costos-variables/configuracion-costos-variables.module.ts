﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { ConfiguracionCostosVariablesComponent } from './configuracion-costos-variables.component';

export const routes = [
    { path: '', component: ConfiguracionCostosVariablesComponent, pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule
    ],
    declarations: [
        ConfiguracionCostosVariablesComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class ConfiguracionCostosVariablesModule { }