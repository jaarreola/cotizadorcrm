import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { fcGeneral } from '../../../services/general.service';
import { MatSnackBar, MatSnackBarConfig, MatCheckboxChange } from '@angular/material';
import { Settings } from '../../../app.settings.model';
import { AppSettings } from '../../../app.settings';

@Component({
  selector: 'app-configuracion-costos-variables',
  templateUrl: './configuracion-costos-variables.component.html',
  styleUrls: ['./configuracion-costos-variables.component.scss']
})
export class ConfiguracionCostosVariablesComponent implements OnInit {
    private urlApiConfiguracion = 'api/EmpresaConfiguracion';
    private urlApiCommon = 'api/Common';
    public settings: Settings;

    config = new MatSnackBarConfig();

    costos: any[] = [];
    configuracion: any = {};
    camposCombustibleVisibles = false;
    camposCasetasVisibles = false;
    camposSueldosVisibles = false;
    camposOcultos = false;
    //Agrega Listado Dinámico para Sueldos
    listadoSueldos: SueldoModel[] = new Array<SueldoModel>();
    formSueldos: FormArray;
    public tipoUnidad = [
        { id: '1', tipoUnidad: 'Clasificación sencilla' },
        { id: '2', tipoUnidad: 'Clasificación doble articulado' }
    ];


    constructor(private _service: fcGeneral, private _snackBar: MatSnackBar, public appSettings: AppSettings) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
        this.settings.loadingSpinner = true;
        this.consultarTiposCosto();
        this.consultarConfiguracion();
        //Inicializa listado de sueldos dinámico
        this.buildFormSueldos();
    }

    consultarTiposCosto() {
        this._service.getAPI(this.urlApiCommon, 'getCostosTiposListado').subscribe(
            result => {
                this.costos = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarConfiguracion() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            filtro: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiConfiguracion, 'getEmpresaConfiguracion', json).subscribe(
            result => {
                this.configuracion = result;
                //Combustible
                if (this.configuracion.combustible_chk_manual) {
                    this.camposCombustibleVisibles = true;
                } else {
                    this.camposCombustibleVisibles = false;
                }
                //Casetas
                if (this.configuracion.caseta_chk_manual) {
                    this.camposCasetasVisibles = true;
                } else {
                    this.camposCasetasVisibles = false;
                }
                //Sueldos
                if (this.configuracion.sueldo_chk_manual) {
                    this.camposSueldosVisibles = true;
                } else {
                    this.camposSueldosVisibles = false;
                }
                //Carga Sueldos
                for (let sueldo of this.configuracion.sueldos) {
                    let rowSueldo: SueldoModel = new SueldoModel();
                    rowSueldo.tipoUnidad = '' + sueldo.tipoUnidad;
                    rowSueldo.rangoInicio = '' + sueldo.rangoInicio;
                    rowSueldo.rangoFin = '' + sueldo.rangoFin;
                    rowSueldo.factor = '' + sueldo.factor;
                    this.listadoSueldos.push(rowSueldo);
                }
                //Llena form dinámico
                let groups = this.listadoSueldos.map(sueldo => {
                    return new FormGroup({
                        tipoUnidad: new FormControl(sueldo.tipoUnidad, [Validators.required]),
                        rangoInicio: new FormControl(sueldo.rangoInicio, [Validators.required]),
                        rangoFin: new FormControl(sueldo.rangoFin, [Validators.required]),
                        factor: new FormControl(sueldo.factor, [Validators.required]),
                    });
                });
                this.formSueldos = new FormArray(groups);
                //Detiene loading
                this.settings.loadingSpinner = false;
            },
            error => {
                this.settings.loadingSpinner = false;
                console.log(error);

            }
        );
    }

    guardar() {
        this.configuracion.combustible_chk_automatico = +this.configuracion.combustible_chk_automatico;
        this.configuracion.combustible_chk_manual = +this.configuracion.combustible_chk_manual;
        this.configuracion.combustible_chk_historico = +this.configuracion.combustible_chk_historico;
        this.configuracion.combustible_chk_tarifa = +this.configuracion.combustible_chk_tarifa;
        this.configuracion.combustible_factor = +this.configuracion.combustible_factor;

        this.configuracion.caseta_chk_automatico = +this.configuracion.caseta_chk_automatico;
        this.configuracion.caseta_chk_manual = +this.configuracion.caseta_chk_manual;
        this.configuracion.caseta_chk_historico = +this.configuracion.caseta_chk_historico;
        this.configuracion.caseta_chk_archivo = +this.configuracion.caseta_chk_archivo;
        this.configuracion.caseta_factor = +this.configuracion.caseta_factor;

        this.configuracion.sueldo_chk_historico = +this.configuracion.sueldo_chk_historico;
        this.configuracion.sueldo_chk_manual = +this.configuracion.sueldo_chk_manual;
        this.configuracion.sueldo_factor = +this.configuracion.sueldo_factor;
        this.configuracion.sueldos = this.listadoSueldos;
        let json = JSON.stringify(this.configuracion);
        this._service.postAPI(this.urlApiConfiguracion, 'setEmpresaConfigVariables', json).subscribe(
            result => {
                this._snackBar.open('Configuración guardada con exito', '', this.config);
                this.configuracion = result;
                console.log(this.configuracion);
            },
            error => {
                console.log(error);
            }
        );
    }

    validateOptions(event: MatCheckboxChange, nameController: string): void {
        console.log(event.checked + '. ' + nameController);
        switch (nameController) {
            //*****COMBUSTIBLE*******
            case 'combustible_chk_manual': {
                //Reset de Checks
                //console.log(event.checked + '. ' + event.source.id);
                if (event.checked) {
                    this.configuracion.combustible_chk_automatico = +0;
                    this.configuracion.combustible_chk_tarifa = +0;
                    this.configuracion.combustible_chk_historico = +0;
                    this.configuracion.combustible_factor = '0';
                    this.camposCombustibleVisibles = true;
                } else {
                    this.camposCombustibleVisibles = false;
                }
                break;
            }
            case 'combustible_chk_automatico':
            case 'combustible_chk_historico':
            case 'combustible_chk_tarifa':{
                //Reset de Checks
                if (event.checked) {
                    this.configuracion.combustible_chk_manual = +0;
                    this.configuracion.combustible_factor = '';
                    this.camposCombustibleVisibles = false;
                }
                break;
            }
            //*****CASETAS*******
            case 'caseta_chk_manual': {
                //Reset de Checks
                //console.log(event.checked + '. ' + event.source.id);
                if (event.checked) {
                    this.configuracion.caseta_chk_historico = +0;
                    this.configuracion.caseta_chk_archivo = +0;
                    this.configuracion.caseta_chk_automatico = +0;
                    this.configuracion.caseta_factor = '0';
                    this.camposCasetasVisibles = true;
                } else {
                    this.camposCasetasVisibles = false;
                }
                break;
            }
            case 'caseta_chk_historico':
            case 'caseta_chk_archivo':
            case 'caseta_chk_automatico': {
                //Reset de Checks
                if (event.checked) {
                    this.configuracion.caseta_chk_manual = +0;
                    this.configuracion.caseta_factor = '';
                    this.camposCasetasVisibles = false;
                }
                break;
            }
            //*****SUELDOS*******
            case 'sueldo_chk_manual': {
                //Reset de Checks
                //console.log(event.checked + '. ' + event.source.id);
                if (event.checked) {
                    this.configuracion.sueldo_chk_historico = +0;
                    this.configuracion.sueldo_factor = '0';
                    this.camposSueldosVisibles = true;
                } else {
                    this.camposSueldosVisibles = false;
                }
                break;
            }
            case 'sueldo_chk_historico': {
                //Reset de Checks
                if (event.checked) {
                    this.configuracion.sueldo_chk_manual = +0;
                    this.configuracion.sueldo_factor = '';
                    this.camposSueldosVisibles = false;
                }
                break;
            }
            default: {
                //statements; 
                break;
            }
        } 
    }

    /*Funciones para Listado de Sueldos Dinpamicos*/
    agregarSueldo() {
        this.listadoSueldos.push(new SueldoModel());

        let group = new FormGroup({
            tipoUnidad: new FormControl(null, [Validators.required]),
            rangoInicio: new FormControl(null, [Validators.required]),
            rangoFin: new FormControl(null, [Validators.required]),
            factor: new FormControl(null, [Validators.required])
        });
        this.formSueldos.push(group);
    }

    buildFormSueldos() {
        let groups = this.listadoSueldos.map(sueldo => {
            return new FormGroup({
                tipoUnidad: new FormControl(sueldo.tipoUnidad, [Validators.required]),
                rangoInicio: new FormControl(sueldo.rangoInicio, [Validators.required]),
                rangoFin: new FormControl(sueldo.rangoFin, [Validators.required]),
                factor: new FormControl(sueldo.factor, [Validators.required]),
            });
        });
        this.formSueldos = new FormArray(groups);
    }

    getControl(index, campo) {
        return this.formSueldos.at(index).get(campo) as FormControl;
    }

    eliminaSueldo(index) {
        this.listadoSueldos.splice(index, 1);
    }
}

export class SueldoModel {
    idEmpresa: number;
    tipoUnidad: string;
    rangoInicio: string;
    rangoFin: string;
    factor: string;

    constructor() {
        this.rangoFin = "0";
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
    }
}