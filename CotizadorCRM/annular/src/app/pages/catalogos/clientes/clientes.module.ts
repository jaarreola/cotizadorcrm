﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClientesComponent } from './clientes.component';
import { AgregarEditarClienteComponent } from './agregar-editar-cliente/agregar-editar-cliente.component';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

export const routes = [
    { path: '', component: ClientesComponent, pathMatch: 'full' },
    { path: 'cliente/:id', component: AgregarEditarClienteComponent, data: { breadcrumb: 'Cliente' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule
    ],
    declarations: [
        ClientesComponent,
        AgregarEditarClienteComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class ClientesModule { }