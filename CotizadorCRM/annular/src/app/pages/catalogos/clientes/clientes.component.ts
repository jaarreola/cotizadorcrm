import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
    private urlApiClientes = 'api/Cliente';
    private urlApiPlazas = 'api/Plaza';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'noCliente',
        'cliente',
        'rfc',
        'plaza',
        'tipoCliente',
        'estatus',
        'contacto',
    ];

    clientes: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        nombre: "",
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    filtros = true;
    isSistemaOrigen = false;
    constructor(private router: Router, private webService: fcGeneral, private _service: fcGeneral) {
        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarClientes();
    }

    nuevo() {
        this.router.navigate(['pages/clientes/cliente', 'N']);
    }

    editar(cliente) {
        this.router.navigate(['pages/clientes/cliente', cliente.idCliente]);
    }

    consultarClientes() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiClientes, 'getClientesLista', json).subscribe(
            result => {
                this.clientes = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.clientes);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            nombre: "",
            estatus: ""
        }
    }

}
