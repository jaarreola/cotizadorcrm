import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import "leaflet-map";
import "style-loader!leaflet/dist/leaflet.css";
import 'leaflet-routing-machine';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';
import { MatDialog, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { fcGeneral } from '../../../../services/general.service';
import { finalize } from 'rxjs/operators';
declare var L: any;

@Component({
  selector: 'app-agregar-editar-cliente',
  templateUrl: './agregar-editar-cliente.component.html',
  styleUrls: ['./agregar-editar-cliente.component.scss']
})
export class AgregarEditarClienteComponent implements OnInit {
    private urlApiClientes = 'api/Cliente';
    private urlApiPlaza = 'api/Plaza';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    latitude: number = 23.675;
    longitude: number = -80.362;

    map: any;
    layerGroup: any;

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    public plazas: any[] = [];
    public tiposCliente: any[] = [];

    idCliente: any;
    cliente: ClienteModel = new ClienteModel();

    guardado = false;
    isSistemaOrigen = false;
    formClientes: FormGroup;
    formContactos: FormArray;
    constructor(private router: Router, public formBuilder: FormBuilder, public dialog: MatDialog, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarPlazas();
        this.consultarTiposCliente();
        this.buildForm();

        let el = document.getElementById("leaflet-map");

        L.Icon.Default.imagePath = 'assets/img/vendor/leaflet';
        this.map = L.map(el).setView([this.latitude, this.longitude], 5);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);

        this.layerGroup = L.layerGroup().addTo(this.map);

        this.route.params.subscribe(params => {
            this.idCliente = params['id'];

            if (this.idCliente != "N") {
                this.idCliente = +params['id'];

                this.consultarCliente();
            } else {
                this.idCliente = 0;
                this.cliente.contactos.push(new ContactoModel());
                this.buildFormContactos();
            }
        });
        
    }

    setMapView() {
        if (this.cliente.posLat != null && this.cliente.posLon != null) {
            this.layerGroup.clearLayers();

            this.map.setView(new L.LatLng(this.cliente.posLat, this.cliente.posLon), 15);
            let marker = L.marker([this.cliente.posLat, this.cliente.posLon], { title: "Cliente" }).addTo(this.layerGroup)
        }
    }

    buildForm() {
        this.formClientes = this.formBuilder.group({
            formNoCliente: this.formBuilder.control(null, [Validators.required, Validators.pattern('^\\d+$')]),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formDomicilio: this.formBuilder.control(null, [Validators.required]),
            formTelefono: this.formBuilder.control(null, [Validators.pattern('^\\d{10}$')]),
            formMovil: this.formBuilder.control(null, [Validators.pattern('^\\d{10}$')]),
            formContacto: this.formBuilder.control(null, [Validators.required]),
            formObservaciones: this.formBuilder.control(null, [Validators.maxLength(250)]),
            formPlaza: this.formBuilder.control(null, []),
            formRfc: this.formBuilder.control(null, [Validators.required, Validators.pattern('^([aA-zZñÑ&]{3,4})(\\d{6})([aA-zZ\\d]{3})$')]),
            formTipo: this.formBuilder.control(null, []),
            formEstado: this.formBuilder.control(null, []),
            formAlias: this.formBuilder.control(null, []),
            formEstatus: this.formBuilder.control(null, []),
            formCorreo: this.formBuilder.control(null, [Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]),
            formLat: this.formBuilder.control(null, []),
            formLon: this.formBuilder.control(null, []),
        });

        const controlNoCliente = this.formClientes.get("formNoCliente");
        controlNoCliente.disable();

        if (this.isSistemaOrigen) {
            this.formClientes.disable();
        }
    }

    buildFormContactos() {
        let groups = this.cliente.contactos.map(contacto => {
            return new FormGroup({
                departamento: new FormControl(contacto.departamento, [Validators.required]),
                nombre: new FormControl(contacto.nombre, [Validators.required]),
                puesto: new FormControl(contacto.puesto, [Validators.required]),
                correo: new FormControl(contacto.correo, [Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]),
                telefono: new FormControl(contacto.telefono, [Validators.pattern('^\\d{10}$')]),
                movil: new FormControl(contacto.movil, [Validators.pattern('^\\d{10}$')]),
                nota: new FormControl(contacto.nota, [Validators.maxLength(25)]),
                estatus: new FormControl(contacto.estatus, [Validators.required]),
            });
        });

        this.formContactos = new FormArray(groups);
    }
    getControl(index, campo) {
        return this.formContactos.at(index).get(campo) as FormControl;
    }

    consultarPlazas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPlaza, 'getPlazaListaByEmpresaArea', json).subscribe(
            result => {
                this.plazas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposCliente() {
        this._service.getAPI(this.urlApiCommon, 'getClienteTiposLista').subscribe(
            result => {
                this.tiposCliente = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarCliente() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idCliente: this.idCliente
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiClientes, 'getClienteById', json).subscribe(
            result => {
                this.cliente = result;
                this.buildFormContactos();
            },
            error => {
                console.log(error);
            },
            () => {
                this.setMapView();
            }
        );
    }

    agregarContacto() {
        this.cliente.contactos.push(new ContactoModel());

        let group = new FormGroup({
            departamento: new FormControl(null, [Validators.required]),
            nombre: new FormControl(null, [Validators.required]),
            puesto: new FormControl(null, [Validators.required]),
            correo: new FormControl(null, [Validators.pattern('^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$')]),
            telefono: new FormControl(null, [Validators.pattern('^\\d{10}$')]),
            movil: new FormControl(null, [Validators.pattern('^\\d{10}$')]),
            nota: new FormControl(null, [Validators.maxLength(25)]),
            estatus: new FormControl(null, [Validators.required]),
        });
        this.formContactos.push(group);
    }

    regresar() {
        if (!this.guardado) {
            const dialogRef = this.dialog.open(ConfirmacionComponent, {
                width: '35%',
                disableClose: true,
                data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this.router.navigate(['pages/clientes']);
                }
            });
        } else {
            this.router.navigate(['pages/clientes']);
        }
    }

    guardar() {
        if (this.formClientes.valid && this.formContactos.valid) {
            if (this.idCliente == 0) {
                this.cliente.idCliente = 0;
            }
            this.cliente.posLat = +this.cliente.posLat;
            this.cliente.posLon = +this.cliente.posLon;
            let json = JSON.stringify(this.cliente);

            this.metodoPost(json, 'setCliente');
        } else {
            Object.keys(this.formClientes.controls).forEach(key => {
                const ctrl = this.formClientes.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            for (let control of this.formContactos.controls as any) {
                Object.keys(control.controls).forEach(key => {
                    const ctrl = control.get(key);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
        }

    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiClientes, funcName, jsonParams).subscribe(
            result => {
                this.idCliente = +result.idCliente;
                this.cliente.idCliente = this.idCliente;

                this.guardado = true;
                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

    nuevo() {
        this.cliente = new ClienteModel();
        this.router.navigate(['pages/clientes/cliente', 'N']);
    }
}

export class ClienteModel {
    idCliente: number;
    idEmpresa: number;
    idPlaza: number;
    idClienteTipo: number;
    nombre: string;
    rfc: string;
    aliasCliente: string;
    direccion: string;
    contacto: string;
    telefono: string;
    movil: string;
    correo: string;
    observaciones: string;
    estatus: string;
    posLat: number;
    posLon: number;
    contactos: ContactoModel[];

    constructor() {
        this.contactos = new Array<ContactoModel>();
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
    }
}

export class ContactoModel {
    idEmpresa: number;
    departamento: string;
    puesto: string;
    nombre: string;
    correo: string;
    telefono: string;
    movil: string;
    nota: string;
    estatus: string;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.departamento = '';
        this.puesto = '';
        this.nombre = '';
        this.correo = '';
        this.telefono = '';
        this.movil = '';
        this.nota = '';
        this.estatus = '';
    }
}
