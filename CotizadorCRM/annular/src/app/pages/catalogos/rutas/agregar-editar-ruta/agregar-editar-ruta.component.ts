import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import "leaflet-map";
import "style-loader!leaflet/dist/leaflet.css";
import 'leaflet-routing-machine';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { fcGeneral } from '../../../../services/general.service';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
declare var L: any;

@Component({
  selector: 'app-agregar-editar-ruta',
  templateUrl: './agregar-editar-ruta.component.html',
  styleUrls: ['./agregar-editar-ruta.component.scss']
})
export class AgregarEditarRutaComponent implements OnInit {
    private urlApiRutas = 'api/Rutas';
    private urlApiPlaza = 'api/Plaza';
    private urlApiCommon = 'api/Common';

    config = new MatSnackBarConfig();

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];

    tiposRuta: any[] = [];
    plazas: any[] = [];
    tiposVehiculo: any[] = [];
    ejes: any[] = [];
    condiciones: any[] = [];

    tiposParada: any[] = [];
    tiposMovimiento: any[] = [];
    segmentos: any[] = [
        {
            direccion: '',
            tipoParada: null,
            movimiento: null
        }
    ];

    idRuta: any;
    ruta: RutaModel = new RutaModel();
    origenLat: any;
    origenLon: any;
    destinoLat: any;
    destinoLon: any;

    latitude: number = 25.6801;
    longitude: number = -100.2988;

    lat1 = 25.67613;
    lon1 = -100.32071;
    lat2 = 25.68301;
    lon2 = -100.27947;

    map: any;
    routingControl = null;

    guardado = false;
    isSistemaOrigen = false;
    formRutas: FormGroup;
    formSegmentos: FormArray;
    constructor(private router: Router, private _service: fcGeneral, private route: ActivatedRoute, private _snackBar: MatSnackBar, public formBuilder: FormBuilder) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarTiposRuta();
        this.consultarPlazas();
        this.consultarTiposVehiculo();
        this.consultarCondicionesOrigen();
        this.consultarEjes();
        this.consultarTiposParada();
        this.consultarTiposMovimiento();

        this.buildForm();

        let el = document.getElementById("leaflet-map");

        L.Icon.Default.imagePath = 'assets/img/vendor/leaflet';
        this.map = L.map(el).setView([this.latitude, this.longitude], 15);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);

        /*L.Routing.control({
            waypoints: [
                L.latLng(this.lat1, this.lon1),
                L.latLng(this.lat2, this.lon2)
            ],
            show: false
        }).addTo(map);*/

        this.route.params.subscribe(params => {
            this.idRuta = params['id'];

            if (this.idRuta != "N") {
                this.idRuta = +params['id'];

                this.consultarRuta();
            } else {
                this.idRuta = 0;
                this.buildFormSegmentos();
            }
        });
    }

    setLatLon() {
        let plazaO = this.plazas.find(x => x.id == this.ruta.idPlazaOrigen);
        this.origenLat = plazaO.Lat;
        this.origenLon = plazaO.Pos;

        let plazaD = this.plazas.find(x => x.id == this.ruta.idPlazaDestino);
        this.destinoLat = plazaD.Lat;
        this.destinoLon = plazaD.Pos;
    }

    setMapView() {
        if (this.ruta.idPlazaOrigen && this.ruta.idPlazaDestino) {
            let valid = true;
            this.setLatLon();

            let points = [L.latLng(this.origenLat, this.origenLon)]; //se agrega el LatLon del Origen
            for (let s of this.ruta.segmentos) { //Se agrega el LatLon de cada segmento
                if (s.posLat && s.posLon) {
                    points.push(L.latLng(s.posLat, s.posLon));
                } else {
                    valid = false;
                }
            }
            points.push(L.latLng(this.destinoLat, this.destinoLon)); //se agrega el LatLong del Destino

            if (valid) {
                if (this.routingControl != null) {
                    this.map.removeControl(this.routingControl);
                }
                this.routingControl = L.Routing.control({
                    waypoints: points,
                    show: false
                }).addTo(this.map);
            }
        }
    }

    buildForm() {
        this.formRutas = this.formBuilder.group({
            formNoRuta: this.formBuilder.control(null, [Validators.required, Validators.pattern('^\\d+$')]),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formOrigen: this.formBuilder.control(null, [Validators.required]),
            formDestino: this.formBuilder.control(null, [Validators.required]),
            formTipoRuta: this.formBuilder.control(null, [Validators.required]),
            formKms: this.formBuilder.control(null, [Validators.required]),
            formTipoVehiculo: this.formBuilder.control(null, [Validators.required]),
            formEjes: this.formBuilder.control(null, [Validators.required]),
            formCondiciones: this.formBuilder.control(null, [Validators.required]),
            formEstatus: this.formBuilder.control(null, [Validators.required]),
        });

        const controlNoRuta = this.formRutas.get("formNoRuta");
        controlNoRuta.disable();

        if (this.isSistemaOrigen) {
            this.formRutas.disable();
        }
    }

    buildFormSegmentos() {
        let groups = this.ruta.segmentos.map(segmento => {
            return new FormGroup({
                direccion: new FormControl(segmento.direccionSegmento, [Validators.required]),
                tipoParada: new FormControl(segmento.idRutaTipoParada, [Validators.required]),
                movimiento: new FormControl(segmento.idRutaMovimiento, [Validators.required]),
                latitud: new FormControl(segmento.posLat, [Validators.required]),
                longitud: new FormControl(segmento.posLon, [Validators.required])
            });
        });

        this.formSegmentos = new FormArray(groups);

        if (this.isSistemaOrigen) {
            this.formSegmentos.disable();
        }
    }
    getControl(index, campo) {
        return this.formSegmentos.at(index).get(campo) as FormControl;
    }

    consultarTiposRuta() {
        this._service.getAPI(this.urlApiCommon, 'getRutaTiposLista').subscribe(
            result => {
                this.tiposRuta = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPlazas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPlaza, 'getPlazaListaByEmpresaArea', json).subscribe(
            result => {
                this.plazas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposVehiculo() {
        this._service.getAPI(this.urlApiCommon, 'getRutaTiposVehiculoLista').subscribe(
            result => {
                this.tiposVehiculo = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarEjes() {
        this._service.getAPI(this.urlApiCommon, 'getVehiculoEjesListado').subscribe(
            result => {
                this.ejes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarCondicionesOrigen() {
        this._service.getAPI(this.urlApiCommon, 'getRutaCondicionesOrigenLista').subscribe(
            result => {
                this.condiciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposParada() {
        this._service.getAPI(this.urlApiCommon, 'getRutaTiposParadaLista').subscribe(
            result => {
                this.tiposParada = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposMovimiento() {
        this._service.getAPI(this.urlApiCommon, 'getRutaMovimientosLista').subscribe(
            result => {
                this.tiposMovimiento = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarRuta() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idRuta: this.idRuta
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiRutas, 'getRutaById', json).subscribe(
            result => {
                this.ruta = result;
                this.buildFormSegmentos();
            },
            error => {
                console.log(error);
            },
            () => {
                this.setMapView();
            }
        );
    }

    agregarSegmento() {
        this.ruta.segmentos.push(new SegmentoModel());

        let group = new FormGroup({
            direccion: new FormControl(null, [Validators.required]),
            tipoParada: new FormControl(null, [Validators.required]),
            movimiento: new FormControl(null, [Validators.required]),
            latitud: new FormControl(null, [Validators.required]),
            longitud: new FormControl(null, [Validators.required])
        });
        this.formSegmentos.push(group);
    }


    regresar() {
        this.router.navigate(['pages/rutas']);
    }

    nuevo() {
        this.ruta = new RutaModel();
        this.router.navigate(['pages/rutas/ruta', 'N']);
    }

    guardar() {
        if (this.formRutas.valid && this.formRutas.valid) {
            if (this.idRuta == 0) {
                this.ruta.idRuta = 0;
            }
            for (let s of this.ruta.segmentos) {
                s.idRuta = 0;
                s.posLat = +s.posLat;
                s.posLon = +s.posLon;
            }
            this.ruta.kms = +this.ruta.kms;
            let json = JSON.stringify(this.ruta);

            this.metodoPost(json, 'setRuta');
        } else {
            Object.keys(this.formRutas.controls).forEach(key => {
                const ctrl = this.formRutas.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            for (let control of this.formSegmentos.controls as any) {
                Object.keys(control.controls).forEach(key => {
                    const ctrl = control.get(key);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
        }

    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiRutas, funcName, jsonParams).subscribe(
            result => {
                this.idRuta = +result.idRuta;
                this.ruta.idRuta = this.idRuta;

                this.guardado = true;
                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}

export class RutaModel {
    idEmpresa: number;
    idRuta: number;
    nombre: string;
    idPlazaOrigen: string;
    idPlazaDestino: string;
    idRutaTipo: number;
    kms: number;
    idRutaTipoVehiculo: number;
    idVehiculoEje: number;
    idRutaCondicionOrigen: number;
    estatus: string;
    segmentos: SegmentoModel[];

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.segmentos = new Array<SegmentoModel>();
    }
}

export class SegmentoModel {
    idEmpresa: number;
    idRuta: number;
    direccionSegmento: string;
    idRutaTipoParada: number;
    idRutaMovimiento: number;
    posLat: number;
    posLon: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
    }
}
