﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { RutasComponent } from './rutas.component';
import { AgregarEditarRutaComponent } from './agregar-editar-ruta/agregar-editar-ruta.component';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';

export const routes = [
    { path: '', component: RutasComponent, pathMatch: 'full' },
    { path: 'ruta/:id', component: AgregarEditarRutaComponent, data: {breadcrumb: 'Ruta'} }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    declarations: [
        RutasComponent,
        AgregarEditarRutaComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class RutasModule { }