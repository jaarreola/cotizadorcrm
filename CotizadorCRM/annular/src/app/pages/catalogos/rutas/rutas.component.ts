import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';

@Component({
  selector: 'app-rutas',
  templateUrl: './rutas.component.html',
  styleUrls: ['./rutas.component.scss']
})
export class RutasComponent implements OnInit {
    private urlApiRutas = 'api/Rutas';
    private urlApiPlaza = 'api/Plaza';
    private urlApiCommon = 'api/Common';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'noRuta',
        'origen',
        'destino',
        'ruta',
        'estatus'
    ];

    plazas: any[] = [];
    rutas: any[] = [];
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idArea: 1,
        idPlazaOrigen: 0,
        idPlazaDestino: 0,
        nombre: "",
        estatus: ""
    }

    public estatus = [
        { id: 'A', estatus: 'Alta' },
        { id: 'B', estatus: 'Baja' }
    ];
    isSistemaOrigen = false;
    filtros = true;
    constructor(private router: Router, private _service: fcGeneral) {
        let sistemaOrigen = sessionStorage.getItem("sistemaOrigen");
        if (sistemaOrigen == "ZAM" || sistemaOrigen == "LISTMS") {
            this.isSistemaOrigen = true;
        }
    }

    ngOnInit() {
        this.consultarPlazas();
        this.consultarRutas();
    }

    consultarPlazas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPlaza, 'getPlazaListaByEmpresaArea', json).subscribe(
            result => {
                this.plazas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarRutas() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiRutas, 'getRutasLista', json).subscribe(
            result => {
                this.rutas = result;
            },
            error => {
                console.log(error);
            },
            () => {
                this.dataSource = new MatTableDataSource(this.rutas);
                this.dataSource.paginator = this.paginator;
            }
        );
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            idPlazaOrigen: 0,
            idPlazaDestino: 0,
            nombre: "",
            estatus: ""
        }
    }

    nuevo() {
        this.router.navigate(['pages/rutas/ruta', 'N']);
    }

    editar(ruta) {
        this.router.navigate(['pages/rutas/ruta', ruta.idRuta]);
    }

}
