import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';
import { Router, ActivatedRoute } from '@angular/router';
import { fcGeneral } from '../../../../services/general.service';
import "leaflet-map";
import "style-loader!leaflet/dist/leaflet.css";
import 'leaflet-routing-machine';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { debounceTime, tap, switchMap, finalize, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AppSettings } from '../../../../app.settings';
import { Settings } from '../../../../app.settings.model';
import { GenerarContratoComponent } from '../../../_modales/generar-contrato/generar-contrato.component';
import { ConfirmarConfiguracionComponent } from '../../../_modales/confirmar-configuracion/confirmar-configuracion.component';
import { RechazarCotizacionComponent } from '../../../_modales/rechazar-cotizacion/rechazar-cotizacion.component';
import { MapModalComponent } from './map-modal/map-modal.component';

declare var L: any; 

@Component({
  selector: 'app-cotizacion',
  templateUrl: './cotizacion.component.html',
  styleUrls: ['./cotizacion.component.scss']
})
export class CotizacionComponent implements OnInit {
    private urlApiAreas = 'api/Areas';
    private urlApiEmpresa = 'api/Empresa';
    private urlApiCliente = 'api/Cliente';
    private urlApiRutas = 'api/Rutas';
    private urlApiPlaza = 'api/Plaza';
    private urlApiCommon = 'api/Common';
    private urlApiTiposOperacion = 'api/TiposOperacion';
    private urlCotizador = 'api/Cotizador';
    private urlVehiculos = 'api/Vehiculos';
    private urlApiTarifas = 'api/CombustibleTarifas';
    private urlApiTarifasUrea = 'api/UreaTarifas';
    private urlApiEmpresaConfiguracion = 'api/EmpresaConfiguracion';
    private urlApiProductos = 'api/Productos';

    config = new MatSnackBarConfig();
    public settings: Settings;

    costos = [];
    costosVariables = [];
    costosFijos = [];
    costosOtros = [];

    ///////Drop downs////////
    areas: any[] = [];
    empresas: any[] = [];
    clientes: any[] = [];
    rutas: any[] = [];
    plazas: any[] = [];
    tiposVehiculo: any[] = [];
    monedas: any[] = [];
    tiposOperacion: any[] = [];
    tiposServicio: any[] = [];
    tiposRuta: any[] = [];
    condiciones: any[] = [];
    tiposCarretera: any[] = [];
    tiposParada: any[] = [];
    tiposMovimiento: any[] = [];
    frecuencias: any[] = [];
    tiposCobro: any[] = [];
    productos: any[] = [];
    unidades: any[] = [];
    tarifas: any[] = [];
    tarifasUrea: any[] = [];
    empresaConfiguracion: any = {};
    empresaConfiguracionModal: EmpresaConfigModel = new EmpresaConfigModel();
    filtroOrigen: Observable<any[]>;
    filtroDestino: Observable<any[]>;

    totalVariables = 0;
    totalFijos = 0;
    totalOtros = 0
    subtotal = 0;
    descuento = 0;
    totalDescuento = 0;
    ajuste = 0;
    total = 0;
    utilidad = 0;
    tarifa = 0;
    gastoCalculadoKm = 0;
    tarifaKm = 0;
    gastoCalculadoUM = 0;
    tarifaUM = 0;
    isClienteOtro = false;
    isProductoOtro = false;

    //Mascaras con formato Moneda
    timeoutSeconds = 3;
    totalVariablesMask = "0.00";
    totalFijosMask = "0.00";
    totalOtrosMask = "0.00";
    subtotalMask = "0.00";
    totalDescuentoMask = "0.00";
    totalMask = "0.00";
    tarifaMask = "0.00";
    gastoCalculadoKmMask = "0.00";
    tarifaKmMask = "0.00";
    gastoCalculadoUMMask = "0.00";
    tarifaUMMask = "0.00";

    public unidadMedida = [
        { id: 0, descripcion: 'No Aplica' },
        { id: 1, descripcion: 'Tonelada' },
        { id: 2, descripcion: 'KG' },
        { id: 3, descripcion: 'M3' },
        { id: 4, descripcion: 'Litros' },
        { id: 5, descripcion: 'Unitario' }
    ];

    /////////DATOS DE RUTA Y SEGMENTOS//////
    ruta: RutaModel = new RutaModel();
    rutaCalculada: any = {};
    latitude: number = 25.6801;
    longitude: number = -100.2988;
    map: any;
    layerGroup: any;
    routingControl = null;
    origenLat: any;
    origenLon: any;
    destinoLat: any;
    destinoLon: any;
    isSetDireccion = true;
    referenciaOrigen: DireccionReferenciaModel = new DireccionReferenciaModel();
    referenciaDestino: DireccionReferenciaModel = new DireccionReferenciaModel();

    edicion = false;
    idCotizacion: any;
    formEncabezado: FormGroup;
    formFrecuencia: FormGroup;
    formRutas: FormGroup;
    formSegmentos: FormArray;
    filtros: Observable<any[]>;

    isLoadEdition = false;
    campoRefOrigenVisibles = true;
    campoRefDestinoVisibles = true;
    campoNoExisteRuta = true;
    boolCamposOcultos = !true;
    subscription: any;

    boolCalculandoRuta: boolean = false;

    estatusCotizacion: string = "Nuevo";
    idCotizacionEstatus: number = 0;

    isSetMapView = false;
    isSetRoutingControls = false;
    isAbrirModalMap = false;

    hoy = new Date();
    observaciones = "Precio vigente a " + this.hoy.toLocaleString('es-MX', {month: 'long', year: 'numeric'}) + "\n" +
        "Estos precios son más IVA menos retención.\n" +
        "Los cambios en los precios de combustible, peajes y tipo de cambio del dólar afectan al precio cotizado.\n" +
        "La capacidad mínima para facturar, son las toneladas indicadas en la cotización.\n" +
        "El producto viaja por cuenta y riesgo del cliente.\n" +
        "Maniobras: Sin maniobra de carga o descarga\n" +
        "El viaje cuenta con 12 horas libres para cargar y 12 libres para descargar.\n" +
        "Después de este tiempo, se cobran demoras en carga o descarga a razón de $4,500.00 por cada 24 horas o fracción excedente.";
    constructor(private router: Router, public dialog: MatDialog, private _service: fcGeneral, public formBuilder: FormBuilder, private _snackBar: MatSnackBar, private route: ActivatedRoute, public appSettings: AppSettings) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
        this.consultarAreas();
        this.consultarEmpresas();
        this.consultarClientes();
        this.consultarRutas();
        this.consultarPlazas();
        this.consultarTiposVehiculo();
        this.consultarMonedas();
        this.consultarTiposOperacion();
        this.consultarTiposServicio();
        this.consultarTiposRuta();
        this.consultarCondicionesOrigen();
        this.consultarTiposCarretera();
        this.consultarTiposParada();
        this.consultarTiposMovimiento();
        this.consultarFrecuencias();
        this.consultarTiposCobro();
        this.consultarProductos();
        this.consultarUnidadesMedida();
        this.consultarTarifas();
        this.consultarTarifasUrea();
        //Inicializa MAPA, utiliza leafletjs por el template para la interacción con mapas (OPENSTREETMAPS). Doc: https://leafletjs.com/reference-1.7.1.html#map-example
        let el = document.getElementById("leaflet-map");
        L.Icon.Default.imagePath = 'assets/img/vendor/leaflet';
        this.map = L.map(el, { scrollWheelZoom: false}).setView([this.latitude, this.longitude], 15);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        /*this.map.getInteractions().forEach(function (interaction) {
            if (interaction instanceof L.interaction.MouseWheelZoom) {
                interaction.setActive(false);
            }
        }, this);*/

        this.layerGroup = L.layerGroup().addTo(this.map);

        this.buildFormEncabezado();
        this.buildFormFrecuencia();
        this.buildFormRuta();
        this.buildFormSegmentos(); 

        this.route.params.subscribe(params => {
            this.idCotizacion = params['id'];
            if (this.idCotizacion != "N") {
                this.idCotizacion = +params['id'];
                this.isLoadEdition = true;
                this.consultarCotizacion();
            } else {
                this.edicion = false;
                this.idCotizacion = 0;
                this.consultarCostosIniciales();
            }
            this.consultarEmpresaConfiguracion();
        });
    }

    ngAfterContentInit() {
        //Inicializa el rendimiento esperado con 2.0
        this.formRutas.get("formRendimiento").setValue('2');
        //Valores default para AUTOQUIMICOS
        this.formEncabezado.get("formEmpresa").setValue(+1);
        this.formEncabezado.get("formSucursales").setValue(+1);
        this.formEncabezado.get("formMoneda").setValue(+1);
        this.formEncabezado.get("formTipoCambio").setValue(+1);
    }
    ///////////////////////FORMULARIO DE ENCABEZADO////////////////////////
    buildFormEncabezado() {
        this.formEncabezado = this.formBuilder.group({
            formCliente: this.formBuilder.control(null, [Validators.required]),
            formTipoOperacion: this.formBuilder.control(null, [Validators.required]),
            formTipoServicio: this.formBuilder.control(null, [Validators.required]),
            formFolio: this.formBuilder.control(null, []),
            formEmpresa: this.formBuilder.control(null, [Validators.required]),
            formSucursales: this.formBuilder.control(null, [Validators.required]),
            formMoneda: this.formBuilder.control(null, [Validators.required]),
            formTipoCambio: this.formBuilder.control(null, []),
            formProductos: this.formBuilder.control(null, []),
            formCantidadUnidadMedida: this.formBuilder.control(null, []),
            formUnidadMedidaCarga: this.formBuilder.control(null, []),

            formClienteOtro: this.formBuilder.control(null, []),
            formProductoOtro: this.formBuilder.control(null, []),
        });

        const controlFolio = this.formEncabezado.get("formFolio");
        controlFolio.disable();

        const controlCliente = this.formEncabezado.get("formCliente");
        const controlClienteOtro = this.formEncabezado.get("formClienteOtro");
        controlCliente.valueChanges.subscribe(id => {
            if (id == 0) {
                this.isClienteOtro = true;
                controlClienteOtro.setValidators([Validators.required]);
            } else {
                this.isClienteOtro = false;
                controlClienteOtro.setValidators([]);
            }

            controlClienteOtro.updateValueAndValidity();
        });

        const controlProducto = this.formEncabezado.get("formProductos");
        const controlProductoOtro = this.formEncabezado.get("formProductoOtro");
        controlProducto.valueChanges.subscribe(id => {
            if (id == 0) {
                this.isProductoOtro = true;
                controlProductoOtro.setValidators([Validators.required]);
            } else {
                this.isProductoOtro = false;
                controlProductoOtro.setValidators([]);
            }

            controlProductoOtro.updateValueAndValidity();
        });
    }
    //////////////////////FORMULARIO DE FRECUENCIA///////////////////////
    buildFormFrecuencia() {
        this.formFrecuencia = this.formBuilder.group({
            formTipoFrecuencia: this.formBuilder.control(null, []),
            formCantidad: this.formBuilder.control(null, []),
            formTipoCobro: this.formBuilder.control(null, []),
            formUnidadMedida: this.formBuilder.control(null, [])
        });
    }
    //////////////////////////////////////////////FUNCIONALIDAD DE LA SECCION DE RUTA Y SEGMENTOS//////////////////////////////////
    buildFormRuta() {
        this.formRutas = this.formBuilder.group({
            formOrigen: this.formBuilder.control(null, []),
            formOrigenDireccion: this.formBuilder.control(null, []),
            formDestino: this.formBuilder.control(null, []),
            formDestinoDireccion: this.formBuilder.control(null, []),
            formTipoRuta: this.formBuilder.control(null, []),
            formKms: this.formBuilder.control(null, []),
            formRendimiento: this.formBuilder.control(null, [Validators.required]),
            formCondiciones: this.formBuilder.control(null, []),
            formTipoCarretera: this.formBuilder.control(null, [Validators.required]),
            formCombustibleTarifa: this.formBuilder.control(null, []),
            formTipoVehiculo: this.formBuilder.control(null, [Validators.required]),
            formRuta: this.formBuilder.control(null, []),
            formEjesExcedentes: this.formBuilder.control(null, []),
            formUreaTarifa: this.formBuilder.control(null, []),
            formRendimientoUrea: this.formBuilder.control(null, [Validators.required])
        });

        //Autocomplete Referencia Origen
        const controlOrigen = this.formRutas.get("formOrigenDireccion");
        this.filtroOrigen = controlOrigen.valueChanges.pipe(
            startWith(''),
            debounceTime(300),
            switchMap(value => this._service.postAPI(this.urlCotizador, 'getDireccionSegmento', { "lugarBusqueda": value }))
        );
        /*this.subscription = this.formRutas.get("formOrigenDireccion").valueChanges.subscribe(value => {
            if (!!value.id) {
                console.log("Tiene Valor");
            } else {
                console.log("PErdió Valor");
            }
        });*/
        //Autocomplete Referencia Destino; por default se encuentra habilitado para búsquedas directas
        const controlDestino = this.formRutas.get("formDestinoDireccion");
        this.filtroDestino = controlDestino.valueChanges.pipe(
            startWith(''),
            debounceTime(300),
            switchMap(value => this._service.postAPI(this.urlCotizador, 'getDireccionSegmento', { "lugarBusqueda": value }))
        );
    }

    buildFormSegmentos() {
        let groups = this.ruta.segmentos.map(segmento => {
            return new FormGroup({
                direccion: new FormControl(segmento.direccionSegmento, [Validators.required]),
                tipoParada: new FormControl(segmento.idRutaTipoParada, [Validators.required]),
                movimiento: new FormControl(segmento.idRutaMovimiento, [Validators.required])
            });
        });

        this.formSegmentos = new FormArray(groups);
    }

    agregarSegmento() {
        let segmento = new SegmentoModel();
        this.ruta.segmentos.push(segmento);

        let group = new FormGroup({
            direccion: new FormControl(null, [Validators.required]),
            tipoParada: new FormControl(null, [Validators.required]),
            movimiento: new FormControl(null, [Validators.required])
        });

        const control = group.get("direccion");

        segmento.filtro = control.valueChanges.pipe(
            startWith(''),
            debounceTime(300),
            switchMap(value => this._service.postAPI(this.urlCotizador, 'getDireccionSegmento', { "lugarBusqueda": value }))
        );

        this.formSegmentos.push(group);
    }

    getControlSegmento(index, campo) {
        return this.formSegmentos.at(index).get(campo) as FormControl;
    }

    
    setLatLon(tipo) {
        if ((!this.isLoadEdition) && (!this.boolCalculandoRuta)) {
            console.log("setLAtLon" + this.boolCalculandoRuta);
            if (tipo == 1) {
                if (this.ruta.idPlazaOrigen) {
                    const controlOrigen = this.formRutas.get("formOrigenDireccion");
                    let plazaO = this.plazas.find(x => x.id == this.ruta.idPlazaOrigen);
                    this.origenLat = plazaO.Lat;
                    this.origenLon = plazaO.Pos;
                    //Agrega el filtro de la Tarifa de Combustible en Origen
                    this.ruta.idCombustibleTarifaOrigen = +plazaO.idCombustibleTarifa;
                    this.consultarTarifasFiltradas();
                    //Busca si existe plaza en INEGI
                    this._service.postAPI(this.urlCotizador, 'getDireccionSegmento', { "lugarBusqueda": plazaO.descripcion, "idPlaza": this.ruta.idPlazaOrigen}).subscribe(
                        result => {
                            if (result.length == 0) {
                                this.openSnackBar("No se encontro la Plaza Origen en Mapa; favor de utilizar el campo de Referencia Origen.", "");
                                //Se habilita Referencia Origen
                                //controlOrigen.enable();
                                this.campoRefOrigenVisibles = true;
                            } else {
                                //Referencia Origen
                                let data = result[0];
                                this.referenciaOrigen.id = +data.id;
                                this.referenciaOrigen.lat = +data.lat;
                                this.referenciaOrigen.lon = +data.lon;
                                //this.referenciaOrigen.direccion = data.direccion;
                                //Agrega variables de Ruta
                                this.ruta.idSegmentoOrigen = +data.id;
                                this.ruta.idSegmentoOrigenLat = +data.lat;
                                this.ruta.idSegmentoOrigenLon = +data.lon;
                                //this.ruta.origenDireccionSegmento = data.direccion; 
                                this.formRutas.get("formOrigenDireccion").setValue('');
                                //controlOrigen.disable();
                                this.campoRefOrigenVisibles = true;
                            }
                        },
                        error => {
                            //controlOrigen.disable();
                            this.campoRefOrigenVisibles = true;
                            console.log("error: " + error);
                            this.openSnackBar(error.error.Message, "");
                        }
                    );
                }
            }
            if (tipo == 2) {
                if (this.ruta.idPlazaDestino) {
                    const controlDestino = this.formRutas.get("formDestinoDireccion");
                    let plazaD = this.plazas.find(x => x.id == this.ruta.idPlazaDestino);
                    this.destinoLat = plazaD.Lat;
                    this.destinoLon = plazaD.Pos;
                    //Agrega el filtro de la Tarifa de Combustible en Destino
                    this.ruta.idCombustibleTarifaDestino = +plazaD.idCombustibleTarifa;
                    this.consultarTarifasFiltradas();
                    //Busca si existe plaza en INEGI
                    setTimeout(() => this._service.postAPI(this.urlCotizador, 'getDireccionSegmentoAux', { "lugarBusqueda": plazaD.descripcion, "idPlaza": this.ruta.idPlazaDestino }).subscribe(
                        result => {
                            if (result.length == 0) {
                                this.openSnackBar("No se encontro la Plaza Destino en Mapa; favor de utilizar el campo de Referencia Destino.", "");
                                //controlDestino.enable();
                                this.campoRefDestinoVisibles = true;
                            } else {
                                let data = result[0];
                                //Referencia Destino
                                this.referenciaDestino.id = +data.id;
                                this.referenciaDestino.lat = +data.lat;
                                this.referenciaDestino.lon = +data.lon;
                                //this.referenciaDestino.direccion = data.direccion;
                                //Agrega variables de Ruta
                                this.ruta.idSegmentoDestino = +data.id;
                                this.ruta.idSegmentoDestinoLat = +data.lat;
                                this.ruta.idSegmentoDestinoLon = +data.lon;
                                //this.ruta.destinoDireccionSegmento = data.direccion; 
                                this.formRutas.get("formDestinoDireccion").setValue('');
                                //controlDestino.disable();
                                this.campoRefDestinoVisibles = true;
                            }
                        },
                        error => {
                            //controlDestino.disable();
                            this.campoRefDestinoVisibles = true;
                            console.log("error: " + error);
                            this.openSnackBar(error.error.Message, "");
                        }
                    ), 1000 * this.timeoutSeconds);
                }
            }
            this.setMapView();
            if ((this.ruta.idPlazaOrigen == "" || this.ruta.idPlazaOrigen == "0" || this.ruta.idPlazaOrigen == null) && (this.ruta.idPlazaDestino == "" || this.ruta.idPlazaDestino == "0" || this.ruta.idPlazaDestino == null)) { //&& (+this.ruta.idRuta == 0 || this.ruta.idRuta === undefined)
                const controlRuta = this.formRutas.get("formRuta");
                controlRuta.enable();
            } else {
                const controlRuta = this.formRutas.get("formRuta");
                if (this.ruta.idRuta > 0) {
                    controlRuta.enable();
                } else {
                    controlRuta.disable();
                }
            }
        }
    }

    setRuta() {
        if (!this.boolCalculandoRuta) {
            console.log("setRuta" + this.boolCalculandoRuta);
            let infoRuta = this.rutas.find(x => x.id == this.ruta.idRuta);
            if (infoRuta) {
                this.ruta.idPlazaOrigen = infoRuta.idPlazaOrigen;
                this.ruta.idPlazaDestino = infoRuta.idPlazaDestino;
                this.ruta.kms = infoRuta.kms;
                //Bloquea campos que no puedes ser modificados al existir una Ruta
                this.campoNoExisteRuta = false;
                //Deshabilita campos de Origen y Destino
                const controlOrigen = this.formRutas.get("formDestino");
                controlOrigen.disable();
                const controlDestino = this.formRutas.get("formOrigen");
                controlDestino.disable();
                //Bloquea Referencias INEGI
                const controlRefOrigen = this.formRutas.get("formOrigenDireccion");
                this.formRutas.get("formOrigenDireccion").setValue('');
                //controlRefOrigen.disable();
                const controlRefDestino = this.formRutas.get("formDestinoDireccion");
                this.formRutas.get("formDestinoDireccion").setValue('');
                //controlRefDestino.disable();
            } else {
                //console.log("func setRuta(): origen: " + this.ruta.idPlazaOrigen + ". Destino: " + this.ruta.idPlazaDestino);
                /*if ((this.ruta.idPlazaOrigen == "" || this.ruta.idPlazaOrigen == "0" || this.ruta.idPlazaOrigen == null) && (this.ruta.idPlazaDestino == "" || this.ruta.idPlazaDestino == "0" || this.ruta.idPlazaDestino == null)) {
                    this.ruta.idPlazaOrigen = null;
                    this.ruta.idPlazaDestino = null;
                }*/
                //Se borro o quito la ruta; hace reseteo de 
                this.ruta.idPlazaOrigen = null;
                this.ruta.idPlazaDestino = null;
                this.ruta.kms = 0;
                this.campoNoExisteRuta = true;
                //Habilita campos de Origen y Destino
                const controlOrigen = this.formRutas.get("formDestino");
                controlOrigen.enable();
                const controlDestino = this.formRutas.get("formOrigen");
                controlDestino.enable();
                //Habilita Referencias INEGI
                const controlRefOrigen = this.formRutas.get("formOrigenDireccion");
                this.formRutas.get("formOrigenDireccion").setValue('');
                controlRefOrigen.enable();
                const controlRefDestino = this.formRutas.get("formDestinoDireccion");
                this.formRutas.get("formDestinoDireccion").setValue('');
                controlRefDestino.enable();
            }
        }
        
    }

    setRendimiento() {
        let infoVehiculo = this.tiposVehiculo.find(x => x.id == this.ruta.idVehiculoArmado);
        if (infoVehiculo) {
            this.ruta.rendimiento = infoVehiculo.rendimiento;
            this.ruta.rendimientoUrea = infoVehiculo.rendimientoUrea;
        }
    }

    setMapView() {
        this.layerGroup.clearLayers();
        let origen = false;
        let destino = false;

        if (this.origenLat && this.origenLon) {
            let markerO = L.marker([this.origenLat, this.origenLon], { title: "Origen" }).addTo(this.layerGroup);
            origen = true;
        }
        for (let s of this.ruta.segmentos) { //Se agrega el LatLon de cada segmento
            let markerS = L.marker([s.posLat, s.posLon], { title: s.direccionSegmento, draggable: true }).addTo(this.layerGroup)
            markerS.on('dragend', function (e) {
                s.posLat = e.target._latlng.lat;
                s.posLon = e.target._latlng.lng;
            })
        }
        if (this.destinoLat && this.destinoLon) {
            let markerD = L.marker([this.destinoLat, this.destinoLon], { title: "Destino" }).addTo(this.layerGroup);
            destino = true;
        }

        if (origen) {
            this.map.setView(new L.LatLng(this.origenLat, this.origenLon), 10);
        }

        this.isSetMapView = true;
    }

    calcularKmTotal() {
        let that = this;
        let points = [];
        points.push([this.ruta.idSegmentoOrigenLat, this.ruta.idSegmentoOrigenLon])
        for (let s of this.ruta.segmentos) {
            points.push([s.posLat, s.posLon])
        }
        points.push([this.ruta.idSegmentoDestinoLat, this.ruta.idSegmentoDestinoLon])

        if (this.routingControl != null) {
            this.map.removeControl(this.routingControl);
        }

        this.routingControl = L.Routing.control({
            waypoints: points,
            lineOptions: {
                addWaypoints: false
            },
            altLineOptions: {
                addWaypoints: false,
                styles: { color: 'red', opacity: 0.0, weight: 0 },
            },
            show: false
        }).addTo(this.map);

        this.routingControl.on('routesfound', function (e) {
            var routes = e.routes;
            var summary = routes[0].summary;
            that.ruta.kms = Math.round(summary.totalDistance / 1000);

            that.calcularRuta();
        });  
    }

    setRoutingControls() {
        this.isSetRoutingControls = true;
        let that = this;
        this.layerGroup.clearLayers();
        let points = [];
        if (this.rutaCalculada.coordenadas) {
            for (let c of this.rutaCalculada.coordenadas) { //Se agrega el LatLon de cada segmento
                if (c.lat && c.lon) {
                    points.push(L.latLng(c.lat, c.lon));
                }
            }
        }
        
        if (this.routingControl != null) {
            this.map.removeControl(this.routingControl);
        }

        this.routingControl = L.Routing.control({
            createMarker: function (i: number, waypoint: any, n: number) {
                let marker = L.marker(waypoint.latLng, { draggable: false });
                /*marker.on('dragend', function (e) {
                    if (i == 0) { //Es el marker de origen
                        that.ruta.idSegmentoOrigenLat = e.target._latlng.lat;
                        that.ruta.idSegmentoOrigenLon = e.target._latlng.lng;
                    } else if (i == n - 1) { //Es el marker de destino
                        that.ruta.idSegmentoDestinoLat = e.target._latlng.lat;
                        that.ruta.idSegmentoDestinoLon = e.target._latlng.lng;
                    } else { //Son los marker de cada segmento
                        that.ruta.segmentos[i - 1].posLat = e.target._latlng.lat;
                        that.ruta.segmentos[i - 1].posLon = e.target._latlng.lng;
                    }
                });*/

                return marker;
            },
            waypoints: points,
            lineOptions: {
                addWaypoints: false
            },
            altLineOptions: {
                addWaypoints: false,
                styles: {color: 'red', opacity: 0.0, weight: 0},    
            },
            show: false
        }).addTo(this.map);

        this.routingControl.on('routesfound', function (e) {
            var routes = e.routes;
            var summary = routes[0].summary;
            console.log("Distancia total: " + summary.totalDistance / 1000 + "km");
            //alert('Total distance is ' + summary.totalDistance / 1000 + ' km and total time is ' + Math.round(summary.totalTime % 3600 / 60) + ' minutes');
        });
        //this.settings.loadingSpinner = false;
    }

    setDireccion(s: SegmentoModel, option) {
        if (this.isSetDireccion) {
            s.idSegmentoINEGI = +option.id;
            s.posLat = +option.lat;
            //s.lat = +option.lat;
            s.posLon = +option.lon;
            //s.lon = +option.lon;

            this.setMapView();
        } else {
            this.isSetDireccion = true;
        }
    }

    setDireccionReferencia(option, tipo: number) {
        if ((!this.isLoadEdition) && (!this.boolCalculandoRuta)) {
            console.log("setDireccionReferencia" + this.boolCalculandoRuta);
            console.log("func setRuta(): origen: " + this.ruta.idPlazaOrigen + ". Destino: " + this.ruta.idPlazaDestino);
            var updatePlazaParams: any = {};
            if (tipo == 1) {
                //Referencia Origen
                this.referenciaOrigen.id = +option.id;
                this.referenciaOrigen.lat = +option.lat;
                this.referenciaOrigen.lon = +option.lon;
                this.referenciaOrigen.direccion = option.direccion;
                //Agrega variables de Ruta
                this.ruta.idSegmentoOrigen = +option.id;
                this.ruta.idSegmentoOrigenLat = +option.lat;
                this.ruta.idSegmentoOrigenLon = +option.lon;
                this.ruta.origenDireccionSegmento = option.direccion;
                //Actualiza la referencia seleccionada en la Plaza
                if (this.ruta.idPlazaOrigen) {
                    if (+this.ruta.idPlazaOrigen > 0) {
                        updatePlazaParams.idPlaza = this.ruta.idPlazaOrigen;
                        updatePlazaParams.IdINEGI = +option.id;
                        updatePlazaParams.DireccionINEGI = option.direccion;
                        updatePlazaParams.LatINEGI = +option.lat;
                        updatePlazaParams.LonINEGI = +option.lon;
                        let json = JSON.stringify(updatePlazaParams);
                        this._service.postAPI(this.urlCotizador, 'updateRefPlaza', json).subscribe(
                            result => {
                                console.log("Plaza Actualizada");
                            },
                            error => {
                                console.log("Error al actualizar Plaza");
                            }
                        );
                    }
                }
            } else {
                //Referencia Destino
                this.referenciaDestino.id = +option.id;
                this.referenciaDestino.lat = +option.lat;
                this.referenciaDestino.lon = +option.lon;
                this.referenciaDestino.direccion = option.direccion;
                //Agrega variables de Ruta
                this.ruta.idSegmentoDestino = +option.id;
                this.ruta.idSegmentoDestinoLat = +option.lat;
                this.ruta.idSegmentoDestinoLon = +option.lon;
                this.ruta.destinoDireccionSegmento = option.direccion;
                //Actualiza la referencia seleccionada en la Plaza
                if (this.ruta.idPlazaDestino) {
                    if (+this.ruta.idPlazaDestino > 0) {
                        updatePlazaParams.idPlaza = this.ruta.idPlazaDestino;
                        updatePlazaParams.IdINEGI = +option.id;
                        updatePlazaParams.DireccionINEGI = option.direccion;
                        updatePlazaParams.LatINEGI = +option.lat;
                        updatePlazaParams.LonINEGI = +option.lon;
                        let json = JSON.stringify(updatePlazaParams);
                        this._service.postAPI(this.urlCotizador, 'updateRefPlaza', json).subscribe(
                            result => {
                                console.log("Plaza Actualizada");
                            },
                            error => {
                                console.log("Error al actualizar Plaza");
                            }
                        );
                    }
                }
                
            }
            //Valida si está trabajando solo con referencias
            //console.log("func setDireccionReferencia(): origen: " + this.ruta.idPlazaOrigen + ". destino: " + this.ruta.idPlazaDestino + ". ruta: " + this.ruta.idRuta);
            if (!(this.ruta.idPlazaOrigen) && !(this.ruta.idPlazaDestino) && !(this.ruta.idRuta)) {
                //No se tiene ingresada Ruta ni plaza origen ni plaza destino
                this.habilitaRutaPlazasReferencias(false, true, true);
            }
        }
    }

    calcularRuta() {
        this.isSetMapView = false;
        //Valida Combinación de rutas
        if (this.ruta.idRuta === undefined) {
            this.ruta.idRuta = 0;
        }
        if (+this.ruta.idRuta == 0 || this.ruta.idRuta === undefined) {
            //No existe la ruta; valida que exista Plaza Origen y/o Destino
            if ((+this.ruta.idPlazaOrigen > 0 && +this.ruta.idPlazaDestino > 0) || (+this.ruta.idSegmentoOrigen > 0 && +this.ruta.idSegmentoDestino > 0)) {
                this.procesaRuta();
            } else {
                this.openSnackBar("Es necesario seleccionar una Plaza Origen y Destino válida y/o Referencia Origen y Destino válida.", "");
            }
        } else {
            this.procesaRuta();
        }
    }

    procesaRuta() {
        if (this.formRutas.valid && this.formRutas.valid) {
            this.settings.loadingSpinner = true; //Activa el spinner antes de llamar API
            this.getRuta();
        } else {
            Object.keys(this.formRutas.controls).forEach(key => {
                const ctrl = this.formRutas.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            for (let control of this.formSegmentos.controls as any) {
                Object.keys(control.controls).forEach(key => {
                    const ctrl = control.get(key);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
            this.openSnackBar("Favor de llenar los campos requeridos de Ruta.", "");
        }
    }

    getRuta() {
        //Renderea los campos numéricos (deciales, enteros, etc...) para evitar BarRequest 400 en llamado a API
        this.boolCalculandoRuta = true;
        let defVar: any = 0;
        this.ruta.rendimiento = +this.ruta.rendimiento;
        this.ruta.rendimientoUrea = +this.ruta.rendimientoUrea;
        this.ruta.ejesExcedentes = +this.ruta.ejesExcedentes;
        //Realiza Validaciones
        if (!this.ruta.idRuta) {
            this.ruta.idRuta = 0;
        }
        if (!this.ruta.idPlazaOrigen) {
            this.ruta.idPlazaOrigen = defVar;
        }
        if (!this.ruta.idPlazaDestino) {
            this.ruta.idPlazaDestino = defVar;
        }
        let json = JSON.stringify(this.ruta);
        //console.log(json);
        this._service.postAPI(this.urlCotizador, 'getRuta', json).subscribe(
            result => {
                this.rutaCalculada = result;
                //console.log(this.rutaCalculada);
                this.ruta.kms = this.rutaCalculada.kms;
                this.costosRutaCalculada();
                this.setRoutingControls();
                this.verificarConfEmpresa();
                this.settings.loadingSpinner = false; //Finaliza Spinner de Cargando
                /*if ((this.ruta.idPlazaOrigen == "" || this.ruta.idPlazaOrigen == "0" || this.ruta.idPlazaOrigen == null) && (this.ruta.idPlazaDestino == "" || this.ruta.idPlazaDestino == "0" || this.ruta.idPlazaDestino == null)) { //&& (+this.ruta.idRuta == 0 || this.ruta.idRuta === undefined)
                    const controlRuta = this.formRutas.get("formRuta");
                    controlRuta.enable();
                } else {
                    const controlRuta = this.formRutas.get("formRuta");
                    controlRuta.disable();
                }*/
                //Regresa campos default 
                if (this.ruta.idRuta == 0) {
                    this.ruta.idRuta = null;
                }
                if (this.ruta.idPlazaOrigen == "0") {
                    this.ruta.idPlazaOrigen = null;
                }
                if (this.ruta.idPlazaDestino == "0") {
                    this.ruta.idPlazaDestino = null;
                }
                setTimeout(() => {
                    this.boolCalculandoRuta = false;
                }, 5000);
                //
            },
            error => {
                console.log(error);
                this.settings.loadingSpinner = false; //Finaliza Spinner de Cargando
                this.openSnackBar("Error al intentar obtener la ruta. Favor de verificar nuevamente Referencia Origen y Referencia Destino.", "");
                //Regresa campos default 
                if (this.ruta.idRuta == 0) {
                    this.ruta.idRuta = null;
                }
                if (this.ruta.idPlazaOrigen == "0") {
                    this.ruta.idPlazaOrigen = null;
                }
                if (this.ruta.idPlazaDestino == "0") {
                    this.ruta.idPlazaDestino = null;
                }
                this.boolCalculandoRuta = false;
            }
        );
    }

    verificarConfEmpresa() {
        //Combustible
        var countOptions: number = 0;
        var boolSelectVariableCombustible = false;
        var boolSelectVariableCasetas = false;
        this.empresaConfiguracion.showCombustible = false;
        this.empresaConfiguracion.showCasetas = false;
        this.ruta.tipoCalculoCombustible = 0;
        this.ruta.tipoCalculoCombustibleTarifa = 0;
        this.ruta.tipoCalculoCombustibleIdCostoTipo = 0;
        this.ruta.tipoCalculoCasetas = 0;
        this.ruta.tipoCalculoCasetasTarifa = 0;
        this.ruta.tipoCalculoCasetasIdCostoTipo = 0;
        this.ruta.tipoCalculoSueldo = 0;
        this.ruta.tipoCalculoSueldoTarifa = 0;
        this.ruta.tipoCalculoSueldoIdCostoTipo = 0;
        /*
         * Tipos de Cálculo:
         * -Combustible: 
         *      0 - No aplica
         *      1 - Automático (de APIS INEGI y Rendimiento)
         *      2 - Cálculo por Tarifa (de Catálogo de Tarifas)
         *      3 - Histórico ZAM
         *      4 - Manual (factores configurados)
         * -Casetas:
         *      0 - No aplica
         *      1 - Automático (de APIS INEGI)
         *      2 - Histótico ZAM
         *      3 - Archivo (archivos de IAVE o TXT General)
         *      4 - Manual (factores configurados)
         * -Sueldos:
         *      0 - No aplica
         *      1 - Histórico ZAM
         *      2 - Manual (factores configurados)
         */
        if (this.rutaCalculada.costoCombustibleAutomatico > 0) { //this.empresaConfiguracion.Var_ComCalculoAuto
            this.ruta.tipoCalculoCombustible = 1;
            countOptions++;
        }
        if (this.rutaCalculada.costoCombustibleHistorico > 0) { //this.empresaConfiguracion.Var_ComHistorico
            this.ruta.tipoCalculoCombustible = 3;
            countOptions++;
        }
        if (this.rutaCalculada.costoCombustibleTarifa > 0) { //this.empresaConfiguracion.Var_ComTarifa
            this.ruta.tipoCalculoCombustible = 2;
            countOptions++;
        }
        if (this.rutaCalculada.costoCombustibleManual > 0) { //this.empresaConfiguracion.Var_ComManual
            this.ruta.tipoCalculoCombustible = 4;
            this.ruta.tipoCalculoCombustibleTarifa = this.empresaConfiguracion.Var_ComFactor;
            this.ruta.tipoCalculoCombustibleIdCostoTipo = this.empresaConfiguracion.Var_ComIdCostoTipo;
            countOptions++;
        }
        //Determina si hay mas de una opción configurada en Variables de Combustible
        if (countOptions > 1) {
            this.ruta.tipoCalculoCombustible = 0;
            this.ruta.tipoCalculoCombustibleTarifa = 0;
            this.ruta.tipoCalculoCombustibleIdCostoTipo = 0;
            boolSelectVariableCombustible = true;
            this.empresaConfiguracion.showCombustible = true;
        }
        //Casetas
        countOptions = 0;
        if (this.rutaCalculada.costoCasetasAutomatico > 0) { //this.empresaConfiguracion.Var_CasCalculoAuto
            this.ruta.tipoCalculoCasetas = 1;
            countOptions++;
        }
        if (this.rutaCalculada.costoCasetasHistorico > 0) { //this.empresaConfiguracion.Var_CasHistorico
            this.ruta.tipoCalculoCasetas = 2;
            countOptions++;
        }
        if (this.rutaCalculada.costoCasetasArchivo > 0) { //this.empresaConfiguracion.Var_CasArchivo
            this.ruta.tipoCalculoCasetas = 3;
            countOptions++;
        }
        if (this.rutaCalculada.costoCasetasManual > 0) { //this.empresaConfiguracion.Var_CasManual
            this.ruta.tipoCalculoCasetas = 4;
            this.ruta.tipoCalculoCasetasTarifa = this.empresaConfiguracion.Var_CasFactor;
            this.ruta.tipoCalculoCasetasIdCostoTipo = this.empresaConfiguracion.Var_CasIdCostoTipo;
            countOptions++;
        }
        //Determina si hay mas de una opción configurada en Variables de Casetas
        if (countOptions > 1) {
            this.ruta.tipoCalculoCasetas = 0;
            this.ruta.tipoCalculoCasetasTarifa = 0;
            this.ruta.tipoCalculoCasetasIdCostoTipo = 0;
            boolSelectVariableCasetas = true;
            this.empresaConfiguracion.showCasetas = true;
        }
        //Sueldos; no puede haber mas de 2 seleccionados, por lo que solo obtiene su valor
        if (this.rutaCalculada.costoSueldoHistorico > 0) { //this.empresaConfiguracion.Var_SueHistorico
            this.ruta.tipoCalculoSueldo = 1;
            countOptions++;
        }
        if (this.rutaCalculada.costoSueldoManual > 0) { //this.empresaConfiguracion.Var_SueManual
            this.ruta.tipoCalculoSueldo = 2;
            this.ruta.tipoCalculoSueldoTarifa = this.empresaConfiguracion.Var_SueFactor;
            this.ruta.tipoCalculoSueldoIdCostoTipo = this.empresaConfiguracion.Var_SueIdCostoTipo;
            countOptions++;
        }
        if (boolSelectVariableCombustible == true || boolSelectVariableCasetas == true) {
            //console.log(this.idCotizacion);
            if (this.idCotizacion <= 0) {
                this.abrirModalConfigEmpresa();
            }
        }
    }

    openSnackBar(message: string, action: string) {
        this._snackBar.open(message, action, {
            duration: 2000,
        });
    }

    ///////////////////////////////////////////////CONSULTA DE COSTOS/////////////////////////////////////////////////
    consultarCostosIniciales() {
        this.settings.loadingSpinner = true;
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlCotizador, 'getCostosIniciales', json).subscribe(
            result => {
                let data = result;
                this.costos = data;
                for (let d of data) {
                    if (d.tipo === 'variable') {
                        this.costosVariables.push(d);
                    }
                    if (d.tipo === 'fijo') {
                        this.costosFijos.push(d);
                    }
                    /*if (d.tipo === 'otros') {
                        this.costosOtros.push(d);
                    }*/
                }
                this.calcularCostoVariable();
                this.calcularCostoFijo();

                this.settings.loadingSpinner = false;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarCostosPorKms() {
        //TODO: No regenera cálculo de costos al modificar KMS hasta que se genere un nuevo criterio.
        /*this.settings.loadingSpinner = true;
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            kms: +this.ruta.kms
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlCotizador, 'getCostosPorKMS', json).subscribe(
            result => {
                let data = result;
                //this.costos = data;
                for (let d of data) {
                    if (d.tipo === 'variable') {
                        let i = this.costosVariables.map(cv => cv.id).indexOf(d.id);
                        this.costosVariables[i].monto = d.monto;
                    }
                    //if (d.tipo === 'fijo') {
                        let i = this.costosFijos.map(cf => cf.id).indexOf(d.id);
                        this.costosFijos[i].monto = d.monto;
                    }
                    if (d.tipo === 'otros') {
                        let i = this.costosOtros.map(co => co.id).indexOf(d.id);
                        this.costosOtros[i].monto = d.monto;
                    }//
                }
                this.calcularCostoVariable();
                this.calcularCostoFijo();

                this.settings.loadingSpinner = false;
            },
            error => {
                this.settings.loadingSpinner = false;
                console.log("error: " + error);
            }
        );*/
    }


    agregaEjesExcedentes() {

    }

    costosRutaCalculada() {
        console.log(this.rutaCalculada.costos);
        for (let c of this.rutaCalculada.costos) {
            console.log(c);
            if (c.tipo === 'variable') {
                let i = this.costosVariables.map(cv => cv.id).indexOf(c.id);
                this.costosVariables[i].monto = c.monto;
            }
            if (c.tipo === 'fijo') {
                let i = this.costosFijos.map(cf => cf.id).indexOf(c.id);
                this.costosFijos[i].monto = c.monto;
            }
            /*if (c.tipo === 'otros') {
                let i = this.costosOtros.map(co => co.id).indexOf(c.id);
                this.costosOtros[i].monto = c.monto;
            }*/
        }
        this.calcularCostoVariable();
        this.calcularCostoFijo();
    }
    ///////////////////////////////////////////CARGA DE DROP DOWN LISTS//////////////////////////////////////////////
    consultarAreas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiAreas, 'getAreasByEmpresaLista', json).subscribe(
            result => {
                this.areas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarEmpresas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiEmpresa, 'getEmpresaLista', json).subscribe(
            result => {
                this.empresas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarClientes() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiCliente, 'getClientesListaAutocomplete', json).subscribe(
            result => {
                this.clientes = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarRutas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiRutas, 'getRutasListaAutocomplete', json).subscribe(
            result => {
                this.rutas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarPlazas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPlaza, 'getPlazaListaByEmpresaArea', json).subscribe(
            result => {
                this.plazas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposVehiculo() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlVehiculos, 'getVehiculoListaByEmpresaAreaModel', json).subscribe(
            result => {
                this.tiposVehiculo = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposVehiculoFiltrado() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1,
            unidadMedida: +this.formEncabezado.get("formUnidadMedidaCarga").value,
            carga: +this.formEncabezado.get("formCantidadUnidadMedida").value,
            idProducto: +this.formEncabezado.get("formProductos").value
        }
        let json = JSON.stringify(param);
        this.tiposVehiculo = [];
        this._service.postAPI(this.urlVehiculos, 'getVehiculoListaByEmpresaAreaModel', json).subscribe(
            result => {
                this.tiposVehiculo = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarMonedas() {
        this._service.getAPI(this.urlApiCommon, 'getMonedasListado').subscribe(
            result => {
                this.monedas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposServicio() {
        this._service.getAPI(this.urlApiCommon, 'getTiposServiciosListado').subscribe(
            result => {
                this.tiposServicio = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposOperacion() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiTiposOperacion, 'getTiposOperacionByEmpresaArea', json).subscribe(
            result => {
                this.tiposOperacion = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposRuta() {
        this._service.getAPI(this.urlApiCommon, 'getRutaTiposLista').subscribe(
            result => {
                this.tiposRuta = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarCondicionesOrigen() {
        this._service.getAPI(this.urlApiCommon, 'getRutaCondicionesOrigenLista').subscribe(
            result => {
                this.condiciones = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposCarretera() {
        this._service.getAPI(this.urlApiCommon, 'getTiposCarreteraListado').subscribe(
            result => {
                this.tiposCarretera = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposParada() {
        this._service.getAPI(this.urlApiCommon, 'getRutaTiposParadaLista').subscribe(
            result => {
                this.tiposParada = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposMovimiento() {
        this._service.getAPI(this.urlApiCommon, 'getRutaMovimientosLista').subscribe(
            result => {
                this.tiposMovimiento = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarFrecuencias() {
        this._service.getAPI(this.urlApiCommon, 'getFrecuenciasServicioListado').subscribe(
            result => {
                this.frecuencias = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTiposCobro() {
        this._service.getAPI(this.urlApiCommon, 'getTipoCobroListado').subscribe(
            result => {
                this.tiposCobro = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarProductos() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiProductos, 'getProductosAutocomplete', json).subscribe(
            result => {
                this.productos = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarUnidadesMedida() {
        this._service.getAPI(this.urlApiCommon, 'getUnidadesMedidaListado').subscribe(
            result => {
                this.unidades = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTarifas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTarifas, 'getCombustibleTarifasAutocomplete', json).subscribe(
            result => {
                this.tarifas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarTarifasUrea() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiTarifasUrea, 'getUreaTarifasAutocomplete', json).subscribe(
            result => {
                this.tarifasUrea = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    consultarEmpresaConfiguracion() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiEmpresaConfiguracion, 'getCostosVariablesEmpresa', json).subscribe(
            result => {
                this.empresaConfiguracion = result;
                //Valida la configuración de la Empresa para determinar el cálculo de costos
                //this.verificarConfEmpresa();
            },
            error => {
                console.log("error: " + error);
            }
        );
    }

    abrirModalConfigEmpresa() {
        const dialogRef = this.dialog.open(ConfirmarConfiguracionComponent, {
            width: '35%',
            disableClose: true,
            data: { configuracion: this.empresaConfiguracion, rutaCalculada: this.rutaCalculada }
        });

        dialogRef.afterClosed().subscribe(result => {
            //console.log(result.data);
            this.ruta.tipoCalculoCombustible = result.data.tipoCalculoCombustible;
            this.ruta.tipoCalculoCasetas = result.data.tipoCalculoCasetas;
            //COMBUSTIBLE
            if (+result.data.tipoCalculoCombustible == 1) {
                //Se selecciono Automático
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Combustible') {
                        c.monto = (this.rutaCalculada.costoCombustibleAutomatico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 }));//parseFloat(this.rutaCalculada.costoCombustibleAutomatico).toFixed(2);
                        //console.log("Costo calculado Combustible (Automático)");
                    }
                }
            } else if (+result.data.tipoCalculoCombustible == 2) {
                //Se selecciono Tarifa
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Combustible') {
                        c.monto = (this.rutaCalculada.costoCombustibleTarifa.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); //parseFloat(this.rutaCalculada.costoCombustibleTarifa).toFixed(2);
                        //console.log("Costo calculado Combustible (Tarifa)");
                    }
                }
            } else if (+result.data.tipoCalculoCombustible == 3) {
                //Se selecciono historico
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Combustible') {
                        c.monto = (this.rutaCalculada.costoCombustibleHistorico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); //parseFloat(this.rutaCalculada.costoCombustibleHistorico).toFixed(2);
                        //console.log("Costo calculado Combustible (Historico)");
                    }
                }
            } else {
                //Costo Manual
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Combustible') {
                        c.monto = (this.rutaCalculada.costoCombustibleManual.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); //parseFloat(this.rutaCalculada.costoCombustibleManual).toFixed(2);
                        //console.log("Costo calculado Combustible (Manual)");
                    }
                }
            }
            //CASETAS
            if (+result.data.tipoCalculoCasetas == 1) {
                //Se selecciono Automático
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Casetas') {
                        c.monto = (this.rutaCalculada.costoCasetasAutomatico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); //parseFloat(this.rutaCalculada.costoCasetasAutomatico).toFixed(2);
                        //console.log("Costo calculado Casetas (Automático)");
                    }
                }
            } else if (+result.data.tipoCalculoCasetas == 2) {
                //Se selecciono Histórico
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Casetas') {
                        c.monto = (this.rutaCalculada.costoCasetasHistorico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); //parseFloat(this.rutaCalculada.costoCasetasHistorico).toFixed(2);
                        //console.log("Costo calculado Casetas (Historico)");
                    }
                }
            } else if (+result.data.tipoCalculoCasetas == 3) {
                //Se selecciono Archivo
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Casetas') {
                        c.monto = (this.rutaCalculada.costoCasetasArchivo.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); // parseFloat(this.rutaCalculada.costoCasetasArchivo).toFixed(2);
                        //console.log("Costo calculado Casetas (Archivo)");
                    }
                }
            } else {
                //Costo Manual
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Casetas') {
                        c.monto = (this.rutaCalculada.costoCasetasManual.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); //  parseFloat(this.rutaCalculada.costoCasetasManual).toFixed(2);
                        //console.log("Costo calculado Casetas (Manual)");
                    }
                }
            }
            //SUELDOS: Solo se calcula de forma Manual o Histórica
            if (this.empresaConfiguracion.Var_SueHistorico) {
                //Cálculo de sueldo por Historico
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Sueldo') {
                        c.monto = (this.rutaCalculada.costoSueldoHistorico.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); // parseFloat(this.rutaCalculada.costoSueldoHistorico).toFixed(2);
                        //console.log("Costo calculado Sueldo (Historico)");
                    }
                }
            } else if (this.empresaConfiguracion.Var_SueManual) {
                //Calculo de sueldo Manual
                for (let c of this.rutaCalculada.costos) {
                    if (c.tipo === 'variable' && c.id === 'Sueldo') {
                        c.monto = (this.rutaCalculada.costoSueldoManual.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 })); // parseFloat(this.rutaCalculada.costoSueldoManual).toFixed(2);
                        //console.log("Costo calculado Sueldo (Manual)");
                    }
                }
            }
            this.costosRutaCalculada();
            //this.ruta.tipoCalculoSueldo = result.data.tipoCalculoSueldo;
        });
    }

    /*
      costosRutaCalculada() {
        for (let c of this.rutaCalculada.costos) {
            if (c.tipo === 'variable') {
                let i = this.costosVariables.map(cv => cv.id).indexOf(c.id);
                this.costosVariables[i].monto = c.monto;
            }
            if (c.tipo === 'fijo') {
                let i = this.costosFijos.map(cf => cf.id).indexOf(c.id);
                this.costosFijos[i].monto = c.monto;
            }
            if (c.tipo === 'otros') {
                let i = this.costosOtros.map(co => co.id).indexOf(c.id);
                this.costosOtros[i].monto = c.monto;
            }
        }
        this.calcularCostoVariable();
        this.calcularCostoFijo();
    }
     * */


    consultarTarifasFiltradas() {
        //Elimina posibles valores nulls antes de request
        if (this.ruta.idCombustibleTarifaOrigen == null) {
            this.ruta.idCombustibleTarifaOrigen = 0;
        } else if (this.ruta.idCombustibleTarifaDestino == null) {
            this.ruta.idCombustibleTarifaDestino = 0;
        }
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idCombustibleTarifaOrigen: +this.ruta.idCombustibleTarifaOrigen,
            idCombustibleTarifaDestino: +this.ruta.idCombustibleTarifaDestino
        }
        let json = JSON.stringify(param);
        this._service.postAPI(this.urlApiTarifas, 'getCombustibleTarifasAutocomplete', json).subscribe(
            result => {
                this.tarifas = result;
            },
            error => {
                console.log("error: " + error);
            }
        );
    }
    //////////////////////////////ACCIONES//////////////////////////////////////////
    agregarCostoVariable() {
        this.costosVariables.push({ id: '', monto: null, tipo: 'variable' });
    }

    agregarCostoFijo() {
        this.costosFijos.push({ id: '', monto: null, tipo: 'fijo' });
    }

    agregarCostoOtro() {
        this.costosOtros.push({ id: '', monto: null, tipo: 'otro' });
    }

    quitarCostoVariable(index) {
        this.costosVariables.splice(index, 1);
        this.calcularCostoVariable();
    }

    quitarCostoFijo(index) {
        this.costosFijos.splice(index, 1);
        this.calcularCostoFijo();
    }

    quitarCostoOtro(index) {
        this.costosOtros.splice(index, 1);
        this.calcularCostoOtro();
    }

    regresar() {
        const dialogRef = this.dialog.open(ConfirmacionComponent, {
            width: '35%',
            disableClose: true,
            data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.router.navigate(['pages/cotizador']);
            }
        });
    }

    //*****CALCULOS DE COSTOS********
    calcularCostoVariable() {
        this.totalVariables = this.costosVariables.map(t => +parseFloat(t.monto.toString().replace(",", ""))).reduce((acc, value) => acc + value, 0);
        this.calcularSubtotal();
    }

    calcularCostoFijo() {
        this.totalFijos = this.costosFijos.map(t => +parseFloat(t.monto.toString().replace(",", ""))).reduce((acc, value) => acc + value, 0);
        this.calcularSubtotal();
    }

    calcularCostoOtro() {
        this.totalOtros = this.costosOtros.map(t => +parseFloat(t.monto.toString().replace(",", ""))).reduce((acc, value) => acc + value, 0);
        this.calcularSubtotal();
    }

    calcularSubtotal() {
        //Valida si existe cantidad en frecuencia
        this.subtotal = this.totalVariables + this.totalFijos + this.totalOtros;
        if (+this.formFrecuencia.get("formCantidad").value > 0) {
            this.subtotal = this.subtotal * +this.formFrecuencia.get("formCantidad").value;
        }
        console.log(+this.formFrecuencia.get("formCantidad").value);
        this.calcularTotal();
    }

    calcularDescuento() {
        this.totalDescuento = this.subtotal - +this.descuento;
        this.calcularTotal();
    }

    calcularTotal() {
        this.total = this.subtotal - +this.descuento - +this.ajuste;
        this.calcularTarifa();
    }

    calcularTarifa() {
        let u = (this.total * this.utilidad) / 100;
        this.tarifa = this.total + +u;
        if (this.ruta) {
            if (this.ruta.kms) {
                this.gastoCalculadoKm = +this.total / +this.ruta.kms;
                this.tarifaKm = +this.tarifa / +this.ruta.kms;
                if (+this.formEncabezado.get("formCantidadUnidadMedida").value > 0) {
                    this.gastoCalculadoUM = +this.total / +this.formEncabezado.get("formCantidadUnidadMedida").value;
                    this.tarifaUM = +this.tarifa / +this.formEncabezado.get("formCantidadUnidadMedida").value;
                } else {
                    this.gastoCalculadoUM = 0;
                    this.tarifaUM = 0;
                }
            }
        }
        this.formatoCantidades();
    }

    formatoCantidades() {
        this.totalVariablesMask = this.totalVariables.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.totalFijosMask = this.totalFijos.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.totalOtrosMask = this.totalOtros.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.subtotalMask = this.subtotal.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.totalDescuentoMask = this.totalDescuento.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.totalMask = this.total.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.tarifaMask = this.tarifa.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.gastoCalculadoKmMask = this.gastoCalculadoKm.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.tarifaKmMask = this.tarifaKm.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.gastoCalculadoUMMask = this.gastoCalculadoUM.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        this.tarifaUMMask = this.tarifaUM.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        //Costos Fijos, Variables y Otros
        /*for (let cv of this.costosVariables) {
            cv.monto = cv.monto.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        }
        for (let cf of this.costosFijos) {
            cf.monto = cf.monto.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        }
        for (let co of this.costosOtros) {
            co.monto = co.monto.toLocaleString('en-us', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        }*/
    }

    consultarCotizacion() {
        this.settings.loadingSpinner = true;

        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idCotizacion: this.idCotizacion
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlCotizador, 'getCotizacionById', json).subscribe(
            result => {
                let data = result;
                if (data.idCotizacionEstatus == 1) {
                    this.edicion = true;
                }
                this.setFormCotizacion(data);
            },
            error => {
                this.isLoadEdition = false;
                this.settings.loadingSpinner = false;
                console.log("error: " + error);
            }
        );
    }

    setFormCotizacion(data) {
        //Estatus de Cotización
        this.estatusCotizacion = data.descEstatus;
        this.idCotizacionEstatus = +data.idCotizacionEstatus;
        //ENCABEZADO
        this.formEncabezado.get("formCliente").setValue(+data.idCliente);
        this.formEncabezado.get("formClienteOtro").setValue(data.nombreCliente);
        this.formEncabezado.get("formTipoOperacion").setValue(+data.idTipoOperacion);
        this.formEncabezado.get("formTipoServicio").setValue(+data.idTipoServicio);
        this.formEncabezado.get("formFolio").setValue(data.folio);
        this.formEncabezado.get("formEmpresa").setValue(+data.idEmpresa);
        this.formEncabezado.get("formSucursales").setValue(+data.idArea);
        this.formEncabezado.get("formMoneda").setValue(+data.idMoneda);
        this.formEncabezado.get("formTipoCambio").setValue(+data.tipoCambio);
        //FRECUENCIA
        this.formFrecuencia.get("formTipoFrecuencia").setValue(+data.idFrecuenciasServicio);
        this.formFrecuencia.get("formCantidad").setValue(+data.cantidadFrecuencia);
        this.formFrecuencia.get("formTipoCobro").setValue(+data.idTipoCobro);
        this.formFrecuencia.get("formUnidadMedida").setValue(+data.idUnidadMedida);
        //RUTA
        if (+data.idPlazaOrigen > 0) {
            this.ruta.idPlazaOrigen = data.idPlazaOrigen;
        }
        this.ruta.origenDireccionSegmento = data.origenDireccionSegmento;
        this.ruta.idSegmentoOrigen = data.origenIdSegmentoINEGI;
        this.ruta.idSegmentoOrigenLat = data.origenPosLat;
        this.ruta.idSegmentoOrigenLon = data.origenPosLon;
        if (+data.idPlazaDestino > 0) {
            this.ruta.idPlazaDestino = data.idPlazaDestino;
        }
        this.ruta.destinoDireccionSegmento = data.destinoDireccionSegmento;
        this.ruta.idSegmentoDestino = data.destinoIdSegmentoINEGI;
        this.ruta.idSegmentoDestinoLat = data.destinoPosLat;
        this.ruta.idSegmentoDestinoLon = data.destinoPosLon;
        this.ruta.idRutaTipo = data.idRutaTipo;
        this.ruta.idRutaCondicionOrigen = data.idRutaCondicionOrigen;
        if (+data.idRuta > 0) {
            this.ruta.idRuta = data.idRuta;
        }
        this.ruta.kms = data.rutaKms;
        this.ruta.rendimiento = +data.rendimiento;
        this.ruta.idCombustibleTarifa = +data.idCombustibleTarifa;
        this.ruta.idTipoCarretera = data.idTipoCarretera;
        this.ruta.idVehiculoArmado = +data.idVehiculoArmado;//this.formEncabezado.get("formTipoVehiculo").setValue(+data.idVehiculoArmado);
        this.rutaCalculada.rendimiento = data.rutaRendimientoEstimado;
        this.rutaCalculada.tiempoEstimado = data.rutaTiempoEsperado;
        this.rutaCalculada.costoCombustibleInfo = data.rutaCostoCombustible;
        this.rutaCalculada.advertencias = data.rutaAdvertencias;
        //Carga la Configuración de la Empresa
        this.empresaConfiguracion.showCombustible = false;
        this.empresaConfiguracion.showCasetas = false;
        this.ruta.tipoCalculoCombustible = +data.tipoCalculoCombustible;
        this.ruta.tipoCalculoCombustibleTarifa = +data.tipoCalculoCombustibleTarifa;
        this.ruta.tipoCalculoCombustibleIdCostoTipo = +data.tipoCalculoCombustibleIdCostoTipo;
        this.ruta.tipoCalculoCasetas = +data.tipoCalculoCasetas;
        this.ruta.tipoCalculoCasetasTarifa = +data.tipoCalculoCasetasTarifa;
        this.ruta.tipoCalculoCasetasIdCostoTipo = +data.tipoCalculoCasetasIdCostoTipo;
        this.ruta.tipoCalculoSueldo = +data.tipoCalculoSueldo;
        this.ruta.tipoCalculoSueldoTarifa = +data.tipoCalculoSueldoTarifa;
        this.ruta.tipoCalculoSueldoIdCostoTipo = +data.tipoCalculoSueldoIdCostoTipo;

        this.formEncabezado.get("formUnidadMedidaCarga").setValue(+data.unidadMedidaCarga);
        this.formEncabezado.get("formCantidadUnidadMedida").setValue(+data.cantidadCarga);
        this.formEncabezado.get("formProductos").setValue(+data.idProducto);
        this.formEncabezado.get("formProductoOtro").setValue(data.nombreProducto);

        this.isSetDireccion = false;
        this.ruta.segmentos = data.segmentos;
        this.buildFormSegmentos();
        //COSTOS
        for (let c of data.costos) {
            if (c.tipo === 'variable') {
                this.costosVariables.push(c);
            }
            if (c.tipo === 'fijo') {
                this.costosFijos.push(c);
            }
            if (c.tipo === 'otro') {
                this.costosOtros.push(c);
            }
        }
        this.calcularCostoVariable();
        this.calcularCostoFijo(); 
        this.descuento = +data.descuento;
        this.calcularDescuento();
        this.ajuste = +data.ajuste;
        this.calcularTotal();
        this.utilidad = +data.utilidad;
        this.calcularTarifa();
        this.observaciones = data.observaciones;
        setTimeout(() => {
            this.isLoadEdition = false;
            this.calcularRuta()
            //this.setRoutingControls();
        }, 500);
        this.settings.loadingSpinner = false;
    }

    guardar() {
        if (this.formEncabezado.valid && this.formRutas.valid
            && this.formSegmentos.valid && this.formFrecuencia.valid) {
            //Valida que solo se puedan afectar registros nuevos o en estado de edición
            if (this.idCotizacionEstatus > 1) {
                this.openSnackBar("Solo pueden ser guardadas Cotizaciones en estatus de Nuevo o Edición.", "");
            } else {
                this.setCotizacion();
            }
        } else {
            Object.keys(this.formEncabezado.controls).forEach(key => {
                const ctrl = this.formEncabezado.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            Object.keys(this.formRutas.controls).forEach(key => {
                const ctrl = this.formRutas.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            Object.keys(this.formFrecuencia.controls).forEach(key => {
                const ctrl = this.formFrecuencia.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            for (let control of this.formSegmentos.controls as any) {
                Object.keys(control.controls).forEach(key => {
                    const ctrl = control.get(key);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }
            this.openSnackBar("Favor de validar y complementar los campos requeridos.", "");
        }  
    }


    //Función de ingreso/actualización de cotización (API)
    setCotizacion() {
        let arrCostos = [];
        let defVar: any = 0;
        for (let cv of this.costosVariables) {
            cv.idEmpresa = +sessionStorage.getItem("idEmpresa");
            cv.monto = +parseFloat(cv.monto.toString().replace(",", "")); //+cv.monto;
        }
        for (let cf of this.costosFijos) {
            cf.idEmpresa = +sessionStorage.getItem("idEmpresa");
            cf.monto = +parseFloat(cf.monto.toString().replace(",", ""));  //+cf.monto;
        }
        for (let co of this.costosOtros) {
            co.idEmpresa = +sessionStorage.getItem("idEmpresa");
            co.monto = +parseFloat(co.monto.toString().replace(",", "")); //+co.monto;
        }
        arrCostos.push.apply(arrCostos, this.costosVariables);
        arrCostos.push.apply(arrCostos, this.costosFijos);
        arrCostos.push.apply(arrCostos, this.costosOtros);
        let cotizacion = {
            idCotizacion: this.idCotizacion,
            folio: this.idCotizacion == 0 ? "" : this.formEncabezado.get("formFolio").value,
            idCotizacionEstatus: 1,
            idCliente: +this.formEncabezado.get("formCliente").value,
            nombreCliente: this.formEncabezado.get("formClienteOtro").value,
            idTipoOperacion: +this.formEncabezado.get("formTipoOperacion").value,
            idTipoServicio: +this.formEncabezado.get("formTipoServicio").value,
            idEmpresa: +this.formEncabezado.get("formEmpresa").value,
            idArea: +this.formEncabezado.get("formSucursales").value,
            idVehiculoArmado: +this.ruta.idVehiculoArmado, //+this.formEncabezado.get("formTipoVehiculo").value,
            idMoneda: +this.formEncabezado.get("formMoneda").value,
            tipoCambio: +this.formEncabezado.get("formTipoCambio").value,
            idPlazaOrigen: (this.ruta.idPlazaOrigen) ? this.ruta.idPlazaOrigen: 0,
            origenDireccionSegmento: this.ruta.origenDireccionSegmento,
            origenPosLat: this.ruta.idSegmentoOrigenLat,
            origenPosLon: this.ruta.idSegmentoOrigenLon,
            origenIdSegmentoINEGI: this.ruta.idSegmentoOrigen,
            idPlazaDestino: (this.ruta.idPlazaDestino) ? this.ruta.idPlazaDestino : 0,
            destinoDireccionSegmento: this.ruta.destinoDireccionSegmento,
            destinoPosLat: this.ruta.idSegmentoDestinoLat,
            destinoPosLon: this.ruta.idSegmentoDestinoLon,
            destinoIdSegmentoINEGI: this.ruta.idSegmentoDestino,
            idRutaTipo: this.ruta.idRutaTipo,
            idRutaCondicionOrigen: this.ruta.idRutaCondicionOrigen,
            idRuta: (this.ruta.idRuta) ? this.ruta.idRuta : 0,
            rutaKms: this.ruta.kms,
            rendimiento: this.ruta.rendimiento,
            idCombustibleTarifa: this.ruta.idCombustibleTarifa,
            idTipoCarretera: this.ruta.idTipoCarretera,
            rutaRendimientoEstimado: this.rutaCalculada.rendimiento,
            rutaTiempoEsperado: this.rutaCalculada.tiempoEstimado,
            rutaCostoCombustible: this.rutaCalculada.costoCombustibleInfo,
            rutaAdvertencias: this.rutaCalculada.advertencias,
            idFrecuenciasServicio: +this.formFrecuencia.get("formTipoFrecuencia").value,
            idTipoCobro: +this.formFrecuencia.get("formTipoCobro").value,
            idUnidadMedida: +this.formFrecuencia.get("formUnidadMedida").value,
            cantidadFrecuencia: +this.formFrecuencia.get("formCantidad").value,
            segmentos: this.ruta.segmentos,
            subtotal: +this.subtotal,
            descuento: +this.descuento,
            ajuste: +this.ajuste,
            utilidad: +this.utilidad,
            costos: arrCostos,
            totalVariables: +this.totalVariables,
            totalFijos: +this.totalFijos,
            totalOtros: +this.totalOtros,
            fecha: new Date(),
            tipoCalculoCombustible: this.ruta.tipoCalculoCombustible,
            tipoCalculoCombustibleTarifa: this.ruta.tipoCalculoCombustibleTarifa,
            tipoCalculoCombustibleIdCostoTipo: this.ruta.tipoCalculoCombustibleIdCostoTipo,
            tipoCalculoCasetas: this.ruta.tipoCalculoCasetas,
            tipoCalculoCasetasTarifa: this.ruta.tipoCalculoCasetasTarifa,
            tipoCalculoCasetasIdCostoTipo: this.ruta.tipoCalculoCasetasIdCostoTipo,
            tipoCalculoSueldo: this.ruta.tipoCalculoSueldo,
            tipoCalculoSueldoTarifa: this.ruta.tipoCalculoSueldoTarifa,
            tipoCalculoSueldoIdCostoTipo: this.ruta.tipoCalculoSueldoIdCostoTipo,
            unidadMedidaCarga: +this.formEncabezado.get("formUnidadMedidaCarga").value,
            cantidadCarga: +this.formEncabezado.get("formCantidadUnidadMedida").value,
            idProducto: +this.formEncabezado.get("formProductos").value,
            nombreProducto: this.formEncabezado.get("formProductoOtro").value,
            rendimientoUrea: this.ruta.rendimientoUrea,
            idUreaTarifa: this.ruta.idUreaTarifa,
            observaciones: this.observaciones
        }
        this.metodoPost(JSON.stringify(cotizacion), "setCotizacion");
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlCotizador, funcName, jsonParams).subscribe(
            result => {
                this.idCotizacion = +result.idCotizacion;
                if (result.idCotizacionEstatus == 1) {
                    this.edicion = true;
                    this.idCotizacionEstatus = 1;
                    this.estatusCotizacion = "Edición";
                }
                this.formEncabezado.get("formFolio").setValue(result.folio);
                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log("error: " + error.error.Message);
                this.openSnackBar("Error: " + error.error.Message, "");
            }
        );
    }

    verificarEstatus() {
        if (this.idCotizacion != 0 && this.edicion) {
            this.abrirModalContrato();
        }
    }

    abrirModalContrato() {
        const dialogRef = this.dialog.open(GenerarContratoComponent, {
            width: '35%',
            disableClose: true,
            data: { idCotizacion: this.idCotizacion }
        });

        dialogRef.afterClosed().subscribe(result => {
            
        });
    }

    rechazarCotizacion() {
        if (this.idCotizacion != 0 && this.edicion) {
            this.abrirModalRechazarCotizacion();
        }
    }

    abrirModalRechazarCotizacion() {
        const dialogRef = this.dialog.open(RechazarCotizacionComponent, {
            width: '35%',
            disableClose: true,
            data: { idCotizacion: this.idCotizacion }
        });

        dialogRef.afterClosed().subscribe(result => {

        });
    }

    abrirModalMapa() {
        const dialogRef = this.dialog.open(MapModalComponent, {
            width: '80%',
            minHeight: '80vh',
            disableClose: false,
            data: {
                ruta: this.ruta, rutaCalculada: this.rutaCalculada,
                origenLat: this.origenLat, origenLon: this.origenLon,
                destinoLat: this.destinoLat, destinoLon: this.destinoLon,
                isSetMapView: this.isSetMapView,
                isSetRoutingControls: this.isSetRoutingControls
            }
        });

        dialogRef.afterClosed().subscribe(
            result => {
                this.calcularKmTotal();
            }
        );
    }

    nuevo() {
        //this.vehiculo = new VehiculoModel();
        //this.router.navigate(['pages/vehiculos/vehiculo', 'N']);
        //this.router.navigate(['pages/cotizador/cotizacion', 'N']);
    }

    habilitaRutaPlazasReferencias(boolHabilitaRuta: boolean, boolHabilitaPlazas: boolean, boolHabilitaReferencias: boolean) {
        //Habilita / deshabilita campos de Ruta
        const controlRuta = this.formRutas.get("formRuta");
        if (boolHabilitaRuta) {
            controlRuta.enable();
        } else {
            controlRuta.disable();
        }
        //Habilita / deshabilita campos de Plazas
        const controlOrigen = this.formRutas.get("formDestino");
        const controlDestino = this.formRutas.get("formOrigen");
        if (boolHabilitaPlazas) {
            controlOrigen.enable();
            controlDestino.enable();
        } else {
            controlOrigen.disable();
            controlDestino.disable();
        }
        //Habilita / deshabilita campos de Referencia
        const controlRefOrigen = this.formRutas.get("formOrigenDireccion");
        const controlRefDestino = this.formRutas.get("formDestinoDireccion");
        if (boolHabilitaReferencias) {
            controlRefOrigen.enable();
            controlRefDestino.enable();
        } else {
            controlRefOrigen.disable();
            controlRefDestino.disable();
        }
    }

    actualizaVehiculos() {
        console.log("ActualizaVehiculos");
        this.consultarTiposVehiculoFiltrado();
        this.calcularSubtotal();
    }
}

export class RutaModel {
    idEmpresa: number;
    idRuta: number;
    idPlazaOrigen: string;
    origenDireccionSegmento: string;
    idSegmentoOrigen: number;
    idSegmentoOrigenLat: number;
    idSegmentoOrigenLon: number;
    idPlazaDestino: string;
    destinoDireccionSegmento: string;
    idSegmentoDestino: number;
    idSegmentoDestinoLat: number;
    idSegmentoDestinoLon: number;
    idRutaTipo: number;
    kms: number;
    rendimiento: number;
    idVehiculoArmado: number;
    idRutaCondicionOrigen: number;
    idTipoCarretera: number;
    idCombustibleTarifa: number;
    idCombustibleTarifaOrigen: number;
    idCombustibleTarifaDestino: number;
    //Configuración de Costos Variables 
    tipoCalculoCombustible: number;
    tipoCalculoCombustibleTarifa: number;
    tipoCalculoCombustibleIdCostoTipo: number;
    tipoCalculoCasetas: number;
    tipoCalculoCasetasTarifa: number;
    tipoCalculoCasetasIdCostoTipo: number;
    tipoCalculoSueldo: number;
    tipoCalculoSueldoTarifa: number;
    tipoCalculoSueldoIdCostoTipo: number;
    segmentos: SegmentoModel[];
    ejesExcedentes: number;
    idUreaTarifa: number;
    rendimientoUrea: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.segmentos = new Array<SegmentoModel>();
        this.idPlazaOrigen = "0";
        this.idPlazaDestino = "0";
        this.idSegmentoOrigen = 0;
        this.idSegmentoDestino = 0;
        this.ejesExcedentes = 0;
        this.idUreaTarifa = 0;
        this.rendimientoUrea = 0;
    }
}

export class SegmentoModel {
    idEmpresa: number;
    idRuta: number;
    direccionSegmento: string;
    idRutaTipoParada: number;
    idRutaMovimiento: number;
    posLat: number;
    lat: number;
    posLon: number;
    lon: number;
    filtro: Observable<any[]>;
    isLoading: boolean;
    idSegmentoINEGI: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.direccionSegmento = "";
    }
}

export class DireccionReferenciaModel {
    idEmpresa: number;
    direccion: string;
    id: number;
    lat: number;
    lon: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.direccion = "";
        this.id = 0;
        this.lat = 0;
        this.lon = 0;
    }
}

export class EmpresaConfigModel {
    tipoCalculoCombustible: number;
    tipoCalculoCombustibleTarifa: number;
    tipoCalculoCombustibleIdCostoTipo: number;
    tipoCalculoCasetas: number;
    tipoCalculoCasetasTarifa: number;
    tipoCalculoCasetasIdCostoTipo: number;
    tipoCalculoSueldo: number;
    tipoCalculoSueldoTarifa: number;
    tipoCalculoSueldoIdCostoTipo: number;

    constructor() {
        this.tipoCalculoCombustible = 0;
        this.tipoCalculoCombustibleTarifa = 0;
        this.tipoCalculoCombustibleIdCostoTipo = 0;
        this.tipoCalculoCasetas = 0;
        this.tipoCalculoCasetasTarifa = 0;
        this.tipoCalculoCasetasIdCostoTipo = 0;
        this.tipoCalculoSueldo = 0;
        this.tipoCalculoSueldoTarifa = 0;
        this.tipoCalculoSueldoIdCostoTipo = 0;
    }
}