import { Component, Inject, OnInit } from '@angular/core';
import "leaflet-map";
import "style-loader!leaflet/dist/leaflet.css";
import 'leaflet-routing-machine';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
declare var L: any; 

@Component({
  selector: 'app-map-modal',
  templateUrl: './map-modal.component.html',
  styleUrls: ['./map-modal.component.scss']
})
export class MapModalComponent implements OnInit {

    ruta: any = {};
    rutaCalculada: any = {};

    latitude: number = 25.6801;
    longitude: number = -100.2988;
    map: any;
    layerGroup: any;
    routingControl = null;

    constructor(public dialogRef: MatDialogRef<MapModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any,) {
        this.ruta = this.data.ruta;
        this.rutaCalculada = this.data.rutaCalculada;
    }

    ngOnInit() {
        //Inicializa MAPA, utiliza leafletjs por el template para la interacción con mapas (OPENSTREETMAPS). Doc: https://leafletjs.com/reference-1.7.1.html#map-example
        let el = document.getElementById("modal-leaflet-map");
        L.Icon.Default.imagePath = 'assets/img/vendor/leaflet';
        this.map = L.map(el, { scrollWheelZoom: false }).setView([this.latitude, this.longitude], 15);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        /*this.map.getInteractions().forEach(function (interaction) {
            if (interaction instanceof L.interaction.MouseWheelZoom) {
                interaction.setActive(false);
            }
        }, this);*/

        this.layerGroup = L.layerGroup().addTo(this.map);

        if (this.data.isSetRoutingControls) {
            this.setRoutingControls();  
        }
        if (this.data.isSetMapView) {
            this.setMapView();
        }
    }

    setRoutingControls() {
        let that = this;
        this.layerGroup.clearLayers();
        let points = [];
        if (this.rutaCalculada.coordenadas) {
            for (let c of this.rutaCalculada.coordenadas) { //Se agrega el LatLon de cada segmento
                if (c.lat && c.lon) {
                    points.push(L.latLng(c.lat, c.lon));
                }
            }
        }

        if (this.routingControl != null) {
            this.map.removeControl(this.routingControl);
        }

        this.routingControl = L.Routing.control({
            createMarker: function (i: number, waypoint: any, n: number) {
                let marker = L.marker(waypoint.latLng, { draggable: true });
                marker.on('dragend', function (e) {
                    if (i == 0) { //Es el marker de origen
                        that.ruta.idSegmentoOrigenLat = e.target._latlng.lat;
                        that.ruta.idSegmentoOrigenLon = e.target._latlng.lng;
                    } else if (i == n - 1) { //Es el marker de destino
                        that.ruta.idSegmentoDestinoLat = e.target._latlng.lat;
                        that.ruta.idSegmentoDestinoLon = e.target._latlng.lng;
                    } else { //Son los marker de cada segmento
                        that.ruta.segmentos[i - 1].posLat = e.target._latlng.lat;
                        that.ruta.segmentos[i - 1].posLon = e.target._latlng.lng;
                    }
                });

                return marker;
            },
            waypoints: points,
            lineOptions: {
                addWaypoints: false
            },
            altLineOptions: {
                addWaypoints: false,
                styles: { color: 'red', opacity: 0.0, weight: 0 },
            },
            show: false
        }).addTo(this.map);

        //this.settings.loadingSpinner = false;
    }

    setMapView() {
        this.layerGroup.clearLayers();
        let origen = false;
        let destino = false;

        if (this.data.origenLat && this.data.origenLon) {
            let markerO = L.marker([this.data.origenLat, this.data.origenLon], { title: "Origen" }).addTo(this.layerGroup);
            origen = true;
        }
        for (let s of this.ruta.segmentos) { //Se agrega el LatLon de cada segmento
            let markerS = L.marker([s.posLat, s.posLon], { title: s.direccionSegmento, draggable: true }).addTo(this.layerGroup)
            markerS.on('dragend', function (e) {
                s.posLat = e.target._latlng.lat;
                s.posLon = e.target._latlng.lng;
            })
        }
        if (this.data.destinoLat && this.data.destinoLon) {
            let markerD = L.marker([this.data.destinoLat, this.data.destinoLon], { title: "Destino" }).addTo(this.layerGroup);
            destino = true;
        }

        if (origen) {
            this.map.setView(new L.LatLng(this.data.origenLat, this.data.origenLon), 10);
        }
    }

    cerrarModal() {
        this.dialogRef.close();
    }
}
