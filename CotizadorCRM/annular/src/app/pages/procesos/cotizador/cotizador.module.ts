import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CotizadorComponent } from './cotizador.component';
import { CotizacionComponent } from './cotizacion/cotizacion.component';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { GenerarContratoModule } from '../../_modales/generar-contrato/generar-contrato.module';
import { ConfirmarConfiguracionModule } from '../../_modales/confirmar-configuracion/confirmar-configuracion.module';
import { RechazarCotizacionModule } from '../../_modales/rechazar-cotizacion/rechazar-cotizacion.module';
import { MapModalComponent } from './cotizacion/map-modal/map-modal.component';

export const routes = [
  { path: '', component: CotizadorComponent, pathMatch: 'full' },
  { path: 'cotizacion/:id', component: CotizacionComponent, data: {breadcrumb: 'Cotizacion'} }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        GenerarContratoModule,
        ConfirmarConfiguracionModule,
        RechazarCotizacionModule,
        NgSelectModule
    ],
    declarations: [
        CotizadorComponent,
        CotizacionComponent,
        MapModalComponent
    ],
    providers: [
        fcGeneral
    ],
    entryComponents: [
        MapModalComponent
    ]
})
export class CotizadorModule { }