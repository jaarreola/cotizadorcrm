import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { AppSettings } from '../../../app.settings';
import { Settings } from '../../../app.settings.model';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';
import * as FileSaver from 'file-saver';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-cotizador',
  templateUrl: './cotizador.component.html',
  styleUrls: ['./cotizador.component.scss']
})
export class CotizadorComponent implements OnInit {
    private urlApiCommon = 'api/Common';
    private urlApiCliente = 'api/Cliente';
    private urlApiPersonal = 'api/Personal';
    private urlApiCotizador = 'api/Cotizador';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
    'check',
    'cotizacion',
    'cliente',
    'fecha',
    'estatus',
    'costo_variable',
    'costo_fijo',
    'otros_costos',
    'tarifa'
    ];

    config = new MatSnackBarConfig();
    public settings: Settings;

    clientes: any[] = [];
    agentes: any[] = [];
    estatus: any[] = [];

    cotizaciones: any[] = [];
     
    hoy = new Date();
    fechaInicio = new Date(new Date().setDate(this.hoy.getDate() - 7))
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idCliente: null,
        fechaInicio: this.fechaInicio,
        fechaFin: new Date(),
        folio: '',
        idCotizacionEstatus: 0,
        idPersonal: null
    }
    filtros = true;

    selection = new SelectionModel<any>(true, []);
    constructor(private router: Router, public appSettings: AppSettings, private _service: fcGeneral, private _snackBar: MatSnackBar) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
        this.consultarClientes();
        this.consultarAgentes();
        this.consultarEstatusCotizacion();

        this.consultarCotizaciones();
    }

    consultarClientes() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiCliente, 'getClientesListaAutocomplete', json).subscribe(
            result => {
                this.clientes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarAgentes() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiPersonal, 'getPersonalListaAutocomplete', json).subscribe(
            result => {
                this.agentes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarEstatusCotizacion() {
        this._service.getAPI(this.urlApiCommon, 'getCotizacionEstatusListado').subscribe(
            result => {
                this.estatus = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarCotizaciones() {
        //Valida datos de filtros
        var paramsFiltro = JSON.parse(JSON.stringify(this.filtro));
        if (paramsFiltro.idCliente == null) {
            paramsFiltro.idCliente = 0;
        }
        if (paramsFiltro.idPersonal == null) {
            paramsFiltro.idPersonal = 0;
        }
        if (paramsFiltro.folio == '') {
            paramsFiltro.folio = 0;
        } else {
            paramsFiltro.folio = +paramsFiltro.folio;
        }
        let json = JSON.stringify(paramsFiltro);
        console.log(json);
        this._service.postAPI(this.urlApiCotizador, 'getCotizacionesLista', json).subscribe(
            result => {
                this.cotizaciones = result;
                this.dataSource = new MatTableDataSource(this.cotizaciones);
                this.dataSource.paginator = this.paginator;
            },
            error => {
                console.log(error);
            }
        );
    }

    nuevo() {
        this.router.navigate(['pages/cotizador/cotizacion', 'N']);
    }

    editar(cotizacion) {
        this.router.navigate(['pages/cotizador/cotizacion', cotizacion.idCotizacion]);
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idCliente: null,
            fechaInicio: this.fechaInicio,
            fechaFin: new Date(),
            folio: '',
            idCotizacionEstatus: 0,
            idPersonal: null
        }
    }

    imprimir() {
        if (!this.validarMismoCliente(this.selection.selected)) {
            this._snackBar.open('No se puede imprimir el archivo porque las cotizaciones seleccionadas no pertenecen al mismo cliente', '', this.config);
            return;
        }
        let idCotizaciones = String(this.selection.selected.map(cot => cot.idCotizacion));

        console.log(idCotizaciones);
        this._service.getAPI(this.urlApiCotizador, "imprimirCotizacion?idCotizaciones=" + idCotizaciones).subscribe(
            result => {
                this.dataURItoBlob(result, this.selection.selected[0]);
            },
            error => {
                this.settings.loadingSpinner = false;
                console.log(error);
            }
        );
    }

    validarMismoCliente(cotizaciones) {
        let mismoCliente = true;
        let cotizacion1ra = cotizaciones[0];

        for (let i = 1; i < cotizaciones.length; i++) {
            if (cotizaciones[i].idCliente != cotizacion1ra.idCliente) {
                mismoCliente = false;
            }
        }

        return mismoCliente;
    }

    dataURItoBlob(b64, row) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        let byteString;
        if (b64.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(b64.split(',')[1]);
        else
            byteString = unescape(b64.split(',')[1]);
        // separate out the mime component
        let mimeString = b64.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        let ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        let blob = new Blob([ia], { type: mimeString });
        FileSaver.saveAs(blob, 'Cotizaciones_' + Date.now.toString());
        this.settings.loadingSpinner = false;
    }

}
