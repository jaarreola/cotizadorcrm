import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ReporteComponent } from './reporte/reporte.component';
import { TareaComponent } from './tarea/tarea.component';

@Component({
  selector: 'app-seguimiento-clientes',
  templateUrl: './seguimiento-clientes.component.html',
  styleUrls: ['./seguimiento-clientes.component.scss']
})
export class SeguimientoClientesComponent implements OnInit {
    public dsCotizacion = new MatTableDataSource<any>();
    public dcCotizacion: string[] = [
        'acciones',
        'cotizacion',
        'cliente',
        'estimado',
        'tarifa',
        'ruta',
        'estatus',
        'agente',
        'ultimoContacto'
    ];

    public dsServicio = new MatTableDataSource<any>();
    public dcServicio: string[] = [
        'acciones',
        'reporte',
        'cliente',
        'causa',
        'quienReporta',
        'estatus',
        'responsable',
        'ultimoContacto'
    ];

    public dsTareas = new MatTableDataSource<any>();
    public dcTareas: string[] = [
        'acciones',
        'titulo',
        'descripcion',
        'vence',
        'etiqueta',
        'seguimiento',
        'estatus',
        'agente',
        'checklist'
    ];

    public dsContactos = new MatTableDataSource<any>();
    public dcContactos: string[] = [
        'empresa',
        'contacto',
        'correo',
        'departamento',
        'telefono',
        'ext',
        'estado',
        'agente',
        'estatus'
    ];

    obj = [{}, {}];
    constructor(private router: Router, public dialog: MatDialog) {
        this.dsTareas = new MatTableDataSource(this.obj);
    }

    ngOnInit() {
    }

    btnCotizacion() {
        this.router.navigate(['pages/cotizador/cotizacion']);
    }

    nuevoReporte() {
        const dialogRef = this.dialog.open(ReporteComponent, {
            width: '40%',
            disableClose: true
        });
    }

    nuevaTarea() {
        const dialogRef = this.dialog.open(TareaComponent, {
            width: '40%',
            disableClose: true
        });
    }

}
