import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.scss']
})
export class ReporteComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<ReporteComponent>) { }

    ngOnInit() {
    }

    cerrarModal() {
        this.dialogRef.close();
    }

}
