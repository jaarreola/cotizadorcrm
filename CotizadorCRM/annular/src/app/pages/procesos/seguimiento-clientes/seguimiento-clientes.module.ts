﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { fcGeneral } from '../../../services/general.service';
import { SeguimientoClientesComponent } from './seguimiento-clientes.component';
import { SharedModule } from '../../../shared/shared.module';;
import { ReporteComponent } from './reporte/reporte.component'
;
import { TareaComponent } from './tarea/tarea.component'


export const routes = [
    { path: '', component: SeguimientoClientesComponent, pathMatch: 'full' },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule
    ],
    declarations: [
        SeguimientoClientesComponent,
        ReporteComponent,
        TareaComponent
    ],
    providers: [
        fcGeneral
    ],
    entryComponents: [
        ReporteComponent,
        TareaComponent
    ]
})
export class SeguimientoClientesModule { }