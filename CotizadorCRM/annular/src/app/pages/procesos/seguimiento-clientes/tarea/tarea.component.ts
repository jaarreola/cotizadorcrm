import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.scss']
})
export class TareaComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<TareaComponent>) { }

    ngOnInit() {
    }

    cerrarModal() {
        this.dialogRef.close();
    }

}
