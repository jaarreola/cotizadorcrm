﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { SharedModule } from '../../../shared/shared.module';
import { ContratosComponent } from './contratos.component';;
import { ContratoComponent } from './contrato/contrato.component'


export const routes = [
    { path: '', component: ContratosComponent, pathMatch: 'full' },
    { path: 'contrato', component: ContratoComponent, data: { breadcrumb: 'Contrato' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule
    ],
    declarations: [
        ContratosComponent,
        ContratoComponent
    ],
    providers: [
    ]
})
export class ContratosModule { }