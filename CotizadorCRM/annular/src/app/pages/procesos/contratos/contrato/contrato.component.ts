import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.scss']
})
export class ContratoComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'acciones',
        'cotizacion',
        'fecha',
        'ruta',
        'estatus',
        'flete_cotizado',
        'costo_variable',
        'costo_fijo'
    ];
    constructor() { }

    ngOnInit() {
    }

}
