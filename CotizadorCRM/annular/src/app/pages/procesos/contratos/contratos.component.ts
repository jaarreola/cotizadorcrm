import { Component, OnInit } from '@angular/core';
import { Settings } from '../../../app.settings.model';
import { MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { AppSettings } from '../../../app.settings';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html',
  styleUrls: ['./contratos.component.scss']
})
export class ContratosComponent implements OnInit {
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'contrato',
        'cliente',
        'nombre',
        'fecha',
        'vigencia',
        'estatus',
        'usuario',
        'numero_rutas',
        'tarifa_promedio',
        'utilidad_promedio',
        'ultima_actualizacion',
        'ultima_revision'
    ];

    public settings: Settings;

    filtros = true;
    constructor(private router: Router, public appSettings: AppSettings) {
        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
    }

    nuevo() {
        this.router.navigate(['pages/contratos/contrato']);
    }

}
