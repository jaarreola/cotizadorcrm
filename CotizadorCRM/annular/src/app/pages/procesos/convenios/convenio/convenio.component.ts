import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { fcGeneral } from '../../../../services/general.service';
import { MatTableDataSource, MatSnackBarConfig, MatSnackBar, MatDialog } from '@angular/material';
import { ControlPosition } from '@agm/core/services/google-maps-types';
import { Settings } from '../../../../app.settings.model';
import { AppSettings } from '../../../../app.settings';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmacionComponent } from '../../../_modales/confirmacion/confirmacion.component';

@Component({
  selector: 'app-convenio',
  templateUrl: './convenio.component.html',
  styleUrls: ['./convenio.component.scss']
})
export class ConvenioComponent implements OnInit {
    private urlApiCommon = 'api/Common';
    private urlApiCliente = 'api/Cliente';
    private urlApiRutas = 'api/Rutas';
    private urlApiAreas = 'api/Areas';
    private urlApiEmpresa = 'api/Empresa';
    private urlVehiculos = 'api/Vehiculos';
    private urlApiPlaza = 'api/Plaza';
    private urlApiCotizador = 'api/Cotizador';
    private urlApiProductos = 'api/Productos';
    private urlApiConvenio = 'api/Convenio';

    public dsCobro = new MatTableDataSource<any>();
    public dcCobro: string[] = [
        'flete',
        'seguro',
        'maniobras',
        'autopistas',
        'otros',
        'subtotal',
        'iva',
        'retencion',
        'total'
    ];

    clientes: any[] = [];
    rutas: any[] = [];
    areas: any[] = [];
    empresas: any[] = [];
    tiposVehiculo: any[] = [];
    monedas: any[] = [];
    tiposOperacion: any[] = [];
    tiposServicios: any[] = [];
    plazas: any[] = [];
    tiposCobro: any[] = [];
    tiposPago: any[] = [];
    direcciones: any[] = [];
    productos: any[] = [];
    ivas: any[] = [];
    retenciones: any[] = [];

    tiposParada: any[] = [];
    tiposMovimiento: any[] = [];

    listaSegmentos = [];
    listaProductos = [];

    config = new MatSnackBarConfig();
    public settings: Settings;

    idConvenio: any;

    formEncabezado: FormGroup;
    formOperacion: FormGroup;
    formControl: FormGroup;
    formSegmentos: FormArray;
    formProductos: FormArray;
    formCobro: FormGroup;
    constructor(private _service: fcGeneral, public formBuilder: FormBuilder, private _snackBar: MatSnackBar, public appSettings: AppSettings, private route: ActivatedRoute, public dialog: MatDialog, private router: Router) {
        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';

        this.settings = this.appSettings.settings;
    }

    ngOnInit() {
        this.buildFormEncabezado();
        this.buildFormOperacion();
        this.buildFormControl();
        this.buildFormSegmentos();
        this.buildFormProductos();
        this.buildFormCobro();

        this.consultarClientes();
        this.consultarRutas();
        this.consultarAreas();
        this.consultarEmpresas();
        this.consultarTiposVehiculo();
        this.consultarMonedas();
        this.consultarTiposParada();
        this.consultarTiposMovimiento();
        this.consultarTiposOperacion();
        this.consultarTiposServicio();
        this.consultarPlazas();
        this.consultarTiposCobro();
        this.consultarTiposPago();
        this.consultarProductos();
        this.consultarIVA();
        this.consultarRetencionesListado();

        this.route.params.subscribe(params => {
            this.idConvenio = params['id'];

            if (this.idConvenio != "N") {
                this.idConvenio = +params['id'];
                this.consultarConvenio();
            } else {
                this.idConvenio = 0;
            }
        });
    }

    ///////////////////////FORMULARIO DE ENCABEZADO////////////////////////
    buildFormEncabezado() {
        this.formEncabezado = this.formBuilder.group({
            formCliente: this.formBuilder.control(null, [Validators.required]),
            formNombre: this.formBuilder.control(null, [Validators.required]),
            formRemitente: this.formBuilder.control(null, [Validators.required]),
            formDestinatario: this.formBuilder.control(null, [Validators.required]),
            formRuta: this.formBuilder.control(null, [Validators.required]),
            formKms: this.formBuilder.control(null, [Validators.required, Validators.pattern("\\d+(\\.\\d+)?")]),
            formFolio: this.formBuilder.control(null, []),
            formAreas: this.formBuilder.control(null, [Validators.required]),
            formEmpresa: this.formBuilder.control(null, [Validators.required]),
            formTipoVehiculo: this.formBuilder.control(null, [Validators.required]),
            formMoneda: this.formBuilder.control(null, [Validators.required]),
            formTipoCambio: this.formBuilder.control(null, [Validators.required])
        });

        const controlFolio = this.formEncabezado.get("formFolio");
        controlFolio.disable();
    }

    buildFormOperacion() {
        this.formOperacion = this.formBuilder.group({
            formTipoOperacion: this.formBuilder.control(null, [Validators.required]),
            formOrigen: this.formBuilder.control(null, [Validators.required]),
            formDestino: this.formBuilder.control(null, [Validators.required]),
            formTipoServicio: this.formBuilder.control(null, [Validators.required]),
            formValor: this.formBuilder.control(null, [Validators.required]),
            formTipoCobro: this.formBuilder.control(null, [Validators.required]),
            formTipoPago: this.formBuilder.control(null, [Validators.required])
        });
    }

    buildFormControl() {
        this.formControl = this.formBuilder.group({
            formRecoger: this.formBuilder.control(null, [Validators.required]),
            formEntregar: this.formBuilder.control(null, [Validators.required]),
            formPago: this.formBuilder.control(null, []),
            formOperador: this.formBuilder.control(null, []),
            formPermisionario: this.formBuilder.control(null, [])
        });

        const controlPago = this.formControl.get("formPago");
        const controlOperador = this.formControl.get("formOperador");
        const controlPermisionario = this.formControl.get("formPermisionario");

        controlOperador.disable();
        controlPermisionario.disable();
        controlPago.valueChanges.subscribe(pago => {
            if (!pago) {
                controlOperador.disable();
                controlPermisionario.disable();
            } else {
                controlOperador.enable();
                controlOperador.setValidators([Validators.required, Validators.pattern("\\d+(\\.\\d{1,2})?")]);

                controlPermisionario.enable();
                controlPermisionario.setValidators([Validators.required, Validators.pattern("\\d+(\\.\\d{1,2})?")]);
            }
        });

        let cambioOperador = true;
        let cambioPermisioanrio = true;
        controlOperador.valueChanges.subscribe(operador => {
            if (cambioOperador) {
                cambioPermisioanrio = false;
                controlPermisionario.setValue(0);
                controlPermisionario.updateValueAndValidity();
                cambioPermisioanrio = true;
            }
        });

        controlPermisionario.valueChanges.subscribe(permisionario => {
            if (cambioPermisioanrio) {
                cambioOperador = false;
                controlOperador.setValue(0);
                controlOperador.updateValueAndValidity();
                cambioOperador = true;
            }
        });
    }

    buildFormSegmentos() {
        let groups = this.listaSegmentos.map(segmento => {
            return new FormGroup({
                direccion: new FormControl(segmento.idPlaza, [Validators.required]),
                tipoParada: new FormControl(segmento.idRutaTipoParada, [Validators.required]),
                movimiento: new FormControl(segmento.idRutaMovimiento, [Validators.required])
            });
        });

        this.formSegmentos = new FormArray(groups);
    }
    agregarSegmento() {
        let segmento = new SegmentoModel();
        this.listaSegmentos.push(segmento);

        let group = new FormGroup({
            direccion: new FormControl(null, [Validators.required]),
            tipoParada: new FormControl(null, [Validators.required]),
            movimiento: new FormControl(null, [Validators.required])
        });

        this.formSegmentos.push(group);
    }
    getControlSegmento(index, campo) {
        return this.formSegmentos.at(index).get(campo) as FormControl;
    }

    buildFormProductos() {
        let groups = this.listaProductos.map(producto => {
            return new FormGroup({
                producto: new FormControl(null, [Validators.required]),
                descripcion: new FormControl(producto.idProducto, [Validators.required]),
                embalaje: new FormControl(null, [Validators.required]),
                cantidad: new FormControl(producto.cantidad, [Validators.required]),
                peso: new FormControl(producto.peso, [Validators.required]),
                volumen: new FormControl(producto.volumen, [Validators.required]),
                pesoEstimado: new FormControl(producto.pesoEstimado, [Validators.required]),
                importe: new FormControl(producto.importe, [Validators.required])
            });
        });
        for (let group of groups) {
            const controlProducto = group.get("producto");
            controlProducto.disable();
            const controlEmbalaje = group.get("embalaje");
            controlEmbalaje.disable();
        }
        this.formProductos = new FormArray(groups);
    }
    agregarProducto() {
        this.listaProductos.push(new Object);

        let group = new FormGroup({
            producto: new FormControl(null, [Validators.required]),
            descripcion: new FormControl(null, [Validators.required]),
            embalaje: new FormControl(null, [Validators.required]),
            cantidad: new FormControl(null, [Validators.required]),
            peso: new FormControl(null, [Validators.required]),
            volumen: new FormControl(null, [Validators.required]),
            pesoEstimado: new FormControl(null, [Validators.required]),
            importe: new FormControl(null, [Validators.required])
        });
        const controlProducto = group.get("producto");
        controlProducto.disable();
        const controlEmbalaje = group.get("embalaje");
        controlEmbalaje.disable();

        this.formProductos.push(group);
    }
    getControlProducto(index, campo) {
        return this.formProductos.at(index).get(campo) as FormControl;
    }
    seleccionarProducto(index) {
        const controlDescripcion = this.getControlProducto(index, 'descripcion');
        let p = this.productos.find(producto => producto.id === controlDescripcion.value);

        const controlProducto = this.getControlProducto(index, 'producto');
        const controlEmbalaje = this.getControlProducto(index, 'embalaje');
        if (p != undefined) {
            controlProducto.setValue(p.id);
            controlEmbalaje.setValue(p.nombreEmbalaje);
        } else {
            controlProducto.setValue(null);
            controlEmbalaje.setValue(null);
        }
    }

    buildFormCobro() {
        this.formCobro = this.formBuilder.group({
            formFlete: this.formBuilder.control(null, [Validators.required]),
            formSeguro: this.formBuilder.control(null, [Validators.required]),
            formManiobras: this.formBuilder.control(null, [Validators.required]),
            formAutopistas: this.formBuilder.control(null, [Validators.required]),
            formOtros: this.formBuilder.control(null, [Validators.required]),
            formSubtotal: this.formBuilder.control(null, [Validators.required]),
            formIva: this.formBuilder.control(null, [Validators.required]),
            formRetencion: this.formBuilder.control(null, [Validators.required]),
            formTotal: this.formBuilder.control(null, [Validators.required])
        });
    }

    consultarTiposParada() {
        this._service.getAPI(this.urlApiCommon, 'getRutaTiposParadaLista').subscribe(
            result => {
                this.tiposParada = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposMovimiento() {
        this._service.getAPI(this.urlApiCommon, 'getRutaMovimientosLista').subscribe(
            result => {
                this.tiposMovimiento = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarClientes() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiCliente, 'getClientesListaAutocomplete', json).subscribe(
            result => {
                this.clientes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarRutas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiRutas, 'getRutasListaAutocomplete', json).subscribe(
            result => {
                this.rutas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarAreas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiAreas, 'getAreasByEmpresaLista', json).subscribe(
            result => {
                this.areas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarEmpresas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiEmpresa, 'getEmpresaLista', json).subscribe(
            result => {
                this.empresas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposVehiculo() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlVehiculos, 'getVehiculoListaByEmpresaAreaModel', json).subscribe(
            result => {
                this.tiposVehiculo = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarMonedas() {
        this._service.getAPI(this.urlApiCommon, 'getMonedasListado').subscribe(
            result => {
                this.monedas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposOperacion() {
        this._service.getAPI(this.urlApiCommon, 'getClasificacionTipoOperacionListado').subscribe(
            result => {
                this.tiposOperacion = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposServicio() {
        this._service.getAPI(this.urlApiCommon, 'getTiposServiciosListado').subscribe(
            result => {
                this.tiposServicios = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarPlazas() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiPlaza, 'getPlazaListaByEmpresaArea', json).subscribe(
            result => {
                this.plazas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposCobro() {
        this._service.getAPI(this.urlApiCommon, 'getTipoCobroListado').subscribe(
            result => {
                this.tiposCobro = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarTiposPago() {
        this._service.getAPI(this.urlApiCommon, 'getTipoPagoListado').subscribe(
            result => {
                this.tiposPago = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarDirecciones() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa")
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiCotizador, 'getDireccionSegmento', json).subscribe(
            result => {
                this.direcciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarProductos() {
        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idArea: 1
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiProductos, 'getProductosAutocomplete', json).subscribe(
            result => {
                this.productos = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarIVA() {
        this._service.getAPI(this.urlApiCommon, 'getIVAListado').subscribe(
            result => {
                this.ivas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarRetencionesListado() {
        this._service.getAPI(this.urlApiCommon, 'getRetencionesListado').subscribe(
            result => {
                this.retenciones = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarConvenio() {
        this.settings.loadingSpinner = true;

        let param = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idConvenio: this.idConvenio
        }
        let json = JSON.stringify(param);

        this._service.postAPI(this.urlApiConvenio, 'getConvenioById', json).subscribe(
            result => {
                let data = result;
                this.setFormConvenio(data);
                console.log(data);
            },
            error => {
                console.log(error);
            }
        );
    }

    setFormConvenio(data) {
        this.formEncabezado.get("formCliente").setValue(+data.idCliente);
        this.formEncabezado.get("formNombre").setValue(data.nombre);
        this.formEncabezado.get("formRemitente").setValue(+data.idClienteRemitente);
        this.formEncabezado.get("formDestinatario").setValue(+data.idClienteDestinatario);
        this.formEncabezado.get("formFolio").setValue(+data.folio);
        this.formEncabezado.get("formAreas").setValue(+data.idArea);
        this.formEncabezado.get("formEmpresa").setValue(+data.idEmpresa);
        this.formEncabezado.get("formTipoVehiculo").setValue(+data.idVehiculoArmado);
        this.formEncabezado.get("formMoneda").setValue(+data.idMoneda);
        this.formEncabezado.get("formTipoCambio").setValue(+data.tipoCambio);
        this.formEncabezado.get("formRuta").setValue(+data.idRuta);
        this.formEncabezado.get("formKms").setValue(+data.rutaKms);
        //OPERACION
        this.formOperacion.get("formTipoOperacion").setValue(+data.idTipoOperacion);
        this.formOperacion.get("formOrigen").setValue(+data.idPlazaOrigen);
        this.formOperacion.get("formDestino").setValue(+data.idPlazaDestino);
        this.formOperacion.get("formTipoServicio").setValue(+data.idTipoServicio);
        this.formOperacion.get("formTipoCobro").setValue(+data.idTipoCobro);
        this.formOperacion.get("formTipoPago").setValue(+data.idTipoPago);
        this.formOperacion.get("formValor").setValue(+data.valorDeclarado);
        //////
        this.formControl.get("formRecoger").setValue(data.recogerEn);
        this.formControl.get("formEntregar").setValue(data.entregarEn);
        this.formControl.get("formPago").setValue(+data.controlarPago);
        this.formControl.get("formOperador").setValue(+data.valorOperador);
        this.formControl.get("formPermisionario").setValue(+data.valorPermisionario);
        //COBRO
        this.formCobro.get("formFlete").setValue(+data.flete);
        this.formCobro.get("formSeguro").setValue(+data.seguro);
        this.formCobro.get("formManiobras").setValue(+data.maniobras);
        this.formCobro.get("formAutopistas").setValue(+data.autopistas);
        this.formCobro.get("formOtros").setValue(+data.otros);
        this.formCobro.get("formSubtotal").setValue(+data.subtotal);
        this.formCobro.get("formTotal").setValue(+data.total);
        this.formCobro.get("formIva").setValue(+data.idImpuestoIva);
        this.formCobro.get("formRetencion").setValue(+data.idImpuestoRetencion);
        //
        this.listaSegmentos = data.segmentos;
        this.buildFormSegmentos();
        this.listaProductos = data.productos;
        this.buildFormProductos();
        this.listaProductos.forEach((s, index) => {
            this.seleccionarProducto(index);
        });

        this.settings.loadingSpinner = false;
    }

    guardar() {
        if (this.formEncabezado.valid && this.formOperacion.valid && this.formSegmentos.valid
            && this.formControl.valid && this.formProductos.valid && this.formCobro.valid) {
            this.setConvenio();
        } else {
            Object.keys(this.formEncabezado.controls).forEach(key => {
                const ctrl = this.formEncabezado.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            Object.keys(this.formOperacion.controls).forEach(key => {
                const ctrl = this.formOperacion.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            Object.keys(this.formControl.controls).forEach(key => {
                const ctrl = this.formControl.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });

            for (let control of this.formSegmentos.controls as any) {
                Object.keys(control.controls).forEach(key => {
                    const ctrl = control.get(key);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }

            for (let control of this.formProductos.controls as any) {
                Object.keys(control.controls).forEach(key => {
                    const ctrl = control.get(key);
                    ctrl.markAsTouched({ onlySelf: true });
                });
            }

            Object.keys(this.formCobro.controls).forEach(key => {
                const ctrl = this.formCobro.get(key);
                ctrl.markAsTouched({ onlySelf: true });
            });
        }
    }

    setConvenio() {
        let segmentos = [];
        this.listaSegmentos.forEach((s, index) => {
            let segmento = {
                idConvenio: +this.idConvenio,
                idEmpresa: +this.formEncabezado.get("formEmpresa").value,
                idPlaza: +this.getControlSegmento(index, "direccion").value,
                idRutaTipoParada: +this.getControlSegmento(index, "tipoParada").value,
                idRutaMovimiento: +this.getControlSegmento(index, "movimiento").value
            }
            segmentos.push(segmento);
        });

        let prods = [];
        this.listaProductos.forEach((p, index) => {
            let producto = {
                idConvenio: +this.idConvenio,
                idEmpresa: +this.formEncabezado.get("formEmpresa").value,
                idProducto: +this.getControlProducto(index, "producto").value,
                cantidad: +this.getControlProducto(index, "cantidad").value,
                peso: +this.getControlProducto(index, "peso").value,
                volumen: +this.getControlProducto(index, "volumen").value,
                pesoEstimado: +this.getControlProducto(index, "pesoEstimado").value,
                importe: +this.getControlProducto(index, "importe").value,
            }
            prods.push(producto);
        });

        let convenio = {
            //ENCABEZADO
            idConvenio: this.idConvenio,
            idEmpresa: +this.formEncabezado.get("formEmpresa").value,
            folio: this.idConvenio == 0 ? 0 : +this.formEncabezado.get("formFolio").value,
            idArea: +this.formEncabezado.get("formAreas").value,
            idCotizacion: 0,
            idCliente: +this.formEncabezado.get("formCliente").value,
            nombre: this.formEncabezado.get("formNombre").value,
            idClienteRemitente: +this.formEncabezado.get("formRemitente").value,
            idClienteDestinatario: +this.formEncabezado.get("formDestinatario").value,
            idRuta: +this.formEncabezado.get("formRuta").value,
            rutaKms: +this.formEncabezado.get("formKms").value,
            idVehiculoArmado: +this.formEncabezado.get("formTipoVehiculo").value,
            idMoneda: +this.formEncabezado.get("formMoneda").value,
            tipoCambio: +this.formEncabezado.get("formTipoCambio").value,
            idConvenioEstatus: 1,
            idUsuario: +sessionStorage.getItem("idUsuario"),
            //OPERACION
            idTipoOperacion: +this.formOperacion.get("formTipoOperacion").value,
            idPlazaOrigen: +this.formOperacion.get("formOrigen").value,
            idPlazaDestino: +this.formOperacion.get("formDestino").value,
            idTipoServicio: +this.formOperacion.get("formTipoServicio").value,
            valorDeclarado: +this.formOperacion.get("formValor").value,
            idTipoCobro: +this.formOperacion.get("formTipoCobro").value,
            idTipoPago: +this.formOperacion.get("formTipoPago").value,
            //
            recogerEn: this.formControl.get("formRecoger").value,
            entregarEn: this.formControl.get("formEntregar").value,
            controlarPago: +this.formControl.get("formPago").value,
            valorOperador: +this.formControl.get("formOperador").value,
            valorPermisionario: +this.formControl.get("formPermisionario").value,
            //COBRO
            flete: +this.formCobro.get("formFlete").value,
            seguro: +this.formCobro.get("formSeguro").value,
            maniobras: +this.formCobro.get("formManiobras").value,
            autopistas: +this.formCobro.get("formAutopistas").value,
            otros: +this.formCobro.get("formOtros").value,
            subtotal: +this.formCobro.get("formSubtotal").value,
            total: +this.formCobro.get("formTotal").value,
            idImpuestoIva: +this.formCobro.get("formIva").value,
            idImpuestoRetencion: +this.formCobro.get("formRetencion").value,
            //SEGMENTOS y PRODUCTOS
            segmentos: segmentos,
            productos: prods
        }

        this.metodoPost(JSON.stringify(convenio), "setConvenio");
    }

    metodoPost(jsonParams, funcName) {
        this._service.postAPI(this.urlApiConvenio, funcName, jsonParams).subscribe(
            result => {
                this.idConvenio = +result.idCotizacion;
                this.formEncabezado.get("formFolio").setValue(+result.folio);
                this._snackBar.open('Registro guardado con exito', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

    regresar() {
        const dialogRef = this.dialog.open(ConfirmacionComponent, {
            width: '35%',
            disableClose: true,
            data: { mensaje: '¿Está seguro de salir de la pantalla sin guardar los cambios?' }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.router.navigate(['pages/convenios']);
            }
        });
    }
}

export class RutaModel {
    segmentos: SegmentoModel[];

    constructor() {
        this.segmentos = new Array<SegmentoModel>();
    }
}

export class SegmentoModel {
    idEmpresa: number;
    idRuta: number;
    direccionSegmento: string;
    idRutaTipoParada: number;
    idRutaMovimiento: number;
    posLat: number;
    lat: number;
    posLon: number;
    lon: number;
    filtro: Observable<any[]>;
    isLoading: boolean;
    idSegmentoINEGI: number;

    constructor() {
        this.idEmpresa = +sessionStorage.getItem("idEmpresa");
        this.direccionSegmento = "";
    }
}