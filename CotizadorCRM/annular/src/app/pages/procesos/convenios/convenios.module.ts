﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmacionModule } from '../../_modales/confirmacion/confirmacion.module';
import { SharedModule } from '../../../shared/shared.module';
import { ConveniosComponent } from './convenios.component';;
import { ConvenioComponent } from './convenio/convenio.component'
import { fcGeneral } from '../../../services/general.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { AumentarVigenciaModule } from '../../_modales/aumentar-vigencia/aumentar-vigencia.module';


export const routes = [
    { path: '', component: ConveniosComponent, pathMatch: 'full' },
    { path: 'convenio/:id', component: ConvenioComponent, data: { breadcrumb: 'Convenio' } }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ConfirmacionModule,
        NgSelectModule,
        ConfirmacionModule,
        AumentarVigenciaModule
    ],
    declarations: [
        ConveniosComponent,
        ConvenioComponent
    ],
    providers: [
        fcGeneral
    ]
})
export class ConveniosModule { }