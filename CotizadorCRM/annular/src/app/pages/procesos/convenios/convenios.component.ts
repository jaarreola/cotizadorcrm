import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBarConfig, MatSnackBar } from '@angular/material';
import { Settings } from '../../../app.settings.model';
import { AppSettings } from '../../../app.settings';
import { Router } from '@angular/router';
import { fcGeneral } from '../../../services/general.service';
import { SelectionModel } from '@angular/cdk/collections';
import { AumentarVigenciaComponent } from '../../_modales/aumentar-vigencia/aumentar-vigencia.component';

@Component({
  selector: 'app-convenios',
  templateUrl: './convenios.component.html',
  styleUrls: ['./convenios.component.scss']
})
export class ConveniosComponent implements OnInit {
    private urlApiConvenio = 'api/Convenio';
    private urlApiCommon = 'api/Common';
    private urlApiCliente = 'api/Cliente';
    private urlApiRutas = 'api/Rutas';

    @ViewChild(MatPaginator) paginator: MatPaginator;
    public dataSource = new MatTableDataSource<any>();
    public displayedColumns: string[] = [
        'check',
        'convenio',
        'cliente',
        'nombre',
        'ruta',
        'estatus',
        'remitente',
        'destinatario'
    ];

    public settings: Settings;
    config = new MatSnackBarConfig();

    clientes: any[] = [];
    rutas: any[] = [];
    estatus: any[] = [];

    convenios: any[] = [];

    hoy = new Date();
    fechaInicio = new Date(new Date().setDate(this.hoy.getDate() - 7))
    filtro: any = {
        idEmpresa: +sessionStorage.getItem("idEmpresa"),
        idCliente: 0,
        fechaInicio: this.fechaInicio,
        fechaFin: new Date(),
        nombre: "",
        idCotizacionEstatus: 0,
        idRuta: 0
    }
    filtros = true;

    selection = new SelectionModel<any>(false, []);
    constructor(private router: Router, public appSettings: AppSettings, private _service: fcGeneral, public dialog: MatDialog, private _snackBar: MatSnackBar) {
        this.settings = this.appSettings.settings;

        this.config.panelClass = ['custom-snackbar'];
        this.config.duration = 2500;
        this.config.verticalPosition = 'top';
    }

    ngOnInit() {
        this.consultarClientes();
        this.consultarRutas();
        this.consultarEstatusConvenio();

        this.consultarConvenios();
    }

    consultarClientes() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiCliente, 'getClientesListaAutocomplete', json).subscribe(
            result => {
                this.clientes = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarRutas() {
        let params = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
        }
        let json = JSON.stringify(params);

        this._service.postAPI(this.urlApiRutas, 'getRutasListaAutocomplete', json).subscribe(
            result => {
                this.rutas = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarEstatusConvenio() {
        this._service.getAPI(this.urlApiCommon, 'getConvenioEstatusListado').subscribe(
            result => {
                this.estatus = result;
            },
            error => {
                console.log(error);
            }
        );
    }

    consultarConvenios() {
        let json = JSON.stringify(this.filtro);

        this._service.postAPI(this.urlApiConvenio, 'getConvenioLista', json).subscribe(
            result => {
                this.convenios = result;
                this.dataSource = new MatTableDataSource(this.convenios);
                this.dataSource.paginator = this.paginator;
            },
            error => {
                console.log(error);
            }
        );
    }

    nuevo() {
        this.router.navigate(['pages/convenios/convenio', 'N']);
    }

    editar(convenio) {
        this.router.navigate(['pages/convenios/convenio', convenio.idConvenio]);
    }

    limpiar() {
        this.filtro = {
            idEmpresa: +sessionStorage.getItem("idEmpresa"),
            idCliente: 0,
            fechaInicio: this.fechaInicio,
            fechaFin: new Date(),
            nombre: "",
            idCotizacionEstatus: 0,
            idRuta: 0
        }
    }

    abrirModalAumentarVigencia() {
        if (this.selection.selected.length > 0) {
            const dialogRef = this.dialog.open(AumentarVigenciaComponent, {
                width: '25%',
                disableClose: true,
                data: this.selection.selected[0]
            });
        } else {
            this._snackBar.open('Selecciona un convenio antes de continuar.', '', this.config);
        }
        
    }

    validarGenerarConvenio() {
        if (this.selection.selected.length > 0) {
            this.generarConvenio();
        } else {
            this._snackBar.open('Selecciona un convenio antes de continuar.', '', this.config);
        }
    }

    generarConvenio() {
        let convenioModel = {
            idConvenio: this.selection.selected[0].idConvenio,
            idCotizacion: this.selection.selected[0].idCotizacion
        }

        let json = JSON.stringify(convenioModel);

        this._service.postAPI(this.urlApiConvenio, "setContratoToConvenio", json).subscribe(
            result => {
                this._snackBar.open('Se ha generado el convenio exitosamente.', '', this.config);
            },
            error => {
                console.log(error);
            }
        );
    }

}
