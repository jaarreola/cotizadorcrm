import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { emailValidator } from '../../theme/utils/app-validators';
import { AppSettings } from '../../app.settings';
import { Settings } from '../../app.settings.model';
import { fcGeneral } from '../../services/general.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
    private urlApiUsuario = 'api/Usuario';

    public error = false;
    public errMsj = "";

    public form:FormGroup;
    public settings: Settings;
    constructor(public appSettings: AppSettings, public fb: FormBuilder, public router: Router, private _service: fcGeneral) {
    this.settings = this.appSettings.settings; 
    this.form = this.fb.group({
        'email': [null, Validators.compose([Validators.required])],
        'password': [null, Validators.compose([Validators.required])],
        'rememberMe': false
    });
    }

    public onSubmit(values:Object):void {
        if (this.form.valid) {
            this.settings.loadingSpinner = true;
            let params = {
                usuario: this.form.get("email").value,
                password: this.form.get("password").value
            }
            let json = JSON.stringify(params);
            this._service.postAPI(this.urlApiUsuario, 'getUsuarioLogin', json).subscribe(
                result => {
                    if (+result.errorCode == 0) {
                        this.iniciarSesion(result);
                    } else {
                        this.settings.loadingSpinner = false;
                        this.error = true;
                        this.errMsj = result.errorMessage;
                    }
                },
                error => {
                    this.settings.loadingSpinner = false;
                    console.log(error);
                }
            );
        }
        
    }

    iniciarSesion(usuario) {
        sessionStorage.setItem("idUsuario", usuario.idUsuario);
        sessionStorage.setItem("idEmpresa", usuario.idEmpresa);
        sessionStorage.setItem("sistemaOrigen", usuario.sistemaOrigen);
        sessionStorage.setItem("nombreUsuario", usuario.nombre + " " + usuario.apellidoPaterno);
        sessionStorage.setItem("nombreEmpresa", usuario.empresa);
        //sessionStorage.setItem("idArea", usuario.idArea);
        this.router.navigate(['/pages']);
    }

    ngAfterViewInit(){
        this.settings.loadingSpinner = false; 
    }
}