﻿import { Injectable } from '@angular/core';
//import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { utils } from '../utils/utils';
import { map } from 'rxjs/operators';

@Injectable()
export class fcGeneral {
    public _utils: utils;

    public headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json, text/javascript, /;',
        'idEmpresa': '' + +sessionStorage.getItem("idEmpresa"),
        'idUsuario': '' + +sessionStorage.getItem("idUsuario"),
        'idArea': '1'
        //'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        //'xhrFields' : '{withCredentials : true}'
    });
    public headersImpresion = new HttpHeaders({
        'Content-Type': 'application/pdf',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3'
    })

    constructor(private _http: HttpClient, private router: Router) {
        this._utils = new utils();
    }

    /**
     * Realiza una metodo GET del web service
     * @param serviceName Nombre del servicio web
     * @param funcName Metodo de la peticion
     */
    public get(serviceName: string, funcName: string): Observable<any> {
        return this._http.get('' + serviceName + '/' + funcName, { headers: this.headers }).pipe(
            map((res: any) => {
            return JSON.parse(res.json());
        }));
    }

    /**
     * Realiza una metodo GET del web service
     * @param serviceName Nombre del servicio web
     * @param funcName Metodo de la peticion
     */
    public getAPI(serviceName: string, funcName: string): Observable<any> {
        return this._http.get('' + serviceName + '/' + funcName, { headers: this.headers }).pipe(
            map((res: any) => {
                return JSON.parse(res);
            }));
    }

    /**
     * Realiza una metodo POST del web service
     * @param serviceName Nombre del web service
     * @param funcName Metodo de la peticion
     */
    public post(serviceName: string, funcName: string, params: any): Observable<any> {
        let json = JSON.stringify(params);

        return this._http.post('' + serviceName + '/' + funcName, json, { headers: this.headers }).pipe(
            map((res: any) => {
            return JSON.parse(res.json());
        }));
    }

    /**
     * Realiza una metodo POST del web service
     * @param serviceName Nombre del web service
     * @param funcName Metodo de la peticion
     */
    public postAPI(serviceName: string, funcName: string, params: any): Observable<any> {
        //let json = JSON.stringify(params);

        return this._http.post('' + serviceName + '/' + funcName, params, { headers: this.headers }).pipe(
            map((res: any) => {
                return JSON.parse(res);
            }));
    }
}