import { Menu } from './menu.model';

export const verticalMenuItems = [ 
    //number, title, route, href, icon, target, hasSubmenu, parentId
    new Menu (1, 'Dashboard', 'dashboard', null, 'dashboard', null, false, 0),
    new Menu (2, 'Procesos', null, null, 'assignment', null, true, 0),
    //new Menu (3, 'Seguimiento a clientes', 'seguimiento-clientes', null, 'dashboard', null, false, 2),
    new Menu (4, 'Cotizador', 'cotizador', null, 'monetization_on', null, false, 2),
    //new Menu (5, 'Contratos', 'contratos', null, 'dashboard', null, false, 2),
    new Menu (6, 'Contratos', 'convenios', null, 'folder_shared', null, false, 2),
    //new Menu (7, 'Consultas', null, null, 'search', null, true, 0),
    //new Menu (8, 'Cotizaciones vs Utilidad real', '/', null, 'dashboard', null, false, 7),
    new Menu (9, 'Catalogos', null, null, 'folder', null, true, 0),
    new Menu (10, 'Clientes', 'clientes', null, 'people', null, false, 9),
    new Menu (11, 'Plazas', 'plazas', null, 'domain', null, false, 9),
    new Menu (12, 'Rutas', 'rutas', null, 'directions', null, false, 9),
    new Menu(13, 'Tipos de Operacion', 'tipo-operaciones', null, 'handyman', null, false, 9),
    new Menu(14, 'Tarifas combustible', 'tarifas-combustible', null, 'local_gas_station', null, false, 9),
    new Menu(15, 'Tarifas UREA', 'tarifas-urea', null, 'local_gas_station', null, false, 9),
    new Menu (16, 'Vehiculos', 'vehiculos', null, 'directions_car', null, false, 9),
    new Menu (17, 'Servicios', 'servicios', null, 'local_shipping', null, false, 9),
    //new Menu (16, 'Servicios', '/servicios', null, 'local_shipping', null, false, 9),
    //new Menu (17, 'Servicios', '/servicios', null, 'local_shipping', null, false, 9),
    new Menu (18, 'Productos', 'productos', null, 'shopping_cart', null, false, 9),    
    new Menu (19, 'Personal/Agentes', 'personal-agentes', null, 'account_circle', null, false, 9),
    new Menu(20, 'Tipo de cambio', 'tipos-cambio', null, 'account_balance', null, false, 9),
    new Menu(22, 'Configuración Costos Variables', 'configuracion-costos-variables', null, 'settings', null, false, 9),
    new Menu (21, 'Configuración Costos Fijos', 'configuracion-costos-fijos', null, 'settings', null, false, 9),
]

export const horizontalMenuItems = [
    new Menu (1, 'Dashboard', '/', null, 'dashboard', null, false, 0),
]