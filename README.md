# CotizadorCRM

Es un producto desarrollado por LIS Software Solutions que tiene la flexibilidad de conectarse con cualquier ERM para sincronizar información existente; o bien, puede ser utilizado sin ninguna integración dando de alta toda la información necesaria en catálogos y configuraciones.

### Pre-Requisitos:

* Visual Stucio 2019
* NetCore 3.1
* nodeV11.6.0 (indispensable)

*Si no se tiene instalada la versión, utilizar  [NVM Windows](https://github.com/coreybutler/nvm-windows/) para manejo de la versión requerida de NODE 11.6.0

### Instalar y compilación de proyecto

Clonar repositorio

```
git clone https://<user>@bitbucket.org/jaarreola/cotizadorcrm.git
```

Instalar los node_modules dentro de la ruta CotizadorCRM\annular 

```
npm install
```

Instalar complemento node-sass dentro de la ruta CotizadorCRM\annular

```
npm install node-sass
```

Instalar complemento ng-select dentro de la ruta CotizadorCRM\annular

```
npm install --save @ng-select/ng-select@1.4.1
```
## Deployment

### Para Linux UBUNTU (only 16.04-x64)

En node.js CMD de Windows, en la carpeta de solución de proyectos (Ej. "C:\FuentesCotizadorCRM\cotizadorcrm\slnCotizadorCRM.sln"), ejecutar el deploy para la versión específica de Linux:

```
dotnet publish -r linux-x64 --self-contained false
```

Al tener la carpeta de publicación, se requieren todos los archivos generados desde linux-x64 (no usar solo los de publish).

### Para Windows (IIS)

Por definir...

## Versionamiento

### Beta 0.1: 

Creación de Front con Template Angular.

Diseño de catálogos.

Diseño de proceso Cotizador. 

## Autores

LIS Software Solutions. 

## Licenciamiento

Uso comercial por LIS Software Solutions. 
